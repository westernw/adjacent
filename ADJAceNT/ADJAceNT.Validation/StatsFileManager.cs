﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ADJAceNT.Validation
{
    class StatsFileManager
    {
        private const string FilenameTemplate = "stats_{0:dd'_'MM'_'yy'-'HH'_'mm'_'ss'_'fff}.csv";

        public static void WriteStats(IEnumerable<string> allStats, string statHeaderLine)
        {
            using (var statsFile = new StreamWriter(string.Format(FilenameTemplate, DateTime.Now)))
            {
                statsFile.WriteLine(statHeaderLine);
                foreach (var line in allStats)
                {
                    statsFile.WriteLine(line);
                }
            }
        }
    }
}
