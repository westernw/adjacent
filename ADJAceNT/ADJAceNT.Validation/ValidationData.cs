﻿namespace ADJAceNT.Validation
{
    class ValidationData
    {
        public double RMSError { get; set; }
        public double TemperatureUsed { get; set; }
        public ValidationRun ActionRecorded { get; set; }
    }

    enum ValidationRun
    {
        None,
        PositiveAdded,
        NegativeAdded
    }
}
