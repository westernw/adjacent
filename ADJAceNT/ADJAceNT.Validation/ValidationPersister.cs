﻿using System.Collections.Generic;
using System.Linq;

namespace ADJAceNT.Validation
{
    class ValidationPersister
    {
        /// <summary>
        /// Will persist the validation data as CSV file in application folder with date-time in filename
        /// </summary>
        /// <param name="validationData">A single validation run data set (or merged as desired by caller)</param>
        public void PersistValidationData(IEnumerable<ValidationData> validationData)
        {
            StatsFileManager.WriteStats(LazyValidationToSimpleCSVIterator(validationData), "RMSE,Temperature,Action");
        }

        /// <summary>
        /// Converts ValidationData objects to simple CSV rows, assuming simple structure of the object
        /// </summary>
        private IEnumerable<string> LazyValidationToSimpleCSVIterator(IEnumerable<ValidationData> validationData)
        {
            return validationData.Select(_ => string.Join(",", _.RMSError, _.TemperatureUsed, _.ActionRecorded));
        }
    }
}
