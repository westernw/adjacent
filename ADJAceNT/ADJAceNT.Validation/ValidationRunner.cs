﻿using System;
using System.Collections.Generic;
using System.Linq;
using ADJAceNT.Entities;
using ADJAceNT.Managers;
using ADJAceNT.ViewModel;
using log4net;
using Moq;

namespace ADJAceNT.Validation
{
    class ValidationRunner
    {
        private readonly ILog _mockLogger;
        private readonly List<string> _searchTerms;
        private readonly ValidationLoader _loader;
        private RankManager _rankMgr;
        private AdMiscActionViewModel _blackWriter;
        private IDataManager _dataMgr;

        public ValidationRunner(List<string> searchTerms, ValidationLoader loader)
        {
            _mockLogger = new Mock<ILog>().Object;
            _searchTerms = searchTerms;
            _loader = loader;
            _loader.LoadAll();
            _dataMgr = new DatabaseManager(_mockLogger);
            _rankMgr = new RankManager("Validation", _dataMgr);
            _blackWriter = new AdMiscActionViewModel(null, _dataMgr) { CurrentUser = "Validation" };
        }

        /// <summary>
        /// Runs the determening of number of positive and negative advertisements to add
        /// using given temparature, adds positive first
        /// Note: Resets database to initial empty version by default, 
        /// use <param name="resetDatabase"/> false to keep previous knowledge
        /// </summary>
        /// <param name="initialTemparature">Value to start with for difference between RMS errors permitted</param>
        /// <param name="coolingStep">How much to decrease the temparature on each iteration of advert added to knowledgebase</param>
        /// <param name="resetDatabase">Resets db to blank copy of validation knowledge base for run</param>
        /// <returns>A list of RMSE throughout the effect of positive and negative advertisement addition to knowledge</returns>
        public List<ValidationData> RunPositiveFirstAt(double initialTemparature, double coolingStep, bool resetDatabase = true)
        {
            return RunAt(resetDatabase, initialTemparature, coolingStep, true);
        }

        /// <summary>
        /// Runs the determening of number of positive and negative advertisements to add
        /// using given temparature, adds negative first
        /// Note: Resets database to initial empty version by default, 
        /// use <param name="resetDatabase"/> false to keep previous knowledge
        /// </summary>
        /// <param name="initialTemparature">Value to start with for difference between RMS errors permitted</param>
        /// <param name="coolingStep">How much to decrease the temparature on each iteration of advert added to knowledgebase</param>
        /// /// <param name="resetDatabase">Resets db to blank copy of validation knowledge base for run</param>
        /// /// <returns>A list of RMSE throughout the effect of negative and positive advertisement addition to knowledge</returns>
        public List<ValidationData> RunNegativeFirstAt(double initialTemparature, double coolingStep, bool resetDatabase = true)
        {
            return RunAt(resetDatabase, initialTemparature, coolingStep, false);
        }

        /// <summary>
        /// Method to compare how much target differs from sample set without ranking
        /// </summary>
        /// <returns>Root Mean Square Error of difference</returns>
        public double GetInitialErrorBetweenDataLoaded()
        {
            return RootMeanSquaredError(_loader.Target, _loader.Sample);
        }

        /// <summary>
        /// This private method is intended to allow run full set {positive, negative} of validation
        /// parameters are described in public method. In summary it would reset database of weights
        /// if instructed by boolean, then run positive and negative algorithm in order determined
        /// by another boolean parameter. Finally it returns all run data in order.
        /// </summary>
        private List<ValidationData> RunAt(bool resetDatabase, double initialTemparature, double coolingStep, bool positiveFirst)
        {
            if(resetDatabase) ResetDatabase();
            var pResults = RunImprovementAlgo(double.NaN, positiveFirst, initialTemparature, coolingStep);
            var nResults = RunImprovementAlgo(pResults.Last().RMSError, !positiveFirst, initialTemparature, coolingStep);
            pResults.AddRange(nResults);
            return pResults;
        }

        /// <summary>
        /// This private method is intended to iterate through ranking and setting advertisement 
        /// interesting/not interesting until the temperature does not permit any more undesired
        /// moves or the list of advertisements runs out (prefer suitable temperature threshold).
        /// 
        /// The idea is based on Simulated Annealing concept from field of AI where it is often
        /// applied to hill-climbing problems to escape local maxima or minima as well as flats.
        /// While the temperature value is high it permits liking/disliking advertisments which
        /// increase RMSE, but as temperature gets very low it only permits advertisements which
        /// lower RMSE and stops once the error starts rising.
        /// </summary>
        /// <param name="initialResult">The initial value to compare to, if unsure use double.MaxValue</param>
        /// <param name="isPositive">Is algorithm to "like" relevant advertisements or "dislike" them when running</param>
        /// <param name="initTemperature">The temperature to start at (depends on input data, should be experimented with by human)</param>
        /// <param name="coolingStep">The cooling step to lower temperature by on each iteration (after each new advertisement adjustment ranked)</param>
        /// <returns>A list of validation data objects depicting the run</returns>
        private List<ValidationData> RunImprovementAlgo(double initialResult, bool isPositive, double initTemperature, double coolingStep)
        {
            //if positive we will be adding advertisements from the target top ranked, otherwise from worsed bottom ranked
            var i = isPositive ? 0 : 1;
            var action = isPositive ? ValidationRun.PositiveAdded : ValidationRun.NegativeAdded;
            var temperature = initTemperature;
            var target = _loader.Target;
            var results = new List<ValidationData>();

            //1. First time ranked
            //2. Calculate Error
            var ranked = _rankMgr.RankAdvertisements(_loader.Sample, _searchTerms);
            var result = RootMeanSquaredError(target, ranked);
            var previous = double.IsNaN(initialResult) ? result : initialResult;
            var halfWay = target.Count/2;
            //bool isImproving = true, but there is no need to exit loop because we need to return anyway so just use true
            //3. Add another advertisement to weights
            //4. Check error result is permissable
            //5. If so, add to results, decrease temperature, repeat : else return
            while (true)
            {
                (isPositive ? _blackWriter.SetAdStatusGood : _blackWriter.SetAdStatusBad).Execute(target[isPositive ? i++ : target.Count - i++]);
                if ((Math.Abs(result - previous) > temperature) || i == halfWay)
                {
                    _blackWriter.SetAdStatusNone.Execute(target[isPositive ? i - 1 : target.Count - i]);
                    return results;
                }
                results.Add(new ValidationData{RMSError = result, TemperatureUsed = temperature, ActionRecorded = action});
                previous = result;
                temperature -= coolingStep;
                ranked = _rankMgr.RankAdvertisements(_loader.Sample, _searchTerms);

                result = RootMeanSquaredError(target, ranked);
                
            }
        }

        /// <summary>
        /// Gets RMSE for differences in advertisement index for each advertisement in list
        /// Square Root of 
        /// (Sum of squared differences between index of Ad in target and index of Ad in result
        /// divided by the number of advertisements)
        /// √(Σ((T(ad.)-R(ad.))^2)/n) where 
        /// T is target list, 
        /// R is result list, 
        /// ad. is advertisement index in list, 
        /// n is total number of advertisements in list
        /// </summary>
        /// <param name="target">The list which is the desired output</param>
        /// <param name="result">The list which is the output generated</param>
        /// <returns>RMSE value relative to the advertisement index position differences</returns>
        private double RootMeanSquaredError(List<Advertisement> target, List<Advertisement> result)
        {
            if (target.Count != result.Count) throw new ArgumentException("The two sequences must have same elements");
            
            //Improvement in performance over simplicity (original simple below)
            //return Math.Sqrt(
            //        target.Sum(
            //        _ => Math.Pow(target.IndexOf(_) - result.IndexOf(_), 2))
            //        /
            //        target.Count
            //       );

            //Make a dictionary of ads to indices for virtually O(1) retireval instead of looking for index every time
            //Note: It is important that Advertisement class implements sensible GetHashCode
            var map = new Dictionary<Advertisement, int>(result.Count);
            for (var i = 0; i < result.Count; i++)
            {
                map.Add(result[i], i);
            }

            //Get the sum of squared differences and divide by number of items to get mean
            //Note: Math.Pow is made to handle generic input such as fractional powers
            //... : is is magnitude slower than any sensible custom implementation
            //... : since we only want to square, we can multiply diff by itself
            var runningTotal = 0.0;
            for (var i = 0; i < target.Count; i++)
            {
                var diff = (i - map[target[i]]);
                runningTotal += diff*diff;
            }
            runningTotal = runningTotal/target.Count;

            //get square root of mean of squared differences and return it
            return Math.Sqrt(runningTotal);
            
        }

        private void ResetDatabase()
        {
            //make sure we don't hold on to database
            _rankMgr = null;
            _blackWriter = null;
            _dataMgr.Dispose();
            _dataMgr = null;
            GC.Collect();   

            //Reset database to being empty with one Validation user
            _loader.ResetDatabase();

            //recreate our objects
            _dataMgr = new DatabaseManager(_mockLogger);
            _rankMgr = new RankManager("Validation", _dataMgr);
            _blackWriter = new AdMiscActionViewModel(null, _dataMgr) { CurrentUser = "Validation" };
        }
    }
}
