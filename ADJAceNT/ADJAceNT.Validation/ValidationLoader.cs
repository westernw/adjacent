﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using ADJAceNT.Entities;
using ADJAceNT.Managers;

namespace ADJAceNT.Validation
{
    class ValidationLoader : IDisposable
    {
        private List<Advertisement> _target;
        private List<Advertisement> _sample;
        private readonly string _dbName;
        private readonly Dictionary<string, string> _valParams;
        private bool _isDisposed;

        private static ValidationLoader _instance;

        /// <summary>
        /// The loader utilises singleton pattern as it manages filesystem IO resources
        /// and will not take risks of conflicting file use, nor protect against them 
        /// by other means
        /// </summary>
        public static ValidationLoader Instance
        {
            get { return _instance ?? (_instance = new ValidationLoader()); }
        }

        /// <summary>
        /// Returns the target advertisement set loaded, intended to be the goal
        /// </summary>
        public List<Advertisement> Target { get { return _target; } }

        /// <summary>
        /// Returns unranked set of advertisements, intended to be in undefined order
        /// same as if retrieved from the relevant websites and saved without reordering
        /// </summary>
        public List<Advertisement> Sample { get { return _sample; } }

        /// <summary>
        /// Requires ValidationParameters to be present in app.config and the connection string
        /// for databaseName with "-backup" in the name
        /// </summary>
        private ValidationLoader()
        {
            _valParams = ConnectorManager.GetConnectorsByGroupName("ValidationParameters");
            _dbName = ConfigurationManager.ConnectionStrings["databaseName"].ToString();
            File.Copy(_dbName.Replace("-backup", string.Empty), _dbName, true);
        }

        /// <summary>
        /// Loads the target and sample advertisements into colelctions
        /// </summary>
        public void LoadAll()
        {
            _target = AdvertisementReaderCsv.Read(_valParams["target"]);
            _sample = AdvertisementReaderCsv.Read(_valParams["sample"]);
        }

        /// <summary>
        /// Resets the SQLite database file used to original state it was in
        /// at launch of Validation (by overwriting with original file)
        /// </summary>
        public void ResetDatabase()
        {
            File.Copy(_dbName.Replace("-backup", string.Empty), _dbName, true);
        }
        
        /// <summary>
        /// Triggers the temporary database file to be deleted
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    try { File.Delete(_dbName); }
                    catch (IOException) { }//if can't delete don't worry
                }
                _isDisposed = true;
            }
        }

        ~ValidationLoader()
        {
            Dispose(false);
        }
    }
}
