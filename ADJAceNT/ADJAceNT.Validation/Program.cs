﻿using System;
using System.Collections.Generic;
using ADJAceNT.Managers;

namespace ADJAceNT.Validation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Initialising Pre-requisites");
            var loader = ValidationLoader.Instance;
            if (loader == null)
            {
                Console.WriteLine("The appconfig is missing validation parameters. Press Any Key to close...");
                Console.Read();
                return;
            }

            var config = ConnectorManager.GetConnectorsByGroupName("SimpleSearchKeyword");
            var runner = new ValidationRunner(new List<string> {config["keyword"]}, loader);
            
            var result = runner.GetInitialErrorBetweenDataLoaded();
            Console.WriteLine("The error on initial advertisement set is: {0}", result);

            var analyser = new ValidationAnalyser();
            var persister = new ValidationPersister();
            const int initialTemparature = 52;
            var allRuns = new List<List<ValidationData>>(initialTemparature / 4 * 2);
            Console.Write("Running");
            for (int i = initialTemparature; i > 0; i -= 4)
            {
                Console.Write(".");
                var step = i/80.0;
                var posResults = runner.RunPositiveFirstAt(i, step);
                allRuns.Add(posResults);
                
                var negResults = runner.RunNegativeFirstAt(i, step);
                allRuns.Add(negResults);

                persister.PersistValidationData(posResults);
                persister.PersistValidationData(negResults);
            }

            analyser.CrossRunAnalysis(allRuns, result);

            Console.WriteLine("Done");
        }
    }
}
