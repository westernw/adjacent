﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ADJAceNT.Validation
{
    class ValidationAnalyser
    {
        /// <summary>
        /// Displays analysis of all runs passed in compared with each other and against base error
        /// </summary>
        /// <param name="statsFromAllRuns">List of validation runs</param>
        /// <param name="baseError">The error of target to unranked initial advertisement set</param>
        public void CrossRunAnalysis(List<List<ValidationData>> statsFromAllRuns, double baseError)
        {
            //make this method behave, don't change collection passed in, make a copy
            statsFromAllRuns = statsFromAllRuns.Select(_ => _.ToList()).ToList();

            //get best run
            Console.WriteLine("\n***Best Run***");
            DisplayCrossRunSection(ref statsFromAllRuns, baseError);

            //get up to 3 next best runner-ups remaining
            Console.WriteLine("\n***Runner ups***");
            var limit = Math.Min(statsFromAllRuns.Count, 3);
            for(int i = 0; i < limit; i++)
                DisplayCrossRunSection(ref statsFromAllRuns, baseError);
        }

        /// <summary>
        /// Displays analysis of single validation run in terms of 
        /// temperature used at populating, final error and
        /// rising and falling edge detail, etc.
        /// </summary>
        /// <param name="statsFromRun">The single validation run (usually produced by <see cref="ValidationRunner"/></param>
        public void AnalyzeAndDisplaySingleRun(List<ValidationData> statsFromRun)
        {
            var firstStat = statsFromRun[0];
            statsFromRun.RemoveAt(0);
            
            double fallingEdgeSum = 0.0;
            double risingEdgeSum = 0.0;
            double prevStatError = firstStat.RMSError;
            var fallingRun = new List<ValidationRun>();
            var risingRun = new List<ValidationRun>();

            foreach (var stat in statsFromRun)
            {
                if (prevStatError > stat.RMSError)
                {
                    fallingEdgeSum += stat.RMSError;
                    fallingRun.Add(stat.ActionRecorded);
                    prevStatError = stat.RMSError;
                }
                else if(prevStatError < stat.RMSError)
                {
                    risingEdgeSum += stat.RMSError;
                    risingRun.Add(stat.ActionRecorded);
                    prevStatError = stat.RMSError;
                }
            }

            var minError = statsFromRun.Last().RMSError;
            var runTemperature = firstStat.TemperatureUsed;
            Console.WriteLine("Run:\nTemperature: {0}, Step: {1}, Final Error:{2}", runTemperature, runTemperature / 80, minError);

            var numPositiveAdded = statsFromRun.Count(_ => _.ActionRecorded == ValidationRun.PositiveAdded);
            var numNegativeAdded = statsFromRun.Count - numPositiveAdded;
            Console.WriteLine("{0} {1} and {2} {3} to achieve this", numPositiveAdded, ValidationRun.PositiveAdded, numNegativeAdded, ValidationRun.NegativeAdded);

            Console.WriteLine(FillInfo("rising", risingEdgeSum, risingRun));
            Console.WriteLine(FillInfo("falling", fallingEdgeSum, fallingRun));
        }

        /// <summary>
        /// Private, selects and removes best run from all pessed in and displays stats
        /// </summary>
        /// <param name="statsFromAllRuns">All validation runs</param>
        /// <param name="baseError">The error of target to unranked initial advertisement set</param>
        private void DisplayCrossRunSection(ref List<List<ValidationData>> statsFromAllRuns, double baseError)
        {
            var bestRun = GetBestRun(statsFromAllRuns);
            var bestMinError = bestRun.Last().RMSError;

            AnalyzeAndDisplaySingleRun(bestRun);

            statsFromAllRuns.Remove(bestRun);
            var avgPercentageImprovementOverOther =
                statsFromAllRuns.Average(_ => (_.Last().RMSError - bestMinError) / bestMinError * 100);
            Console.WriteLine("Average % improvement over the rest is {0}", avgPercentageImprovementOverOther);
            Console.WriteLine("Improvement over the base is {0}", (baseError - bestMinError) / bestMinError * 100);
        }

        /// <summary>
        /// Chooses most successful run based on result RootMeanSquareError
        /// </summary>
        /// <param name="runs">All runs to select from</param>
        /// <returns>
        /// The run with smallest final RMSE 
        /// (or first in order if there are several same RMSE
        /// for a number of runs)
        /// </returns>
        private List<ValidationData> GetBestRun(IEnumerable<List<ValidationData>> runs)
        {
            var bestRun = new List<ValidationData>();
            var bestMinError = double.MaxValue;
            foreach (var run in runs)
            {
                var runError = run.Last().RMSError;
                if (bestMinError > runError)
                {
                    bestMinError = runError;
                    bestRun = run;
                }
            }
            return bestRun;
        }

        /// <summary>
        /// Formats the information string based on the run and passed parameters
        /// </summary>
        /// <param name="edge">Either rising or falling (not checked as method is private)</param>
        /// <param name="totalChangeSum">the value of total error gathered on the passed edge</param>
        /// <param name="allRunStates">All validation run states to comment on</param>
        /// <returns>String with data filled in based on input parameters, intended for printing to user</returns>
        private string FillInfo(string edge, double totalChangeSum, List<ValidationRun> allRunStates)
        {
            var onPositive = allRunStates.Count(_ => _ == ValidationRun.PositiveAdded);
            var onNegative = allRunStates.Count - onPositive;
            var mostCommon = 
                onNegative == onPositive
                ? ValidationRun.None
                : onNegative > onPositive
                    ? ValidationRun.NegativeAdded
                    : ValidationRun.PositiveAdded;

            var differenceInDistribution = Math.Abs(onPositive - onNegative);

            return string.Format("The total {0} edge of {1} was mostly caused by {2} advertisements during learning, outnumbering the opposite by {3}", 
                edge, totalChangeSum, mostCommon, differenceInDistribution);
        }
    }
}
