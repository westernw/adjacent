﻿// This comment is to clearly state that the contents of this file
// are used from dragonz project at https://wpfactb.codeplex.com/SourceControl/latest#trunk/AutoCompleteTextBox/dragonz.actb/core/AutoCompleteManager.cs
// Accessed November 2014.
// The file is under Microsoft Public License (Ms-PL), detail: https://wpfactb.codeplex.com/license
// Refactoring and modifications have been applied to tailor the class to the task in this project and comment showing understanding of functionality

using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace ADJAceNT.ThirdParty
{
    public class AutoCompleteComboBox : ComboBox
    {
        private readonly AutoCompleteManager _acm;  //the manager for actual suggestions
        private TextBox _textBox;                   //the textbox that listens for input
        private int _oldSelStart;                   //backup fields
        private int _oldSelLength;
        private string _oldText;

        public AutoCompleteManager AutoCompleteManager
        {
            get { return _acm; }
        }

        public AutoCompleteComboBox()
        {
            IsEditable = true;
            IsTextSearchEnabled = false;
            GotMouseCapture += AutoCompleteComboBox_GotMouseCapture;

            //set autocompletion and async as default
            _acm = new AutoCompleteManager();
        }

        private void AutoCompleteComboBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            _oldSelStart = _textBox.SelectionStart;
            _oldSelLength = _textBox.SelectionLength;
            _oldText = _textBox.Text;
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (_acm.AutoCompleting)    //if the suggestions are open don't do anything
            {
                return;
            }
            if (e.Key == Key.Up || e.Key == Key.Down)
            {
                SelectedValue = Text;
            }
            base.OnPreviewKeyDown(e);
        }

        protected override void OnDropDownOpened(EventArgs e)
        {
            _acm.Disabled = true;           
            IsTextSearchEnabled = true;
            SelectedValue = Text;

            base.OnDropDownOpened(e);

            if (SelectedValue == null)
            {
                Text = _oldText;
                _textBox.SelectionStart = _oldSelStart;
                _textBox.SelectionLength = _oldSelLength;
            }
        }

        protected override void OnDropDownClosed(EventArgs e)
        {
            base.OnDropDownClosed(e);
            _acm.Disabled = false;
            IsTextSearchEnabled = false;
        }

        /// <summary>
        /// Attaches textbox when control fully loads
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _textBox = GetTemplateChild("PART_EditableTextBox") as TextBox;
            _acm.AttachTextBox(_textBox);
        }
    }
}