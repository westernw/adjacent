﻿// This comment is to clearly state that the contents of this file
// are used from project at  http://www.codeproject.com/Articles/563862/Multi-Select-ComboBox-in-WPF
// Accessed December 2014.
// The file is under CPOL, detail: http://www.codeproject.com/info/cpol10.aspx
// Refactoring and modifications have been applied to tailor the class to the task in this project and comment showing understanding of functionality

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using ADJAceNT.Helpers;

namespace ADJAceNT.ThirdParty
{
    /// <summary>
    /// Interaction logic for MultiComboBox.xaml
    /// </summary>
    public partial class MultiComboBox : UserControl
    {
        private readonly ObservableCollection<Node> _nodeList;
        public MultiComboBox()
        {
            InitializeComponent();
            _nodeList = new ObservableCollection<Node>();
        }

        #region Dependency Properties
        /// <summary>
        /// Original key-value dictionary types replaced to be simple lists to ease use from other classes.
        /// </summary>

        public static readonly DependencyProperty ItemsSourceProperty =
             DependencyProperty.Register("ItemsSource", typeof(List<string>), typeof(MultiComboBox), 
                new FrameworkPropertyMetadata(null, OnItemsSourceChanged));

        public static readonly DependencyProperty SelectedItemsProperty =
         DependencyProperty.Register("SelectedItems", typeof(List<string>), typeof(MultiComboBox), 
            new FrameworkPropertyMetadata(null, OnSelectedItemsChanged));

        public static readonly DependencyProperty TextProperty =
           DependencyProperty.Register("Text", typeof(string), typeof(MultiComboBox), new UIPropertyMetadata(string.Empty));

        public static readonly DependencyProperty DefaultTextProperty =
            DependencyProperty.Register("DefaultText", typeof(string), typeof(MultiComboBox), new UIPropertyMetadata(string.Empty));

        public static readonly DependencyProperty AllLabelProperty =
           DependencyProperty.Register("AllLabel", typeof(string), typeof(MultiComboBox), new UIPropertyMetadata(string.Empty));


        public List<string> ItemsSource
        {
            get { return (List<string>)GetValue(ItemsSourceProperty); }
            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }

        public List<string> SelectedItems
        {
            get { return (List<string>)GetValue(SelectedItemsProperty); }
            set
            {
                SetValue(SelectedItemsProperty, value);
            }
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public string AllLabel
        {
            get { return (string)GetValue(AllLabelProperty); }
            set { SetValue(AllLabelProperty, value); }
        }

        public string DefaultText
        {
            get { return (string)GetValue(DefaultTextProperty); }
            set { SetValue(DefaultTextProperty, value); }
        }
        #endregion

        #region Events
        private static void OnItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MultiComboBox control = (MultiComboBox)d;
            control.DisplayInControl();
        }

        private static void OnSelectedItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MultiComboBox control = (MultiComboBox)d;
            control.SelectNodes();
            control.SetText();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox clickedBox = (CheckBox)sender;

            if (clickedBox.Content.ToString() == AllLabel )
            {
                if (clickedBox.IsChecked.Value)
                {
                    foreach (Node node in _nodeList)
                    {
                        node.IsSelected = true;
                    }
                }
                else
                {
                    foreach (Node node in _nodeList)
                    {
                        node.IsSelected = false;
                    }
                }

            }
            else
            {
                int selectedCount = _nodeList.Count(s => s.IsSelected && s.Title != AllLabel); //count all checked boxes except the added _allKeyword
                _nodeList.FirstOrDefault(i => i.Title == AllLabel).IsSelected = selectedCount == _nodeList.Count - 1;  //decide is all boxes are checked which would trigger the _allKeyword to be ckecked too
            }
            SetSelectedItems();
            SetText();

        }
        #endregion


        #region Methods
        private void SelectNodes()
        {
            if (SelectedItems.Count == 1 && SelectedItems[0] == AllLabel)
            {
                foreach(var node in _nodeList) node.IsSelected = true; //adding logic to select all items if All is set for selected list
                SetSelectedItems(); //adding so that selected items would be set into the list right away
            }
            else
            {
                foreach (string item in SelectedItems)
                {
                    Node node = _nodeList.FirstOrDefault(i => i.Title == item);
                    if (node != null) node.IsSelected = true;
                }
            }
        }

        private void SetSelectedItems()
        {
            if (SelectedItems == null) SelectedItems = new List<string>();
            SelectedItems.Clear();
            foreach (Node node in _nodeList)
            {
                if (node.IsSelected && node.Title != AllLabel)
                {
                    if (this.ItemsSource.Count > 0) SelectedItems.Add(node.Title);
                }
            }
        }

        private void DisplayInControl()
        {
            _nodeList.Clear();
            if (this.ItemsSource.Count > 0)
                _nodeList.Add(new Node(AllLabel));
            foreach (string item in this.ItemsSource)
            {
                Node node = new Node(item);
                _nodeList.Add(node);
            }
            MultiSelectCombo.ItemsSource = _nodeList;
        }

        private void SetText()
        {
            if (SelectedItems != null)
            {
                var isAllSelected = _nodeList.Any(_ => _.Title == AllLabel && _.IsSelected);
                if (isAllSelected)
                    Text = AllLabel;
                else
                {
                    var numOfSelected = _nodeList.Count(_ => _.IsSelected);
                    Text = numOfSelected > 0 ? 
                        string.Concat(numOfSelected, "/", ItemsSource.Count, " Selected") : 
                        string.Empty;
                }
            }           
            // set DefaultText if nothing else selected
            if (string.IsNullOrEmpty(Text))
            {
                Text = DefaultText;
            }
        }
       
        #endregion
    }

    public class Node : BasePropertyChanged
    {
        private string _title;
        private bool _isSelected;
        #region ctor
        public Node(string title)
        {
            Title = title;
        }
        #endregion

        #region Properties
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                OnPropertyChanged("Title");
            }
        }
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                _isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
        #endregion

    }
}

