﻿// This comment is to clearly state that the contents of this file
// are used from dragonz project at https://wpfactb.codeplex.com/SourceControl/latest#trunk/AutoCompleteTextBox/dragonz.actb/provider/SimpleStaticDataProvider.cs
// Accessed November 2014.
// The file is under Microsoft Public License (Ms-PL), detail: https://wpfactb.codeplex.com/license
// Refactoring and modifications have been applied to tailor the class to the task in this project

using System;
using System.Collections.Generic;
using System.Linq;

namespace ADJAceNT.ThirdParty
{
    public class SimpleStaticDataProvider
    {
        private readonly IEnumerable<string> _source;

        public SimpleStaticDataProvider(IEnumerable<string> source)
        {
            _source = source;
        }

        public IEnumerable<string> GetItems(string textPattern)
        {
            return _source.Where(item => item.StartsWith(textPattern, StringComparison.OrdinalIgnoreCase));
        }
    }
}

