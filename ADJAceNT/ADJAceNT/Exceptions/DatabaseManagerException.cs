﻿using System;
using ADJAceNT.Managers;

namespace ADJAceNT.Exceptions
{
    /// <summary>
    /// The class is used by <see cref="DatabaseManager"/> when errors occur
    /// </summary>
    [Serializable]
    public class DatabaseManagerException : Exception
    {
        public DatabaseManagerException() {}

        public DatabaseManagerException(string message) : base(message) {}

        public DatabaseManagerException(string message, Exception inner) : base(message, inner) {}
    }
}
