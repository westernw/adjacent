﻿using System.Collections.Generic;

namespace ADJAceNT.Managers
{
    public interface IExclusionWordReader
    {
        List<string> GetExclusionWords();
    }
}