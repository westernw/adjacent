using System.Collections.Generic;
using ADJAceNT.Entities;

namespace ADJAceNT.Managers
{
    public interface IRankManager
    {
        string CurrentUser { get; set; }

        /// <summary>
        /// Returns advertisements sorted for user property set, if no user set, sorted using searchTerms only
        /// </summary>
        /// <param name="advertisements">List of advertisements to sort</param>
        /// <param name="searchTerms">Relevant search terms which produced the advertisements or provide bias to sort</param>
        /// <returns>a sorted list of advertisements based on user data and search terms</returns>
        List<Advertisement> RankAdvertisements(List<Advertisement> advertisements, List<string> searchTerms);
    }
}