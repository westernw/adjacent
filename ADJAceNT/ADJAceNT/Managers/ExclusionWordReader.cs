﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ADJAceNT.Managers
{
    internal class ExclusionWordReader : AdvertisementBaseCsv, IExclusionWordReader
    {
        public List<string> GetExclusionWords()
        {
            var filePath = Path.Combine(ApplicationPath, "Dictionaries/MostCommonWordExclusion.txt");
            if (File.Exists(filePath))
            {
                return File.ReadAllLines(filePath)
                    .Skip(1)
                    .Select(_ => _.ToUpperInvariant())
                    .ToList();
            }
            throw new ApplicationException("Cannot locate exclusion words file, the application will refuse to work without the file. Check log for detail",
                new Exception(string.Format("{0} missing, please reinstall application", filePath)));
        } 
    }
}
