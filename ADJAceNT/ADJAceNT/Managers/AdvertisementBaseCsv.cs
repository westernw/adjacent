﻿using ADJAceNT.Entities;

namespace ADJAceNT.Managers
{
    internal class AdvertisementBaseCsv
    {
        protected const string Headers = "Company,DescriptionSummary,JobTitle,Salary,Location,JobLink";
        protected static readonly string ApplicationPath = ApplicationDataStore.InstallationRoot;
    }
}