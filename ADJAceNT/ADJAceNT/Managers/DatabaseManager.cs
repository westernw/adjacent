﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using ADJAceNT.Enums;
using ADJAceNT.Exceptions;
using log4net;

namespace ADJAceNT.Managers
{
    /// <summary>
    /// The class manages all interaction with application database
    /// </summary>
    class DatabaseManager : IDataManager
    {
        protected SQLiteConnection Connection;
        private readonly ILog _logger;
        private const string ArgumentEmptyString = "The {0} cannot be null or empty";

        #region SQL template strings

        /// <summary>
        /// Delete from log all later and including date
        /// {0} username, {1} date to delete from up
        /// </summary>
        private const string DeleteAllHistoryTemplate = "DELETE FROM {0}_log WHERE time >= '{1:yyyy'-'MM'-'dd' 'HH':'mm':'ss}'";

        /// <summary>
        /// Need to select minimum date within deletion range for each word and use the weight
        /// {0} username
        /// </summary>
        private const string SelectHistoryWeightsTemplate = "SELECT Word, Weight, min(time) FROM {0}_log WHERE time >= '{1:yyyy'-'MM'-'dd' 'HH':'mm':'ss}' GROUP BY Word";
        
        /// <summary>
        /// rolling back the weights with update case in one update
        /// {0} word, {1} weight value
        /// </summary>
        private const string CaseTemplate = "WHEN Word = '{0}' THEN {1} ";

        /// <summary>
        /// {0} username, {1} <see cref="CaseTemplate"/> filled in
        /// </summary>
        private const string RollbackAllWeightsTemplate = "UPDATE Words_{0} SET Weight = CASE " +
                                                          "{1}" +
                                                          " ELSE Weight END;";

        /// <summary>
        /// {0} username, {1} equality operator (>, >=, etc.), {2} order "desc" or empty
        /// Making orderby to be stable sort by using rowid on tiebreakers
        /// </summary>
        private const string SelectWordsOrderedTemplate = "SELECT Word FROM Words_{0} WHERE Weight {1} 0 ORDER BY Weight,rowid {2} LIMIT 10";

        /// <summary>
        /// Selects word from log based on date ranges, orders by weight
        /// {0} username, {1} Weight operator (>, >=, etc.), {2} upper range date, {3} lower range date,
        /// {4} Order "desc" for positive and none for negative 
        /// Making orderby to be stable sort by using rowid on tiebreakers
        /// </summary>
        private const string SelectWordsOrderedFromHistoryTemplate = "SELECT word FROM {0}_log " +
                                                                     "WHERE Weight {1} 0 " +
                                                                     "and time <= '{2:yyyy'-'MM'-'dd' 'HH':'mm':'ss}' " +
                                                                     "and time > '{3:yyyy'-'MM'-'dd' 'HH':'mm':'ss}' " +
                                                                     "GROUP BY word " +
                                                                     "ORDER BY sum(weight),rowid {4} " +
                                                                     "LIMIT 10";
        
        /// <summary>
        /// {0} username
        /// </summary>
        private const string TriggerTemplate = @"CREATE TRIGGER update_{0}log AFTER INSERT ON Words_{0} BEGIN INSERT INTO {0}_log  (word,weight,time) values (new.word,new.weight,DATETIME('NOW') ); END;";

        private const string LogTableTemplate = @"CREATE TABLE [{0}_log] (primaryindex INTEGER PRIMARY KEY, word VARCHAR(45), weight INTEGER, time DATE);";

        private const string WordTableTemplate = @"CREATE TABLE IF NOT EXISTS [Words_{0}] ([Word] VARCHAR(45) NOT NULL PRIMARY KEY, Weight INT)";

        /// <summary>
        /// {0} username, {1} min,max
        /// </summary>
        private const string SingleTimeSelect = "SELECT {1}(time) FROM {0}_log";

        /// <summary>
        /// Selects time and word from log based on date ranges and optional offset or 0
        /// {0} username, {1} lower range number, {2} upper range number, {3} date to go back from, {4} date to go back to, {5} unit {seconds,minutes,hours,days,months,years}
        /// </summary>
        private const string TimeBasedSelect = "SELECT time, word FROM {0}_log WHERE time <= datetime('{3}', '-{2} {5}') and time > datetime('{4}', '-{1} {5}') ";

        /// <summary>
        /// Drops table; {0} Tablename
        /// </summary>
        private const string DropTableTemplate = "DROP TABLE {0}";

        /// <summary>
        /// Deletes user from users table; {0} Tablename
        /// </summary>
        private const string DeleteUserTemplate = "DELETE FROM Users WHERE Username = '{0}'";

        #endregion

        public DatabaseManager(ILog logger = null, bool autoConnect = true)
        {
            _logger = logger ?? LogManager.GetLogger("GeneralLogFileAppender");
            if(autoConnect) Connect();
        }

        /// <summary>
        /// Connects to the SQLite database, creates new if not found
        /// </summary>
        public void Connect()
        {
            Connection = new SQLiteConnection(String.Format("Data Source=\"{0}\";Version=3;", 
#if DEBUG
                ConfigurationManager.ConnectionStrings["databaseName"]
#else
                Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
                "ADJAceNT", ConfigurationManager.ConnectionStrings["databaseName"].ConnectionString)
#endif
                ));
            try
            {
                Connection.Open();
            }
            catch (ArgumentException e)
            {
                _logger.Error("App.config is likely missing databaseName in connection strings section", e);
                throw new DatabaseManagerException("App.config is likely missing databaseName in connection strings section", e);
            }
        }

        #region Public Methods
        /// <summary>
        /// Adds new application user to database and sets up initial data required
        /// </summary>
        /// <param name="username">The new user username</param>
        /// <returns>true if new user was added, false on failure (either user exists or database failed)</returns>
        public bool AddUser(string username)
        {
            CheckCommonParameters(username);

            using (var cmd = new SQLiteCommand(Connection))
            {
                //not considerable overhead, try adding table users if it doesn't exist
                cmd.CommandText = @"CREATE TABLE IF NOT EXISTS [Users] ([Username] TEXT NOT NULL PRIMARY KEY)";    
                cmd.ExecuteNonQuery();

                //add the user using parameters to avoid issues with user input string sanitizing
                cmd.CommandText = "INSERT INTO Users Values (@username)";
                cmd.Parameters.Add(new SQLiteParameter("@username", username));
                try
                {
                    cmd.ExecuteNonQuery(); // Execute the query

                    //add table for user words specific to user if new user was added and other relevant
                    CreateuserSupportTables(username);
                }
                catch (SQLiteException)
                {
                    return false;
                }
            }
            return true;
        }

        public List<string> GetAllUsers()
        {
            if (Connection.State == ConnectionState.Closed) throw new DatabaseManagerException("The connection must be open with Connect() method or in constructor");

            using (var cmd = new SQLiteCommand(@"SELECT username FROM Users", Connection))
            {
                var allUsers = new List<string>();
                try
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            allUsers.Add(reader["username"].ToString());
                    }
                }
                catch (SQLiteException e)
                {
                    _logger.Error("Missing 'Users' table in "+Connection.DataSource+".db", e);
                    throw new DatabaseManagerException("The database is missing users table", e);
                }
                return allUsers;
            }
        }

        /// <summary>
        /// Adds the word for specific user with weight of 1 or does nothing if the word exists
        /// Useful for words which may be triggered unknown number of times, 
        /// e.g. search terms (releveant once, but identical later searches should not distort data
        /// </summary>
        /// <param name="username">CurrentUser for who the word is recorded</param>
        /// <param name="word">The word to record</param>
        /// <param name="isPositive">Increments word weight is true, decrements if false</param>
        public void AddOrIgnoreWordWeightMapping(string username, string word, bool isPositive)
        {
            using (var trans = Connection.BeginTransaction())
            {
                try
                {
                    AddOrPrivateWordWeightMapping(trans, "IGNORE", username, word, isPositive);
                    trans.Commit();
                }
                catch (DatabaseManagerException)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        /// Adds the word for specific user with weight 1 or increments the existing value if word exists
        /// Useful for accumulating interest in words,
        /// e.g. clicking on specific advertisements
        /// </summary>
        /// <param name="username">CurrentUser for who the word is recorded</param>
        /// <param name="word">The word to record</param>
        /// <param name="isPositive">Increments word weight is true, decrements if false</param>
        public void AddOrUpdateWordWeightMapping(string username, string word, bool isPositive)
        {
            using (var trans = Connection.BeginTransaction())
            {
                try
                {
                    AddOrPrivateWordWeightMapping(trans, "REPLACE", username, word, isPositive);
                    trans.Commit();
                }
                catch (DatabaseManagerException)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        /// <summary>
        /// Optimised to perform a batch of inserts for set of words
        /// Increments/Decrements weights for existing words
        /// </summary>
        /// <param name="username">User</param>
        /// <param name="words">words to insert</param>
        /// <param name="isPositive">increment or decrement weight for words</param>
        public void BulkAddOrUpdateWordWeightMapping(string username, List<string> words, bool isPositive)
        {
            BulkAddOrPrivateWordWeightMapping("REPLACE", username, words, isPositive);
        }

        /// <summary>
        /// Optimised to perform a batch of inserts for set of words
        /// Ignores existing words
        /// </summary>
        /// <param name="username">User for who to insert</param>
        /// <param name="words">Words to insert</param>
        /// <param name="isPositive">increment if true, decrement if false</param>
        public void BulkAddOrIgnoreWordWeightMapping(string username, List<string> words, bool isPositive)
        {
            BulkAddOrPrivateWordWeightMapping("IGNORE", username, words, isPositive);
        }
        
        /// <summary>
        /// Loads dictionary of words to weights mapping, excluding any 0 weights
        /// </summary>
        /// <param name="username">The user for who to load weight mapping</param>
        /// <returns></returns>
        public Dictionary<string, int> GetUserWordWeightMappings(string username)
        {
            CheckCommonParameters(username);

            var words = new Dictionary<string, int>();
            using (var cmd = new SQLiteCommand(Connection))
            {
                cmd.CommandText = "SELECT * FROM Words_"+username+" WHERE Weight != 0;";

                using (var reader = cmd.ExecuteReader())
                {
                    while(reader.Read())
                        words.Add(reader.GetString(0), reader.GetInt32(1));
                }
            }
            return words;
        }

        /// <summary>
        /// Retrieves user history as a dictionary for datetime rounded to given time unit and most representative changes
        /// Example usage of method:
        /// GetUserHistoryByTimeUnit("Bob", TimeUnit.minutes, 15, 10)
        /// Would retrieve historical data for Bob aggregated in 15 minute sections for 5, thus if latest record was at 1/1/2015 15:00
        /// It would return same date with time 14:45:"...", 14:30:"...", 14:15:"....", 14:00:"...", 13:45:"..."
        /// </summary>
        /// <param name="username">User for who to retrieve history</param>
        /// <param name="timeUnit">The unit of time to split on</param>
        /// <param name="numberOfUnitsPerSection">The number of TimeUnits to pack per set of historical data</param>
        /// <param name="historyRecordLimit">The limit to number of instances to return, use 0 to set no limit</param>
        /// <returns>The dictionary of item per time unit value</returns>
        public Dictionary<DateTime, string> GetUserHistoryByTimeUnit(string username, TimeUnit timeUnit, int numberOfUnitsPerSection, int historyRecordLimit)
        {
            CheckCommonParameters(username);
            if (numberOfUnitsPerSection < 1) throw new ArgumentException("Value must be at least 1 record per group");
            if (historyRecordLimit < 0) throw new ArgumentException("Value must be positive or 0 for no limit");
            List<List<Tuple<DateTime, string>>> sections;

            // SELECT statements do in fact use the transaction system. This is used to acquire and release a 
            // SHARED lock on the database. As a result, it is indeed more efficient to wrap multiple SELECT 
            // statements in a single transaction. By doing so, the lock is only acquired and released once,
            // rather than for each individual SELECT statement. This ends up being slightly more efficient 
            // while also assuring that all SELECT statements will access the same version of the database 
            // in case something has been added/deleted by some other program.
            // http://stackoverflow.com/questions/7349189/optimizing-select-with-transaction-under-sqlite3
            using (var trans = Connection.BeginTransaction())
            {
                string maxDatetime = GetLogRecordDateString(username, "max", trans);
                string minDatetime = GetLogRecordDateString(username, "min", trans);
                if (maxDatetime == null) return new Dictionary<DateTime, string>();


                switch (timeUnit)
                {
                    case TimeUnit.seconds:
                        sections = GetHistoryForSmallTimeUnits(username, trans,
                            minDatetime, maxDatetime, new TimeSpan(0, 0, numberOfUnitsPerSection), historyRecordLimit);
                        break;
                    case TimeUnit.minutes:
                        sections = GetHistoryForSmallTimeUnits(username, trans,
                            minDatetime, maxDatetime, new TimeSpan(0, numberOfUnitsPerSection, 0), historyRecordLimit);
                        break;
                    case TimeUnit.hours:
                        sections = GetHistoryForSmallTimeUnits(username, trans,
                            minDatetime, maxDatetime, new TimeSpan(numberOfUnitsPerSection, 0, 0), historyRecordLimit);
                        break;
                    case TimeUnit.days:
                    case TimeUnit.months:
                    case TimeUnit.years:
                        sections = GetHistoryForLargeTimeUnits(username, trans, numberOfUnitsPerSection,
                            historyRecordLimit, minDatetime, maxDatetime, timeUnit);
                        break;
                    default:
                        throw new ArgumentException(
                            string.Format("The TimeUnit {0} enumeration is not valid for this method",timeUnit), 
                            "timeUnit");
                }
                trans.Commit(); //commit to end transaction (not sure if dispose ends it properly)
            }

            //select minimum date in each list of tuples for key, select first 3 most common words as value
            return sections.ToDictionary(_ => _.Min(___ => ___.Item1), _ => string.Join(", ", _.GroupBy(__ => __.Item2).OrderBy(___ => ___.Key).Take(3).Select(____ => ____.Key)));
        }

        public void RevertUserDataByDate(string username, DateTime rollbackDate)
        {
            CheckCommonParameters(username);

            using (var trans = Connection.BeginTransaction())
            using (var cmd = new SQLiteCommand{Transaction = trans})
            {
                try
                {
                    try
                    {
                        //get the weight mappings for all words to revert
                        var mappings = GetOldestWordWeightMappingsFromLog(cmd, username, rollbackDate);

                        //get all cases
                        var cases = string.Concat(mappings.Select(pair => string.Format(CaseTemplate, pair.Item1, pair.Item2)));

                        //when no history recorded after rollback date, just assume all is deleted
                        if (string.IsNullOrWhiteSpace(cases)) return;

                        //rollback all weights for username with all cases
                        cmd.CommandText = string.Format(RollbackAllWeightsTemplate, username, cases);
                    }
                    catch (SQLiteException e)
                    {
                        throw new DatabaseManagerException(
                            "Failed to load data to rollback to, if error persists after closing and restarting application contact support. Details written to log.",
                            e);
                    }

                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException e)
                    {
                        throw new DatabaseManagerException(
                            "Failed to rollback data, if error persists after closing and restarting application contact support. Details written to log.",
                            e);
                    }

                    try
                    {
                        //delete from log table
                        cmd.CommandText = string.Format(DeleteAllHistoryTemplate, username, rollbackDate);
                        cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException e)
                    {
                        throw new DatabaseManagerException(
                            "Failed to delete history after successful rollback, if error persists after closing and restarting application contact support. Details written to log.",
                            e);
                    }
                }
                catch (DatabaseManagerException e)
                {
                    _logger.Error(e.Message, e.InnerException);
                    trans.Rollback();               //rollback anything deleted
                    throw;                          //throw on without changing
                }
                //commit all changes if no exception
                trans.Commit();
            }
        }

        /// <summary>
        /// Returns a string of comma separated words which have the set of most negative weight value
        /// </summary>
        /// <param name="username">User for who to find negative words</param>
        /// <returns></returns>
        public string GetMostNegativeWordsOverall(string username)
        {
            CheckCommonParameters(username);
            return GetMostWordsByPolarity(username, "<", string.Empty);
        }

        /// <summary>
        /// Returns a string of comma separated words which have the set of most positive weight value
        /// </summary>
        /// <param name="username">User for who to find negative words</param>
        /// <returns></returns>
        public string GetMostPositiveWordsOverall(string username)
        {
            CheckCommonParameters(username);
            return GetMostWordsByPolarity(username, ">", "desc");
        }

        public string GetMostNegativeWordsInHistory(string username, DateTime maxDate, DateTime minDate)
        {
            CheckCommonParameters(username);
            return GetHistoryWordsByPolarity(username, maxDate, minDate, "<", string.Empty);
        }

        public string GetMostPositiveWordsInHistory(string username, DateTime maxDate, DateTime minDate)
        {
            CheckCommonParameters(username);
            return GetHistoryWordsByPolarity(username, maxDate, minDate, ">", "desc");
        }

        public void DeleteUser(string username)
        {
            CheckCommonParameters(username);
            using (var trans = Connection.BeginTransaction())
            using (var cmd = new SQLiteCommand { Transaction = trans })
            {
                var logTableName = string.Concat(username, "_log");
                var wordsTableName = string.Concat("Words_", username);

                try
                {
                    cmd.CommandText = string.Format(DropTableTemplate, logTableName);
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = string.Format(DropTableTemplate, wordsTableName);
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = string.Format(DeleteUserTemplate, username);
                    cmd.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (SQLiteException e)
                {
                    trans.Rollback();
                    _logger.Error("Cannot delete user",e);
                    throw new DatabaseManagerException("Cannot delete user, more detail in log file", e);
                }
            }
        }

        #endregion

        #region Private Internal Methods
        private void BulkAddOrPrivateWordWeightMapping(string insertType, string username, IEnumerable<string> words, bool isPositive)
        {
            CheckCommonParameters(username);
            if (words == null) throw new ArgumentNullException("words");

            using (var trans = Connection.BeginTransaction())
            {
                try
                {
                    foreach (var word in words)
                    {
                        AddOrPrivateWordWeightMapping(trans, insertType, username, word, isPositive);
                    }
                    trans.Commit();
                }
                catch (DatabaseManagerException)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        private void AddOrPrivateWordWeightMapping(SQLiteTransaction trans, string insertType, string username, string word, bool isPositive)
        {
            CheckCommonParameters(username);
            if (String.IsNullOrWhiteSpace(word)) throw new ArgumentException(string.Format(ArgumentEmptyString, "word"), "word");

            using (var cmd = new SQLiteCommand { Transaction = trans })
            {
                //username always comes from program itself sanitised on first login, 
                //the database is local user copy, hence user always has access to all data without sql injection
                string insert = string.Format("INSERT or {0} into Words_{1} (Word, Weight) " +
                                      "values (@word, coalesce ((SELECT Weight {2} from Words_{1} where word = @word ), {2}))",
                                    insertType, username, "{0}");

                cmd.CommandText = string.Format(insert, "+0");
                cmd.Parameters.Add(new SQLiteParameter("@word", word.ToUpperInvariant()));

                try
                {
                    cmd.ExecuteNonQuery(); // Execute the query
                    cmd.CommandText = string.Format(insert, (isPositive ? "+1" : "-1"));
                    cmd.ExecuteNonQuery(); // Execute the query
                }
                catch (SQLiteException e)
                {
                    _logger.Error("Failed to record user data", e);
                    throw new DatabaseManagerException("Failed to record user data, more detail in log", e);
                }
            }
        }

        /// <summary>
        /// Retrieves a single section of historical data, only used internally
        /// </summary>
        /// <param name="cmd">command to use for all selects</param>
        /// <param name="username">user</param>
        /// <param name="timeUnit">time unit</param>
        /// <param name="low">the value to check time &gt; datetime(maxDate, -low timeUnit)</param>
        /// <param name="up">the value to check time &lt; datetime(maxDate, -up timeUnit)</param>
        /// <param name="minDate">The date to measure history to(should be same as max if low and up are used for expected results)</param>
        /// <param name="maxDate">The date to measure history from</param>
        /// <returns>All resulting rows of time and word</returns>
        private List<Tuple<DateTime, string>> GetSingleUserHistorySectionByTimeUnit(SQLiteCommand cmd, string username, TimeUnit timeUnit, int low, int up, object minDate, object maxDate)
        {
            var words = new List<Tuple<DateTime, string>>();

            cmd.CommandText = string.Format(TimeBasedSelect, username, low, up, maxDate, minDate, timeUnit);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                    words.Add(new Tuple<DateTime, string>(reader.GetDateTime(0), reader.GetString(1)));
            }
            return words;
        }


        /// <summary>
        /// Get date string for log record for specific user 
        /// </summary>
        /// <param name="username">the user for which to query log table</param>
        /// <param name="minOrMax">min for earliest date, max for latest date</param>
        /// <param name="trans">transaction to use, uses connection if null</param>
        /// <returns>Returns the date string for oldest or latest log record based on input</returns>
        private string GetLogRecordDateString(string username, string minOrMax, SQLiteTransaction trans = null)
        {
            if(minOrMax != "min" && minOrMax != "max") throw new ArgumentException("must be 'min' or 'max'", "minOrMax");

            using (var cmd = (trans == null ? 
                new SQLiteCommand { Connection = this.Connection} : 
                new SQLiteCommand { Transaction = trans }))
            {
                cmd.CommandText = string.Format(SingleTimeSelect, username, minOrMax);
                var datevalue = cmd.ExecuteScalar(CommandBehavior.SingleRow);
                return datevalue == DBNull.Value ? null : (string)datevalue;
            }
        }

        /// <summary>
        /// Gets all log records for user and generates subset list based 
        /// on interval and history record limit.
        /// </summary>
        private List<List<Tuple<DateTime, string>>> GetHistoryForSmallTimeUnits(string username, 
            SQLiteTransaction transaction, string minDatetime, string maxDatetime, TimeSpan interval, int historyRecordLimit)
        {
            List<Tuple<DateTime, string>> result;

            //Get all log records from oldest to newest
            using (var cmd = new SQLiteCommand { Transaction = transaction })
            {
                result = GetSingleUserHistorySectionByTimeUnit(
                    cmd, username,
                    TimeUnit.seconds, 1, 0, minDatetime, maxDatetime);
            }

            var hasLimit = historyRecordLimit != 0;                                     //if has limit need to stop processing after reached in loop below 
            var currentBarrier = Convert.ToDateTime(maxDatetime).Subtract(interval);    //the bottom line time for first set of historical records to aggregate at
            var allSections = new List<List<Tuple<DateTime, string>>>();                //each set of aggregated records saved as section
            var section = new List<Tuple<DateTime, string>>();                          //holds single set of records within one interval

            //Get the records in order and enumerate it (the order is enumerable), not as expensive as sort
            var orderedEnumerator = result.OrderByDescending(_ => _.Item1).GetEnumerator();
 
            while (orderedEnumerator.MoveNext())
            {
                var item = orderedEnumerator.Current;
                if (item.Item1 > currentBarrier)            //within interval, keep adding to section
                {
                    section.Add(item);
                }
                else
                {
                    //keep correct interval
                    currentBarrier = currentBarrier.Subtract(interval);

                    //if nothing was found in previous interval just
                    //skip to closest item which is current
                    if (item.Item1 <= currentBarrier)
                        currentBarrier = item.Item1.Subtract(interval);

                    if (section.Count > 0)
                    {
                        if (hasLimit)
                        {
                            if (historyRecordLimit <= 1) break;
                            historyRecordLimit--;
                        }
                        allSections.Add(section);
                    }

                    //since current item was outside of interval it must be in next one
                    section = new List<Tuple<DateTime, string>> { item };
                }
            }
            //add last section the elements ran out at
            if (section.Count > 0)
            {
                allSections.Add(section);
            }
            return allSections;
        }

        private List<List<Tuple<DateTime, string>>> GetHistoryForLargeTimeUnits(string username, 
            SQLiteTransaction transaction, int numberOfUnitsPerSection, int historyRecordLimit, string minDatetime, string maxDatetime, TimeUnit timeUnit)
        {
            var low = numberOfUnitsPerSection;
            var up = 0;
            var hasNoLimit = historyRecordLimit == 0;
            var sections = new List<List<Tuple<DateTime, string>>>();

            using (var cmd = new SQLiteCommand{Transaction = transaction})
            {
                List<Tuple<DateTime, string>> result;
                var minDate = Convert.ToDateTime(minDatetime);
                do
                {
                    result = GetSingleUserHistorySectionByTimeUnit(cmd,
                        username, timeUnit, low, up, maxDatetime, maxDatetime);

                    if (result.Count > 0)
                    {
                        sections.Add(result);
                        --historyRecordLimit;
                    }

                    up = low;
                    low = low + numberOfUnitsPerSection;

                } while ((hasNoLimit || historyRecordLimit > 0) && !result.Exists(_ => _.Item1 == minDate));
            }
            return sections;
        }

        private void CreateuserSupportTables(string username)
        {
            ExecuteNonQueryByTemplate(username, WordTableTemplate);
            ExecuteNonQueryByTemplate(username, LogTableTemplate);
            ExecuteNonQueryByTemplate(username, TriggerTemplate);
        }

        private void ExecuteNonQueryByTemplate(string username, string template)
        {
            using (var cmd = new SQLiteCommand(Connection))
            {
                //only called internally so username is validated beofore
                cmd.CommandText = string.Format(template, username);

                try
                {
                    cmd.ExecuteNonQuery(); // Execute the query
                }
                catch (SQLiteException e)
                {
                    _logger.Error("Failed to record user data", e);
                    throw new DatabaseManagerException("Failed to record user data, more detail in log", e);
                }
            }
        }

        private IEnumerable<Tuple<string, int>> GetOldestWordWeightMappingsFromLog(SQLiteCommand cmd, string username, DateTime rollbackDate)
        {
            cmd.CommandText = string.Format(SelectHistoryWeightsTemplate, username, rollbackDate);

            var mappings = new List<Tuple<string, int>>(); 
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    mappings.Add(new Tuple<string, int>(reader.GetString(0), reader.GetInt32(1)));
                }
            }
            return mappings;
        }

        private string GetMostWordsByPolarity(string username, string operatorSymbol, string orderByString)
        {
            return GetWordsByCommand(string.Format(SelectWordsOrderedTemplate, username, operatorSymbol, orderByString));
        }

        private string GetHistoryWordsByPolarity(string username, DateTime maxDate, DateTime minDate, string operatorSymbol, string orderByString)
        {
            return GetWordsByCommand(string.Format(SelectWordsOrderedFromHistoryTemplate, username, operatorSymbol, maxDate, minDate, orderByString));
        }

        private string GetWordsByCommand(string command)
        {
            var words = new List<string>();
            using (var cmd = new SQLiteCommand(Connection))
            {
                cmd.CommandText = command;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                        words.Add(reader.GetString(0));
                }
            }
            var result = string.Join(", ", words);
            return string.Empty == result ? null : result;  //want to return null to signify no words exist, not a valid string
        }

        private void CheckCommonParameters(string username)
        {
            if (String.IsNullOrWhiteSpace(username)) throw new ArgumentException(string.Format(ArgumentEmptyString, "username"), "username");
            if (Connection.State == ConnectionState.Closed) throw new DatabaseManagerException("The connection must be open with Connect() method or in constructor");
        }

        #endregion

        public void Dispose()
        {
            if(Connection != null) Connection.Dispose();
        }
    }
}
