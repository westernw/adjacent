﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ADJAceNT.Entities;
using ADJAceNT.Helpers;
using MinimalCometLibrary;
using Newtonsoft.Json.Linq;

namespace ADJAceNT.Managers
{
    public class WebsiteDataReader : IDisposable
    {
        private readonly ImportIO _src;
        private readonly HashSet<string> _sessionQueries;
        private ConcurrentQueue<Advertisement> _advertisements;
        private WaitonlyCountdownEvent _mainCountdownLatch;
        private WaitonlyCountdownEvent _descriptionCountdownLatch;
        private bool _isQueryCancelled;
        private CancellationToken _latestCt;


        //The ConcurrentQueue original ToArray method calls internal ToList().ToArray() 
        //After comparing variants of new List(ToArray), ToList, ToArray.ToList in SimpleBenchmark 
        //can assure that this method is most effective in our use-case
        public List<Advertisement> Advertisements
        {
            get { return _advertisements.ToArray().ToList(); }//return isolated instance of data rather than direct reference to internal collection
        }

        /// <summary>
        /// Accessor for all search terms used in latest set of queries
        /// </summary>
        public List<string> QuerySearchTerms
        {
            get { return _sessionQueries.ToList(); }
        }

        /// <summary>
        /// Returns eventually consistent number of advertisements loaded
        /// or 0 if the loading is complete/not started
        /// </summary>
        public int CurrentAdsLoadedCount
        {
            get
            {
                return _mainCountdownLatch == null
                    ? -1
                    : _mainCountdownLatch.InitialCount - _mainCountdownLatch.CurrentCount;
            }
        }

        /// <summary>
        /// Returns eventually consistent number of advertisement descriptions loaded
        /// or 0 if the loading is complete/not started
        /// </summary>
        public int CurrentAdDescriptionsLoadedCount
        {
            get
            {
                return _descriptionCountdownLatch == null
                    ? 0
                    : _descriptionCountdownLatch.InitialCount - _descriptionCountdownLatch.CurrentCount;
            }
        }

        public WebsiteDataReader(ImportIO source)
        {
            _isQueryCancelled = false;
            _src = source;
            _advertisements = new ConcurrentQueue<Advertisement>();
            _sessionQueries = new HashSet<string>();
        }

        /// <summary>
        /// Loads the advertisements into the Advertisements collection in class
        /// </summary>
        /// <param name="searchTerms">Terms to search on websites selected</param>
        /// <param name="selectedConnectorGuids">The GUID for websites selected</param>
        /// <param name="ct">Cancellation token to cancel result load</param>
        public IWaitonlyCountdownEvent LoadResults(string[] searchTerms, List<string> selectedConnectorGuids, CancellationToken ct = default(CancellationToken))
        {
            _isQueryCancelled = false;
            _latestCt = ct;
            foreach( var term in searchTerms) _sessionQueries.Add(term);

            if (_mainCountdownLatch != null)
                _mainCountdownLatch.AddCount(searchTerms.Length);
            else
                _mainCountdownLatch = new WaitonlyCountdownEvent(searchTerms.Length);

            foreach (string term in searchTerms)
            {
                if (_latestCt.IsCancellationRequested)
                {
                    ClearResults();
                    return new WaitonlyCountdownEvent(0);
                }
                //cannot reuse dictionary as import.io takes the object as key and it must be new
                var jobsQuery = new Dictionary<string, Object>
                {
                    {"input", new Dictionary<string, String> {{"keywords", term}}},
                    {"connectorGuids", selectedConnectorGuids}
                };

                Task.Factory.StartNew(
                    () =>
                    {
                        bool recorded = true;
                        try
                        {
                            recorded = _src.DoQuery(jobsQuery, HandleQuery);
                        }
                        finally
                        {
                            if (!recorded && !_mainCountdownLatch.IsSet)
                                _mainCountdownLatch.Signal();
                        }
                    }, _latestCt);
            }
            return _mainCountdownLatch;
        }

        /// <summary>
        /// Populates the description of advertisements for websites which don't provide it on search page.
        /// Uses internal advertisements by default unless passed in explicitly, if passed in they overwritee the internal collection
        /// </summary>
        /// <param name="selectedDetailGuids">The collection of website-guid pairs</param>
        /// <param name="advertisements">All advertisements to populate, replace internal advertisement colelction</param>
        /// <param name="ct">token to be able to cancel long running retrieval of descriptions</param>
        /// <returns>Handle to wait on completion</returns>
        public IWaitonlyCountdownEvent PopulateDescriptions(Dictionary<string, string> selectedDetailGuids, List<Advertisement> advertisements, CancellationToken ct = default(CancellationToken))
        {
            _isQueryCancelled = false;
            _latestCt = ct;
            _advertisements = new ConcurrentQueue<Advertisement>(advertisements);
            if (_descriptionCountdownLatch != null)
                _descriptionCountdownLatch.AddCount(_advertisements.Count);
            else
                _descriptionCountdownLatch = new WaitonlyCountdownEvent(_advertisements.Count);

            //using for performance
            var hostGuidMapping = new Dictionary<string,string>();

            foreach (var ad in _advertisements)
            {
                if (_latestCt.IsCancellationRequested)
                {
                    ClearResults();
                    return new WaitonlyCountdownEvent(0);
                }

                if (ad.DescriptionSummary == null)
                {
                    string guid;
                    if (hostGuidMapping.ContainsKey(ad.JobLink.Host))   //if already cached use it
                    {
                        guid = hostGuidMapping[ad.JobLink.Host];
                    }
                    else //if not in dictionary, get and add
                    {
                        guid = selectedDetailGuids[ad.JobLink.Host.Substring(ad.JobLink.Host.IndexOf('.')+1)];//use host without www. to get guid
                        hostGuidMapping.Add(ad.JobLink.Host, guid);
                    }

                    var detailQuery = new Dictionary<String, Object>
                    {
                        {"input", new Dictionary<String, String> {{"webpage/url", ad.JobLink.AbsoluteUri}}},
                        {"connectorGuids", new List<String>{guid}}
                    };

                    Advertisement localAd = ad;
                    Task.Factory.StartNew(() =>
                    {
                        bool recorded = true;
                        try
                        {
                            recorded = _src.DoQuery(detailQuery,
                                new AdvertisementQuery(localAd, HandleAdvertisementQuery));

                            if (!recorded && localAd.DescriptionSummary == null) 
                                localAd.DescriptionSummary = "Retrieval Failed";
                        }
                        finally
                        {
                            if(!recorded && !_descriptionCountdownLatch.IsSet)
                                _descriptionCountdownLatch.Signal();
                        }
                    }, _latestCt);
                }
            }
            return _descriptionCountdownLatch;
        }

        private void HandleAdvertisementQuery(Query query, Dictionary<string, Object> message)
        {
            BaseHandler(query, message, DescriptionHandlerAction, _descriptionCountdownLatch);
        }

        /// <summary>
        /// private callback to handle data as it comes in, similar to how TIBCO Rendezvous (RV) works.
        /// </summary>
        /// <param name="query">original query</param>
        /// <param name="message">the resulting data</param>
        private void HandleQuery(Query query, Dictionary<string, Object> message)
        {
            BaseHandler(query, message, FullAdHandlerAction, _mainCountdownLatch);
        }

        /// <summary>
        /// Common handler template with slot for specific action
        /// </summary>
        /// <param name="query">Original query</param>
        /// <param name="message">Original Message</param>
        /// <param name="specificHandler">Specific action to take on data of message</param>
        /// <param name="relevantLatch">The countdown for main or description</param>
        private void BaseHandler(Query query, Dictionary<string, Object> message, Action<Query, JObject> specificHandler, CountdownEvent relevantLatch)
        {
            if (_isQueryCancelled && !query.IsFinished)
            {
                query.OnMessage(new Dictionary<string, object> { { "type", "CANCEL" } });
            }
            else if (_latestCt.IsCancellationRequested)
            {
                if (!_isQueryCancelled) ClearResults();
                return;
            }
            else if (message["type"].Equals("MESSAGE"))
            {
                specificHandler(query, (JObject) message["data"]);
            }

            if (query.IsFinished && !relevantLatch.IsSet) relevantLatch.Signal();
        }

        /// <summary>
        /// Action for whole advertisement loading
        /// </summary>
        /// <param name="query">Not used, added for compatiblity of signature</param>
        /// <param name="data">The data part of message</param>
        private void FullAdHandlerAction(Query query, JObject data)
        {
            JToken results;
            if (data.TryGetValue("results", out results))
            {
                foreach (JToken ad in results)
                {
                    _advertisements.Enqueue(ad.ToObject<Advertisement>());
                }
            }
        }

        /// <summary>
        /// Action for description loading
        /// </summary>
        /// <param name="specialQuery">The query subclass that has advertisement object publicly available</param>
        /// <param name="data">The data part of message</param>
        private void DescriptionHandlerAction(Query specialQuery, JObject data)
        {
            var query = (AdvertisementQuery)specialQuery;
            JToken results;
            if (data.TryGetValue("results", out results))
                if (results.HasValues)
                    query.Ad.DescriptionSummary = results.First.Value<string>("longdescription");
                else
                    query.Ad.DescriptionSummary = "No description found";
            else if (data.TryGetValue("error", out results))
                query.Ad.DescriptionSummary = results.ToString();
            else
                query.Ad.DescriptionSummary = "Retrieval Failed";
        }

        /// <summary>
        /// Aborts loading and waits until asynchronous result loading stops, empties the results
        /// </summary>
        public void ClearResults()
        {
            _isQueryCancelled = true;
            if(_mainCountdownLatch != null && !_mainCountdownLatch.IsSet) _mainCountdownLatch.Signal(_mainCountdownLatch.CurrentCount);
            if(_descriptionCountdownLatch != null && !_descriptionCountdownLatch.IsSet) _descriptionCountdownLatch.Signal(_descriptionCountdownLatch.CurrentCount);
            _src.Disconnect();
            _src.Connect();
            _advertisements = new ConcurrentQueue<Advertisement>();
            _sessionQueries.Clear();
        }

        /// <summary>
        /// Release object resources, should be called when done using the object
        /// Allows object to be utilise the "using" construct 
        /// </summary>
        public void Dispose()
        {
            ClearResults(); //to avoid exceptions in loading if Dispose called explicitly in middle of loading
            if(_src != null) _src.Disconnect();
            if(_mainCountdownLatch != null) _mainCountdownLatch.Dispose();
            if(_descriptionCountdownLatch != null) _descriptionCountdownLatch.Dispose();
        }
    }
}
