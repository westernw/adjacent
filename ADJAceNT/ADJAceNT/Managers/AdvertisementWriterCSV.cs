﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using ADJAceNT.Entities;

namespace ADJAceNT.Managers
{
    class AdvertisementWriterCsv : AdvertisementBaseCsv
    {
        private const string FilenameFormat = "search_{0:dd'_'MM'_'yy'-'HH'_'mm'_'ss'_'fff}{1}.csv";
        private const string EscapedQuote = "\"";

        /// <summary>
        /// Saves advertisements to csv file in folder provided or default folder
        /// </summary>
        /// <param name="advertisements">All advertisements to save to single file</param>
        /// <param name="user">The user who is currently saving advertisements</param>
        /// <param name="folderpath">Saves to default application folder 'searches' subfolder if omitted</param>
        /// <param name="filenameSuffix">if passed in, added to end of filename before extension</param>
        public static void Write(List<Advertisement> advertisements, string user, string folderpath = null, string filenameSuffix = null)
        {
            if (folderpath == null) folderpath = ApplicationPath;

            var subfolder = Path.Combine(folderpath, "searches", user);
            if (!Directory.Exists(subfolder))
                Directory.CreateDirectory(subfolder);

            var csvLines = new ConcurrentQueue<string>(new List<string>{ Headers });
            Parallel.ForEach(advertisements, _ => csvLines.Enqueue(AdToCsvRow(_)));
            File.WriteAllLines(Path.Combine(subfolder, string.Format(FilenameFormat, DateTime.Now, filenameSuffix)), csvLines);
        }

        internal static string AdToCsvRow(Advertisement ad)
        {
            return string.Join(",",
                new List<string>
                {
                    StringToCsvCell(ad.Company),
                    StringToCsvCell(ad.DescriptionSummary),
                    StringToCsvCell(ad.JobTitle),
                    StringToCsvCell(ad.Salary),
                    StringToCsvCell(ad.Location),
                    StringToCsvCell(ad.JobLink.AbsoluteUri)
                });
        }

        //Adopted from http://stackoverflow.com/questions/6377454/escaping-tricky-string-to-csv-format
        //we want to quote all strings and escape double quotes so that it is easy to read in and complies with csv format
        private static string StringToCsvCell(string str)
        {
            bool mustQuote = (str.Contains(",") || str.Contains(EscapedQuote) || str.Contains("\r") || str.Contains("\n"));
            if (mustQuote)
            {
                var sb = new StringBuilder();
                sb.Append(EscapedQuote);
                foreach (char nextChar in str)
                {
                    if (nextChar == '"')
                        sb.Append(EscapedQuote).Append(EscapedQuote);
                    else
                        sb.Append(nextChar);
                }
                sb.Append(EscapedQuote);
                return sb.ToString();
            }
            return string.Concat(EscapedQuote, str, EscapedQuote);
        }
    }
}