﻿using System;
using System.IO;
using System.Linq;
using ADJAceNT.Entities;
using LAIR.ResourceAPIs.WordNet;

namespace ADJAceNT.Managers
{
    class SynonymManager
    {
        private readonly WordNetEngine _wordNetEngine;

        public SynonymManager(string pathToWordNet = null)
        {
            // create wordnet engine (use disk-based retrieval)
            _wordNetEngine = new WordNetEngine(pathToWordNet ?? Path.Combine(ApplicationDataStore.InstallationRoot, @"Dictionaries\WordNet_3.1"), false);
        }

        /// <summary>
        /// Uses WordNet API to retrieve most likely synonyms for noun and return as array of strings
        /// </summary>
        /// <param name="word">the word for which to find synonyms, always returned as part of set</param>
        /// <returns>array of synonyms or empty array if none found</returns>
        public string[] GetMostLikelySynonymGroup(string word)
        {
            try
            {
                var synonyms = _wordNetEngine.GetMostCommonSynSet(word, WordNetEngine.POS.Noun).Words;
                if(!synonyms.Contains(word.Replace(' ', '_')))
                    synonyms.Add(word);

                return synonyms.Select(_ => _.Replace('_',' ')).ToArray();
            }
            catch (Exception) //GetMostCommonSynSet throws on null or more than one most common (which should not happen)
            {
                return new[] {word};
            }
        }
    }
}
