﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using ADJAceNT.Entities;
using Microsoft.Win32;

namespace ADJAceNT.Managers
{
    class AdvertisementReaderCsv : AdvertisementBaseCsv
    {
        private readonly static string[] Separator = new []{"\",\""};

        /// <summary>
        /// Opens a file selection dialog and loads advertisement files selected
        /// </summary>
        /// <param name="username">User who loads files</param>
        /// <param name="ct">Optional cancellation mechanism in case of many files</param>
        /// <returns></returns>
        public static List<Advertisement> OpenFileDialogAndLoad(string username, CancellationToken ct = default(CancellationToken))
        {
            var ads = new List<Advertisement>();

            var openFileDialog = new OpenFileDialog
            {
                Title = "Select the search file(s) to load",
                InitialDirectory = Path.Combine(ApplicationPath, "searches", username),
                Filter = "Search files (*.csv)|*.csv",
                Multiselect = true,
                CheckFileExists = true,
                CheckPathExists = true
            };

            if(openFileDialog.ShowDialog() == true)
            {
                foreach (var file in openFileDialog.FileNames)
                {
                    if(ct.IsCancellationRequested) return new List<Advertisement>();

                    try
                    {
                        ads.AddRange(Read(file, false));
                    }
                    catch (ArgumentException){ }
                }
            }
            return ads;
        } 

        /// <summary>
        /// Reads a csv file written with <see cref="T:ADJAceNT.Managers.AdvertisementWriterCsv"/> 
        /// into a list of <see cref="T:ADJAceNT.Entities.Advertisement"/> objects
        /// </summary>
        /// <param name="filename">Filename with extension when <para>useDefualtPath</para> is true, or full path when useDefaultPath is false</param>
        /// <param name="useDefaultPath">True to prepend default application path or false to use just the filename specified</param>
        /// <param name="hasHeader">True if csv file has header (does if written by Writer mentioned), False to signify that there is no header line in csv</param>
        /// <returns>List of Advertisement object loaded from file</returns>
        public static List<Advertisement> Read(string filename, bool useDefaultPath = true, bool hasHeader = true)
        {
            if (useDefaultPath)
                filename = Path.Combine(ApplicationPath, filename);

            if(!File.Exists(filename))
                throw new ArgumentException("The file specified does not exist", filename);

            var lines = File.ReadAllLines(filename);
            var ads = new List<Advertisement>(lines.Length-1);

            //start at 1 to skip header or at 0 if no header (in C# cannot cast bool to int directly)
            for(int i = hasHeader ? 1 : 0; i < lines.Length; ++i)
            {
                ads.Add(CsvRowToAd(lines[i]));
            }

            return ads;
        }

        internal static Advertisement CsvRowToAd(string csvRow)
        {
            csvRow = csvRow.Trim('"');
            var colValues = csvRow.Split(Separator, StringSplitOptions.RemoveEmptyEntries);
            if(colValues.Length != 6) throw new ArgumentException("Invalid row is csv file:"+csvRow);

            return new Advertisement
            {
                Company = colValues[0],
                DescriptionSummary = colValues[1],
                JobTitle = colValues[2],
                Salary = colValues[3],
                Location = colValues[4],
                JobLink = new Uri(colValues[5])
            };
        }
    }
}