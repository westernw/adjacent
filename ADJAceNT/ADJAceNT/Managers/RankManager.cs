﻿using System;
using System.Collections.Generic;
using System.Linq;
using ADJAceNT.Entities;
using ADJAceNT.Helpers;

namespace ADJAceNT.Managers
{
    public class RankManager : IRankManager
    {
        private readonly IDataManager _userData;
        private readonly RankComparer _comparer;
        private string _currentUser;

        public string CurrentUser
        {
            get { return _currentUser; }
            set { _currentUser = value ?? ""; }
        }

        /// <summary>
        /// Class that allows ranking of Advertisement object based on user data
        /// </summary>
        /// <param name="username">Current User for who the advertisements will be ranked, can be assigned later if not provided</param>
        /// <param name="userDataManager">Current User data manager, default database used if not supplied</param>
        public RankManager(string username = "", IDataManager userDataManager = null)
        {
            CurrentUser = username;
            _userData = userDataManager ?? new DatabaseManager();
            _comparer = new RankComparer();
        }

        /// <summary>
        /// Sorts advertisements for user property set, if no user set, sorted using searchTerms only
        /// </summary>
        /// <param name="advertisements">List of advertisements to sort</param>
        /// <param name="searchTerms">Relevant search terms which produced the advertisements or provide bias to sort</param>
        /// <returns>a sorted list of advertisements based on user data and search terms</returns>
        public List<Advertisement> RankAdvertisements(List<Advertisement> advertisements, List<string> searchTerms)
        {
            var ads = advertisements.Select(_ => _.DeepClone()).ToList();
            var latestWordRankings = _currentUser.Equals("") ? new Dictionary<string,int>() : _userData.GetUserWordWeightMappings(_currentUser);

            /* https://www.informit.com/guides/content.aspx?g=dotnet&seqNum=781
             * Given the choice between passing an IComparer<T> reference or a Comparison delegate to Array.Sort, 
             * always prefer the IComparer<T>. There are two reasons for that. First, as you've seen, 
             * passing the default IComparer performs much better than passing the default comparison method.
             * 
             * The second reason has a smaller performance impact. Array.Sort will wrap a Comparison delegate 
             * in an intermediate object, which then requires an extra redirection (or two) in order to access. 
             * The result is about a 1% increase in runtime. */

            //want to efficiently use them with our uppercase description
            var upperSearchTerms = searchTerms.Select(_ => _.ToUpperInvariant()).ToList();

            foreach (var ad in ads)
            {
                if (ad.DescriptionSummary == null)
                    ad.DescriptionSummary = "Retrieval Failed";
                
                //make description upper as in bag of words
                var desc = ad.DescriptionSummary.ToUpperInvariant();
                
                //If the unique location is in bag of words it indicates user likes the location
                int weight;
                foreach (string word in ad.Location.ToUpperInvariant().SplitByPunctuation().Distinct())
                {
                    if (latestWordRankings.TryGetValue(word, out weight))
                    {
                        ad.Rank += weight;
                    }
                }

                //For all words in description, add weights if applicable
                foreach (var word in desc.SplitByPunctuation())
                {
                    if (latestWordRankings.TryGetValue(word, out weight))
                    {
                        ad.Rank += weight;
                    }
                    if (upperSearchTerms.Any(_ => word.IndexOf(_, StringComparison.Ordinal) >= 0))
                    {
                        ad.Rank ++;
                    }
                }

                //if salary does not have a set value decrease the rank as such advertisements are not as interesting
                if (!ad.Salary.Any(char.IsDigit))
                {
                    var fivePercent = Math.Abs(ad.Rank / 100);   //drop rank value by about 1% or minimum of 1
                    ad.Rank -= fivePercent > 0 ? fivePercent : 1; 
                }
            }

            //now sort all advertisements with own comparer on Rank from above
            ads.Sort(_comparer);

            return ads;
        }

        class RankComparer : IComparer<Advertisement>
        {
            public int Compare(Advertisement x, Advertisement y)
            {
                if (x.Rank > y.Rank)    //if rank is higher it needs to go to start of list
                    return -1;
                
                //if rank is lower towards end of list (if equal we put it next for stable sort)
                return 1;
            }
        }
    }
}
