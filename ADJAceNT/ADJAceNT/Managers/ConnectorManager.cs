﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;

namespace ADJAceNT.Managers
{
    static class ConnectorManager
    {
        /// <summary>
        /// Retrieves all string key-value pairs from config file for given section name
        /// </summary>
        /// <param name="groupName">config section name</param>
        /// <returns>string pairs from section or null if no section found, empty dictionary if section is empty</returns>
        internal static Dictionary<string, string> GetConnectorsByGroupName(string groupName)
        {
            var connectors = ConfigurationManager.GetSection(groupName) as NameValueCollection;
            if (connectors == null) return null;

            var connectorDict = new Dictionary<string, string>(connectors.Count);
            foreach (var key in connectors.AllKeys)
            {
                connectorDict.Add(key, connectors[key]);
            }
            return connectorDict;
        }
    }
}
