using System;
using System.Collections.Generic;
using ADJAceNT.Enums;

namespace ADJAceNT.Managers
{
    public interface IDataManager : IDisposable
    {
        /// <summary>
        /// Connects to the data source, creates new if not found
        /// </summary>
        void Connect();

        /// <summary>
        /// Adds new application user to data source and sets up initial data required
        /// </summary>
        /// <param name="username">The new user username</param>
        bool AddUser(string username);

        /// <summary>
        /// Returns all recorded usernames
        /// </summary>
        /// <returns>All users or empty list</returns>
        List<string> GetAllUsers();

        Dictionary<string, int> GetUserWordWeightMappings(string username);

        void AddOrIgnoreWordWeightMapping(string username, string word, bool isPositive);
        void AddOrUpdateWordWeightMapping(string username, string word, bool isPositive);
        void BulkAddOrIgnoreWordWeightMapping(string username, List<string> word, bool isPositive);
        void BulkAddOrUpdateWordWeightMapping(string username, List<string> word, bool isPositive);

        Dictionary<DateTime, string> GetUserHistoryByTimeUnit(string username, TimeUnit timeUnit,
            int numberOfUnitsPerSection, int historyRecordLimit);

        void RevertUserDataByDate(string username, DateTime rollBackToDate);
        string GetMostNegativeWordsOverall(string username);
        string GetMostPositiveWordsOverall(string username);
        string GetMostNegativeWordsInHistory(string username, DateTime maxDate, DateTime minDate);
        string GetMostPositiveWordsInHistory(string username, DateTime maxDate, DateTime minDate);
        void DeleteUser(string username);
    }
}