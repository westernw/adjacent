﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using ADJAceNT.ThirdParty;
using ADJAceNT.ViewModel;

namespace ADJAceNT.View
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            InitializeComponent();
        }

        private void FocusInputBox()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Input,
            new Action(delegate
            {
                UsernameComboBox.Focus(); Keyboard.Focus(UsernameComboBox); // Set Keyboard Focus
            })); 
        }

        /// <summary>
        /// Will set keyboard focus on input whenever login view becomes visible and will reload list of usernames
        /// </summary>
        /// <param name="sender">parent control of the view</param>
        /// <param name="e">visibility event</param>
        private void OnLoginVisible(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool) e.NewValue)
            {
                var vm = DataContext as LoginViewModel;
                if (vm != null)
                {
                    var allUsers = vm.AllUsers.ToList();
                    allUsers.Sort();
                    UsernameComboBox.AutoCompleteManager.DataProvider = new SimpleStaticDataProvider(allUsers);
                    UsernameComboBox.ItemsSource = allUsers;
                }
                UsernameComboBox.AutoCompleteManager.AutoAppend = true;
                UsernameComboBox.AutoCompleteManager.Asynchronous = true;

                FocusInputBox();
            }
        }
    }
}
