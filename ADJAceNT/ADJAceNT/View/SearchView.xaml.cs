﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using ADJAceNT.ViewModel;

namespace ADJAceNT.View
{
    /// <summary>
    /// Interaction logic for SearchView.xaml
    /// </summary>
    public partial class SearchView : UserControl
    {
        public SearchView()
        {
            InitializeComponent();
            DataContextChanged += OnDataContextChanged;
        }

        /// <summary>
        /// When data context is assigned method subscribes to focusing event
        /// </summary>
        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var publisher = DataContext as SearchViewModel;
            if (publisher != null) publisher.ResetFocus += ResetFocus;
        }

        //Sets both focus on search box
        public void ResetFocus(object sender, EventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Input,
            new Action(delegate
            {
                SearchTextBox.Focus();         // Set Logical Focus
                Keyboard.Focus(SearchTextBox); // Set Keyboard Focus
            }));
        }
    }
}
