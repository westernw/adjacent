﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Principal;
using System.Windows;
using System.Windows.Threading;
using ADJAceNT.Entities;
using log4net;

namespace ADJAceNT.View
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            //if running from program files, require admin elevation, otherwise run
            //http://www.blackwasp.co.uk/CheckAdmin.aspx
            var currentFolder = new Uri(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            bool isElevated = principal.IsInRole(WindowsBuiltInRole.Administrator);
            if (!isElevated && currentFolder.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)))
            {
                var admin = Process.GetCurrentProcess();
                admin.StartInfo.UseShellExecute = true;
                admin.StartInfo.Verb = "runas";
                admin.StartInfo.FileName = Path.Combine(currentFolder, "ADJAceNT.exe");
                admin.Start();
                Process.GetCurrentProcess().Kill();
            }

            this.Dispatcher.UnhandledException += LastHopeHandler;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            
            //setting up log4net
            GlobalContext.Properties["LogPath"] = Path.Combine(ApplicationDataStore.InstallationRoot, "logs");
            GlobalContext.Properties["AppVersion"] = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            GlobalContext.Properties["AppName"] = Assembly.GetExecutingAssembly().GetName();
            GlobalContext.Properties["UserName"] = Environment.GetEnvironmentVariable("USERNAME");

            log4net.Config.XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile));
        }
        
        private void LastHopeHandler(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            LogManager.GetLogger("GeneralLogFileAppender").Error("Application was forced to fail", e.Exception);

            string errorMessage = string.Format("Sorry for inconvenience: {0}", e.Exception.Message);
            WebExceptionDefault(e.Exception, errorMessage);
            e.Handled = true;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;

            const string errorMessage = "Sorry for inconvenience: {0}";
            if (ex != null)
            {
                LogManager.GetLogger("GeneralLogFileAppender").Error("Application was forced to fail", ex);
                WebExceptionDefault(ex, string.Format(errorMessage, ex.Message));
            }
            else
            {
                LogManager.GetLogger("GeneralLogFileAppender").Error("Application was forced to fail" + e.ExceptionObject);
                MessageBox.Show(string.Format(errorMessage, e.ExceptionObject), "Uncaught Thread Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            //cannot supress issue, but exit without crash window
            Environment.Exit(1);
        }

        private void WebExceptionDefault(Exception ex, string customMessage)
        {
            if (Dispatcher.CheckAccess())
            {
                DisplayError(ex, customMessage);
            }
            else
            {
                Dispatcher.Invoke(new Action(() => DisplayError(ex, customMessage)), DispatcherPriority.Render);
            }
        }

        private void DisplayError(Exception ex, string customMessage)
        {
            if (ex is WebException)
                MessageBox.Show(this.MainWindow,
                    "Please check your internet connection and try later\nDetail: " + customMessage, "Error",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            else
                MessageBox.Show(this.MainWindow, customMessage+Environment.NewLine+
                    "Please reinstall application or contact developer if unsure:"+Environment.NewLine+
                    "aleksandrskurjanovs@gmail.com", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
