﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace ADJAceNT.View
{
    /// <summary>
    /// Interaction logic for MainScreen.xaml
    /// </summary>
    public partial class MainScreen : UserControl
    {
        private readonly Action _resetAndFitDelegate;

        public MainScreen()
        {
            _resetAndFitDelegate = () =>
            {
                FitToSize();
                ScrollToTop();
            };
            InitializeComponent();
            AdListView.IsVisibleChanged += OnAdVisible;
        }

        /// <summary>
        /// When listview becomes visible we subscribe to event of advertisements loading
        /// </summary>
        private void OnAdVisible(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            AdListView.TargetUpdated += ListView_FitToSize;//subscribe to search event
            AdListView.IsVisibleChanged -= OnAdVisible;
        }

        private void ListView_FitToSize(object sender, EventArgs e)
        {
            if (AdListView.Dispatcher.CheckAccess())
            {
                // This thread has access so it can update the UI thread.
                FitToSize();
                ScrollToTop();
            }
            else
            {
                // This thread does not have access to the UI thread. Propogate to UI thread.
                AdListView.Dispatcher.BeginInvoke(DispatcherPriority.Render, _resetAndFitDelegate);
            }
        }

        private void FitToSize()
        {
            var gridView = AdListView.View as GridView;
            var actualWidth = AdListView.ActualWidth - SystemParameters.VerticalScrollBarWidth; //get total width

            //reset width before recalculating last column to force update values
            for (int i = 0; i < gridView.Columns.Count - 1; i++)
            {
                gridView.Columns[i].Width = double.NaN;
            }
            AdListView.UpdateLayout();

            //keep subtracting width of all columns up to last
            for (int i = 0; i < gridView.Columns.Count - 1; i++)
            {
                actualWidth = actualWidth - gridView.Columns[i].ActualWidth;
            }
            gridView.Columns[gridView.Columns.Count - 1].Width = actualWidth;   //set last column to width remaining
        }

        private void ScrollToTop()
        {
            if (AdListView.Items.Count > 0) //if there are items
            {
                AdListView.Items.MoveCurrentToFirst();                  //select first
                AdListView.ScrollIntoView(AdListView.Items.CurrentItem);//scroll to it
            }
        }
    }
}
