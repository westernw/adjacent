﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using ADJAceNT.ViewModel;

namespace ADJAceNT.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainViewModel _lazyContext;
        private readonly TaskScheduler _ui;

        public MainWindow()
        {
            InitializeComponent();

            _lazyContext = new MainViewModel();
            _lazyContext.ObjectLoaded += OnDataContextChanged;
            DataContext = _lazyContext;
            _ui = TaskScheduler.FromCurrentSynchronizationContext();
        }

        private void OnDataContextChanged(object sender, EventArgs eventArgs)
        {          
            //makes all bindings re-evaluate (because viewmodel finished loading) in UI thread
            Task.Factory.StartNew(() =>
            {
                DataContext = null;
                DataContext = _lazyContext;
            }, CancellationToken.None, TaskCreationOptions.None, _ui);

            //makes main screen visible
            Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.SplashScreen.Visibility = Visibility.Collapsed;
                this.SplashScreen = null;
                this.MainScreen.Visibility = Visibility.Visible;
            }));

            //unsubcribe from event as we won't need it anymore, avoid leaking resources
            _lazyContext.ObjectLoaded -= OnDataContextChanged;
        }
    }
}
