﻿using System.Windows.Controls;
using ADJAceNT.Entities;

namespace ADJAceNT.View
{
    /// <summary>
    /// Interaction logic for UserHistoryView.xaml
    /// </summary>
    public partial class UserHistoryView : UserControl
    {
        private bool _manualSelection;

        public UserHistoryView()
        {
            _manualSelection = false;
            InitializeComponent();
        }

        private void AdjustHistoryView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_manualSelection) return;

            _manualSelection = true;
            if (e.AddedItems.Count == 1)
            {
                AdjustSelectionTo_OnSelected(e.AddedItems[0] as HistoryItem);
            }
            else if (e.RemovedItems.Count == 1)
            {
                AdjustSelectionFrom_OnUnselected(e.RemovedItems[0] as HistoryItem);
            }
            _manualSelection = false;
        }

        private void AdjustSelectionTo_OnSelected(HistoryItem selectedItem)
        {
            foreach (HistoryItem item in HistoryListView.Items)
            {
                if (!ShouldAdjustItem(item, selectedItem, true))
                    break;
            }
        }

        private void AdjustSelectionFrom_OnUnselected(HistoryItem unselectedItem)
        {
            for (int i = HistoryListView.Items.Count-1; i >= 0; i--)
            {
                if (!ShouldAdjustItem(HistoryListView.Items[i] as HistoryItem, unselectedItem, false))
                    break;
            }
        }

        private bool ShouldAdjustItem(HistoryItem item, HistoryItem itemEffected, bool gotSelected)
        {
            if (item.Equals(itemEffected)) return false;

            var container = (ListViewItem)HistoryListView.ItemContainerGenerator.ContainerFromItem(item);
            container.IsSelected = gotSelected;
            return true;
        }
    }
}
