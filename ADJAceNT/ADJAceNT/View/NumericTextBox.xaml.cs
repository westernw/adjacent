﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using ADJAceNT.Helpers;

namespace ADJAceNT.View
{
    /// <summary>
    /// Interaction logic for NumericTextBox.xaml
    /// </summary>
    public partial class NumericTextBox : UserControl
    {
        public static readonly DependencyProperty TitleProperty =
           DependencyProperty.Register("Title", typeof(string), typeof(NumericTextBox), new UIPropertyMetadata(string.Empty));

        public static readonly DependencyProperty TitleLocationProperty =
           DependencyProperty.Register("TitleLocation", typeof(Dock), typeof(NumericTextBox), new UIPropertyMetadata(Dock.Right));

        public static readonly DependencyProperty TextProperty =
           DependencyProperty.Register("Text", typeof(string), typeof(NumericTextBox), new UIPropertyMetadata(string.Empty));

        public static readonly DependencyProperty InputDelegateCommandProperty =
           DependencyProperty.Register("InputDelegateCommand", typeof(DelegateCommand), typeof(NumericTextBox), new UIPropertyMetadata(null));

        public static readonly DependencyProperty IsNegativeValidProperty =
           DependencyProperty.Register("IsNegativeValid", typeof(bool), typeof(NumericTextBox), new UIPropertyMetadata(true));

        public static readonly DependencyProperty IsDecimalValidProperty =
           DependencyProperty.Register("IsDecimalValid", typeof(bool), typeof(NumericTextBox), new UIPropertyMetadata(true));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public Dock TitleLocation
        {
            get { return (Dock)GetValue(TitleLocationProperty); }
            set { SetValue(TitleLocationProperty, value); }
        }

        public DelegateCommand InputDelegateCommand
        {
            get { return (DelegateCommand)GetValue(InputDelegateCommandProperty); }
            set { SetValue(InputDelegateCommandProperty, value); }
        }

        public bool IsNegativeValid
        {
            get { return (bool)GetValue(IsNegativeValidProperty); }
            set { SetValue(TextProperty, value); }
        }

        public bool IsDecimalValid
        {
            get { return (bool)GetValue(IsDecimalValidProperty); }
            set { SetValue(TitleProperty, value); }
        }
        
        public NumericTextBox()
        {
            InitializeComponent();
        }

        private void ValidateCharacter_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //don't handle anything except allowed keys
            e.Handled = true;
            var textBox = sender as TextBox;

            if (textBox == null) return;

            var strKey = e.Text;
            //allowed chars - . 0 1 2 3 4 5 6 7 8 9
            //- only as first char when IsNegativeValid
            //. only once per text when IsDecimalValid
            //digits at any time except don't allow zeroes prepended for non-decimals

            if (IsNegativeValid && strKey == "-" && string.IsNullOrEmpty(textBox.Text))
            {
                e.Handled = false;
            }

            if (IsDecimalValid && strKey == "." && !textBox.Text.Contains("."))
            {
                e.Handled = false;
            }

            if (strKey != null && char.IsDigit(strKey, 0))
            {
                e.Handled = false;
            }

            if (textBox.Text != null && textBox.Text.Length == 1 && textBox.Text[0] == '0' && !IsDecimalValid)
            {
                textBox.Text = "";
            }
        }

        private void UpdateExplicitBinding_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var textBox = sender as TextBox;

            if (textBox == null) return;

            BindingExpression be = textBox.GetBindingExpression(TextBox.TextProperty);
            be.UpdateSource();
        }
    }
}
