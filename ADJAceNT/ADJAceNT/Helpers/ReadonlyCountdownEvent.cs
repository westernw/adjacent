﻿using System;
using System.Threading;

namespace ADJAceNT.Helpers
{
    /// <summary>
    /// Intended to interface with outside class users who must 
    /// wait of the event when use of custom events within holder
    /// of object is not feasible, no other operations other than
    /// wait are available
    /// </summary>
    public interface IWaitonlyCountdownEvent
    {
        void Wait();
        void Wait(CancellationToken cancellationToken);
        bool Wait(TimeSpan timeout);
        bool Wait(TimeSpan timeout, CancellationToken cancellationToken);
        bool Wait(int millisecondsTimeout);
        bool Wait(int millisecondsTimeout, CancellationToken cancellationToken);
    }

    /// <summary>
    /// Class that is used to wrap full CountdownEvent object to inherit interface
    /// sealed to prevent use to further inherit
    /// </summary>
    internal sealed class WaitonlyCountdownEvent : CountdownEvent, IWaitonlyCountdownEvent
    {
        public WaitonlyCountdownEvent(int initialCount) : base(initialCount)
        {}
    }
}
