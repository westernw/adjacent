﻿using System.Windows.Threading;

namespace ADJAceNT.Helpers
{
    class WPFRenderHelper
    {
        /// <summary>
        /// Method forces the UI to update, intended to be used before long-running thread/task
        /// Origin: used similar concept at work when had a long loading task right after UI binding update
        /// and needed to refresh it
        /// </summary>
        public static void ForceUiUpdate()
        {
            Dispatcher.CurrentDispatcher.Invoke(() => { }, DispatcherPriority.ContextIdle);
        }
    }
}
