﻿using System;
using System.Windows.Input;

namespace ADJAceNT.Helpers
{
    /// <summary>
    /// Wrapper for lambda methods to be used as UI control commands
    /// Helps in supporting the MVVM pattern
    /// </summary>
    public class DelegateCommand : ICommand
    {
        private readonly Action<object> _command;

        public DelegateCommand(Action command)
        {
            _command = wrapper => command();
        }

        public DelegateCommand(Action<object> command)
        {
            _command = command;
        }

        public bool CanExecute(object parameter)
        {
            if (parameter is bool) return (bool)parameter;
            return true;
        }

        public void Execute(object parameter)
        {
            _command(parameter);
        }

        public event EventHandler CanExecuteChanged;
    }
}
