﻿using System;

namespace ADJAceNT.Helpers
{
    public static class StringHelper
    {
        private static readonly char[] Punctuation = new[] { ' ', '.', ',', ';', ':', '-', '(', ')', '\'', '\"', '*', '/' };
            
        /// <summary>
        /// Splits the string on ' ', '.', ',', ';', ':', '-', '(', ')', '\'', '\"', '*'
        /// Removes empty entries and returns result
        /// </summary>
        /// <param name="source">The string to split</param>
        /// <returns>array of resulting strings after split</returns>
        public static string[] SplitByPunctuation(this string source)
        {
            return source.Split(Punctuation, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
