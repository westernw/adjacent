﻿using System.ComponentModel;

namespace ADJAceNT.Helpers
{
    /// <summary>
    /// Class that wraps the PropertyChanged functionality and can be inherited by any class that needs to use PropertyChanged
    /// </summary>
    public abstract class BasePropertyChanged : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
