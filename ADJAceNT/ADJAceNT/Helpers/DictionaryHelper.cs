﻿using System.Collections.Generic;
using System.Linq;

namespace ADJAceNT.Helpers
{
    public static class DictionaryHelper
    {
        /// <summary>
        /// Method for IDictionary key-value type with value of type string
        /// Adds the value with specified key if not already present, otherwise updates value if new value is a longer string
        /// </summary>
        /// <typeparam name="TKey">Any key type</typeparam>
        /// <param name="dict">object the method is invoked on</param>
        /// <param name="key">the key</param>
        /// <param name="value">the string value to add or update</param>
        public static void AddOrUpdateIfLonger<TKey>(this IDictionary<TKey,string> dict, TKey key, string value)
        {
            if (dict.ContainsKey(key))
            {
                if(dict[key].Length < value.Length)
                    dict[key] = value;
            }
            else
            {
                dict.Add(key, value);
            }
        }

        /// <summary>
        /// Returns true if disctionary is null or empty, false otherwise
        /// </summary>
        public static bool IsNullOrEmpty<TKey, TValue>(this Dictionary<TKey, TValue> dict)
        {
            return dict == null || dict.Count == 0;
        }

        /// <summary>
        /// Takes an IEnumerable of keys as input and returns matched values
        /// as IEnumerable
        /// Any keys not found will not be used
        /// </summary>
        public static IEnumerable<TValue> GetValuesByKeys<TKey, TValue>(this Dictionary<TKey, TValue> dict,
            IEnumerable<TKey> keys)
        {
            return dict.Keys.Intersect(keys).Select(_ => dict[_]);
        }
    }
}
