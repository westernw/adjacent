﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ADJAceNT.Entities;
using ADJAceNT.Enums;
using ADJAceNT.Managers;

namespace ADJAceNT.Model
{
    public class UserHistoryModel
    {
        private readonly IDataManager _dataManager;
        private List<HistoryItem> _selectedHistory;

        /// <summary>
        /// Returns independent new collection of selected items, to affect items directly use methods provided
        /// </summary>
        public List<HistoryItem> SelectedHistory { 
            get { return _selectedHistory.ToList(); }
            set { _selectedHistory = value; }
        } 

        /// <summary>
        /// Returns Observablecollection the ViewModel can use for binding, represents all history loaded
        /// The collection is never reset, hence binding is never broken within class
        /// </summary>
        public ObservableCollection<HistoryItem> History { get; private set; }

        /// <summary>
        /// Comma separated string of words that have highest negative 
        /// effect on rank of advertisements in currently loaded history 
        /// </summary>
        public string MostNegativeWordsInHistory { get; private set; }

        /// <summary>
        /// Comma separated string of words that have highest positive
        /// effect on rank of advertisements in curretly loaded history
        /// </summary>
        public string MostPositiveWordsInHistory { get; private set; }

        /// <summary>
        /// Comma separated string of words that have highest negative 
        /// effect on rank of advertisements in whole database for CurrentUser
        /// </summary>
        public string MostNegativeWordsOverall { get; private set; }

        /// <summary>
        /// Comma separated string of words that have highest positive
        /// effect on rank of advertisements in whole database for CurrentUser
        /// </summary>
        public string MostPositiveWordsOverall { get; private set; }

        public string CurrentUser { get; set; }

        public UserHistoryModel(IDataManager dataManager)
        {
            _dataManager = dataManager;
            History = new ObservableCollection<HistoryItem>();
            _selectedHistory = new List<HistoryItem>();
        }

        /// <summary>
        /// Sets all current History loaded as selected
        /// </summary>
        public void SelectAll()
        {
            _selectedHistory = new List<HistoryItem>(History);
        }

        /// <summary>
        /// No items will be selected returned by SelectedHistory
        /// </summary>
        public void UnSelectAll()
        {
            _selectedHistory = new List<HistoryItem>();
        }

        /// <summary>
        /// Clears both selected and loaded history
        /// </summary>
        public void ClearHistoryLoaded()
        {
            MostNegativeWordsInHistory = null;
            MostPositiveWordsInHistory = null;
            MostNegativeWordsOverall = null;
            MostPositiveWordsOverall = null;
            UnSelectAll();
            History.Clear();
        }

        /// <summary>
        /// Loads the history with specified parameters
        /// Returns to signify if any results were loaded, 
        /// if no results exist, previous state of data is kept
        /// </summary>
        /// <param name="selectedTime">The time unit to base query on</param>
        /// <param name="numberOfUnitsPerGroup">Group into N time units</param>
        /// <param name="limitOfGroups">Max number of groups to return, 0 for no limit</param>
        /// <returns>True if new history loaded, False if no history was loaded and previous is kept</returns>
        public bool LoadHistoryByTimeUnit(TimeUnit selectedTime, int numberOfUnitsPerGroup, int limitOfGroups)
        {
            var results = _dataManager.GetUserHistoryByTimeUnit(CurrentUser, selectedTime, numberOfUnitsPerGroup, limitOfGroups);
            var resultsExist = results.Count > 0;
            if (resultsExist)
            {
                ClearHistoryLoaded();
                //add to history loaded in order
                foreach (var pair in results.OrderByDescending(_ => _.Key))
                {
                    History.Add(new HistoryItem(pair.Key, pair.Value));
                }
                if (History.Count > 0)
                {
                    var maxDate = History.First().ItemDate;
                    var minDate = History.Last().ItemDate;
                    MostNegativeWordsInHistory = _dataManager.GetMostNegativeWordsInHistory(CurrentUser, maxDate, minDate);
                    MostPositiveWordsInHistory = _dataManager.GetMostPositiveWordsInHistory(CurrentUser, maxDate, minDate);
                }
            }
            MostNegativeWordsOverall = _dataManager.GetMostNegativeWordsOverall(CurrentUser);
            MostPositiveWordsOverall = _dataManager.GetMostPositiveWordsOverall(CurrentUser);
            return resultsExist;
        }

        /// <summary>
        /// Uses the SelectedHistory to delete the items permanently
        /// </summary>
        public void DeleteSelectedPermanently(DateTime rollBackToDate)
        {
            _dataManager.RevertUserDataByDate(CurrentUser, rollBackToDate);
            ClearHistoryLoaded();
            MostNegativeWordsOverall = _dataManager.GetMostNegativeWordsOverall(CurrentUser);
            MostPositiveWordsOverall = _dataManager.GetMostPositiveWordsOverall(CurrentUser);
        }

        public void DeleteCurrentProfilePermanently()
        {
            _dataManager.DeleteUser(CurrentUser);
            ClearHistoryLoaded();
            CurrentUser = string.Empty;
        }
    }
}
