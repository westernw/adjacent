﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using ADJAceNT.Enums;

/* Short explanaition
 *  
 * good g_btn 	c                                   if good                     if none
 * bad	g_btn	c                                   	if u_btn                	if u_btn
 * none g_btn	v                                   		Visible             		Collapsed
 * good b_btn	c                                   	else                    	else
 * bad	b_btn	c                                   		Collapsed           		Visible
 * none b_btn	v                  -->              if bad              -->     else
 * good u_btn	v                                   	if u_btn                	if u_btn
 * bad	u_btn	v                                   		Visible             		Visible
 * none u_btn	c                                   	else                    	else 
 *                                                  		Collapsed           		Collapsed
 * gg || bg || gb || bb || nu == Collapsed          if none
 * ng || nb || gu || bu 	  == Visible           	    if u_btn
                                                    		Collapsed
                                                    	else
                                                    		Visible
 */
namespace ADJAceNT.Converters
{
    [ValueConversion(typeof(AdvertisementStatus), typeof(Visibility), ParameterType = typeof(string))]
    class AdvertisementStatusButtonVisibilityStatusConverter : IValueConverter
    {
        /// <summary>
        /// Returns appropriate visibility for button based on advertisement status and button parameter passed in
        /// </summary>
        /// <param name="value">Advertisement Status</param>
        /// <param name="targetType">Not used</param>
        /// <param name="parameter">Undo for undo button, any string otherwise</param>
        /// <param name="culture">Not used</param>
        /// <returns><see cref="F:System.Windows.Visibility.Visible"/> or <see cref="F:System.Windows.Visibility.Collapsed"/> based on input parameters</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) throw new ArgumentException("No object value parameters can be null");

            var status = (AdvertisementStatus)value;
            var isUndoBtn = parameter.Equals("Undo");
            return (status == AdvertisementStatus.None && isUndoBtn) || (status != AdvertisementStatus.None && !isUndoBtn) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
