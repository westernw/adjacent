﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace ADJAceNT.Converters
{
    [ValueConversion(typeof(bool), typeof(Visibility), ParameterType = typeof(bool))]
    public class InvertableBooleanToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// Converts a Boolean value to a <see cref="T:System.Windows.Visibility"/> enumeration value.
        /// </summary>
        /// <param name="value">The Boolean value to convert. This value can be a standard Boolean value.</param><param name="targetType">This parameter is not used.</param><param name="parameter">Inverts conversion if true (i.e. true value, true parameter returns collapsed.</param><param name="culture">This parameter is not used.</param>
        /// <returns>
        /// <see cref="F:System.Windows.Visibility.Visible"/> if <paramref name="value"/> is true and parameter is true; otherwise, <see cref="F:System.Windows.Visibility.Collapsed"/>.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) throw new ArgumentException("No object value parameters can be null");

            return (bool)value ^ (bool)parameter ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
