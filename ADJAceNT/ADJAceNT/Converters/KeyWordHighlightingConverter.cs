﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using ADJAceNT.Helpers;

namespace ADJAceNT.Converters
{
    [ValueConversion(typeof(string), typeof(Visibility))]
    class KeyWordHighlightingConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Any(_ => _ == null)) throw new ArgumentException("No object values parameters can be null");

            //the line is required as I use datacontext which is unloaded for performance by framework
            //explained in https://social.msdn.microsoft.com/Forums/vstudio/en-US/36aec363-9e33-45bd-81f0-1325a735cc45/what-is-disconnecteditem-and-what-is-causing-it?forum=wpf
            //and few related threads, as of .NET 4.5 below static object is exposed to check against
            if (values[0] == BindingOperations.DisconnectedSource) return null;

            var descriptionSummary = values[0] as string;
            var searchWords = values[1] as List<string>;

            if (descriptionSummary == null || searchWords == null) throw new ArgumentException("Values[0] must be of string and value[1] must be of List<string> types");
 
            string wholeDescription = descriptionSummary;
            var tb = new TextBlock { TextWrapping = TextWrapping.Wrap };
            var allSearchWordsSeparated = new HashSet<string>();

            foreach (var word in searchWords)
            {
                var words = word.Split(' ');
                foreach (var w in words) allSearchWordsSeparated.Add(w.ToUpper());
            }

            var highlights = new Dictionary<int, string>();

            //for each word scan whole description recording highlights
            foreach (var word in allSearchWordsSeparated)
            {
                int wordIndex = wholeDescription.IndexOf(word, StringComparison.OrdinalIgnoreCase);

                while (wordIndex != -1)
                {
                    highlights.AddOrUpdateIfLonger(wordIndex, word);    //own extension method
                    wordIndex = wholeDescription.IndexOf(word, wordIndex + word.Length, StringComparison.OrdinalIgnoreCase);
                }
            }

            //make all highlights sorted to apply them easily
            var highlightsSorted = new SortedList<int, string>(highlights);

            int lastIndex = 0;
            //for all highlights in order, apply them to textblock
            foreach (var highlightPair in highlightsSorted)
            {
                var distanceToHighlight = highlightPair.Key - lastIndex;
                //if didn't already highlight in case of subword match 
                //e.g. INTERNET highlighted, now NET comes with Key 
                //pointing to middle of interNET when lastIndex is 
                //already on char after the word INTERNET
                if (distanceToHighlight > 0)    
                {
                    tb.Inlines.Add(wholeDescription.Substring(lastIndex, distanceToHighlight));
                    tb.Inlines.Add(new Bold(new Run(wholeDescription.Substring(highlightPair.Key, highlightPair.Value.Length))));
                    lastIndex = highlightPair.Key + highlightPair.Value.Length;
                }
            }

            //add what is left over after last highlight
            if (wholeDescription.Length > lastIndex)
                tb.Inlines.Add(wholeDescription.Substring(lastIndex));

            return tb;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
