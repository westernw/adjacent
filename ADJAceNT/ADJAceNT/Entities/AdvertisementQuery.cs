﻿using MinimalCometLibrary;

namespace ADJAceNT.Entities
{
    class AdvertisementQuery : Query
    {
        public Advertisement Ad { get; private set; }

        public AdvertisementQuery(Advertisement ad, QueryHandler queryCallback)
            : base(queryCallback)
        {
            Ad = ad;
        }
    }
}
