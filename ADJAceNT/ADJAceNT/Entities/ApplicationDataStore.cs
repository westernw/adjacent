﻿using System;
using System.IO;
#if DEBUG
using System.Reflection;
#endif

namespace ADJAceNT.Entities
{
    class ApplicationDataStore
    {
        private static readonly string _installationRoot;

        static ApplicationDataStore()
        {
            _installationRoot =
#if DEBUG
            new Uri(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;
#else
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ADJAceNT");
#endif
        }

        public static string InstallationRoot
        {
            get
            {
                return _installationRoot;
            }
        }
    }
}
