﻿using System;

namespace ADJAceNT.Entities
{
    public class HistoryItem
    {
        public HistoryItem() { }

        public HistoryItem(DateTime itemDate, string words)
        {
            ItemDate = itemDate;
            CommonWords = words;
        }

        public DateTime ItemDate { get; set; }
        public string CommonWords { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((HistoryItem) obj);
        }

        protected bool Equals(HistoryItem other)
        {
            return ItemDate.Equals(other.ItemDate) && string.Equals(CommonWords, other.CommonWords);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                //common 397 prime value to use when generating general-quality hashes 
                //accepted answer http://stackoverflow.com/questions/5059994/custom-type-gethashcode
                return (ItemDate.GetHashCode() * 397) ^ (CommonWords != null ? CommonWords.GetHashCode() : 0);
            }
        }

        public static bool operator ==(HistoryItem left, HistoryItem right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(HistoryItem left, HistoryItem right)
        {
            return !Equals(left, right);
        }
    }
}