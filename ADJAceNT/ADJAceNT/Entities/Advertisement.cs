﻿using System;
using ADJAceNT.Enums;
using ADJAceNT.Helpers;

namespace ADJAceNT.Entities
{
    /// <summary>
    /// Holds advertisements, used in JSON serialisation
    /// </summary>
    public class Advertisement : BasePropertyChanged
    {
        public string Company { get { return _company ?? Na; } set { _company = value; } }
        public string DescriptionSummary { get; set; }
        public string JobTitle { get { return _jobTitle ?? Na; } set {_jobTitle = value; } }
        public string Salary { get { return _salary ?? Na; } set { _salary = value; } }
        public string Location { get { return _location ?? Na; } set { _location = value; } }
        public Uri JobLink { get; set; }

        public int Rank { get; set; }
        public AdvertisementStatus Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        private const string Na = "Not Specified";
        private string _company;
        private string _jobTitle;
        private string _salary;
        private string _location;
        private AdvertisementStatus _status = AdvertisementStatus.None;

        /// <summary>
        /// Returns true only if Job advertisement links are same or if object instance is same
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var ad = obj as Advertisement;
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (ad == null) return false;
            return ad.JobLink == JobLink;
        }

        public override int GetHashCode()
        {
            return JobLink.GetHashCode();//job link is not intended to ever be null, if it is it will throw exception explicitly and can be solved
        }

        public Advertisement DeepClone()
        {
            return new Advertisement
            {
                _company = _company,
                _jobTitle = _jobTitle,
                _location = _location,
                _salary = _salary,
                _status = _status,
                DescriptionSummary = DescriptionSummary,
                JobLink = JobLink,
                Rank = Rank
            };
        }

        public static bool operator ==(Advertisement left, Advertisement right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Advertisement left, Advertisement right)
        {
            return !Equals(left, right);
        }
        
        public override string ToString()
        {
            return "Company:" + Company + ", Summary:" + DescriptionSummary + ", Title:" + JobTitle + ", Link:" +
                   JobLink + ", Salary:" + Salary + ", Location:" + Location;
        }
    }
}
