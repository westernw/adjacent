﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using ADJAceNT.Entities;
using ADJAceNT.Enums;
using ADJAceNT.Exceptions;
using ADJAceNT.Helpers;
using ADJAceNT.Model;

namespace ADJAceNT.ViewModel
{
    public class UserHistoryViewModel : BasePropertyChanged
    {
        private readonly string _installationPath;
        private readonly INotificationBarViewModel _notificationViewModel;
        private readonly UserHistoryModel _dataModel;
        private bool _isViewOpen;
        private string _selectedTimeUnit;
        private string _numberOfUnitsPerGroup;
        private string _limitOfGroups;
        private string _lastNotificationMessage;
        private NotificationType _lastNotificationType;

        public ObservableCollection<HistoryItem> History { get { return _dataModel.History; } }
        public List<HistoryItem> SelectedHistory
        {
            get { return _dataModel.SelectedHistory; }
            set { _dataModel.SelectedHistory = value; }
        }
        public List<string> TimeUnits { get; private set; }
        public DelegateCommand LoadHistoryByParameters { get; private set; }
        public DelegateCommand DeleteHistorySelected { get; private set; }
        public DelegateCommand DeleteUserProfile { get; private set; }
        public DelegateCommand ExitToMainScreen { get; private set; }
        public DelegateCommand CloseView { get; private set; }
        public DelegateCommand OpenView { get; private set; }
        public DelegateCommand OpenFolderInExplorer { get; private set; }

        public string SelectedTimeUnit
        {
            get { return _selectedTimeUnit; }
            set { _selectedTimeUnit = value; OnPropertyChanged("SelectedTimeUnit"); }
        }

        public string NumberOfUnitsPerGroup
        {
            get { return _numberOfUnitsPerGroup; }
            set { _numberOfUnitsPerGroup = value; OnPropertyChanged("NumberOfUnitsPerGroup"); }
        }

        public string LimitOfGroups
        {
            get { return _limitOfGroups; }
            set { _limitOfGroups = value; OnPropertyChanged("LimitOfGroups"); }
        }

        public string MostNegativeWordsInHistory { get { return _dataModel.MostNegativeWordsInHistory; } }

        public string MostPositiveWordsInHistory { get { return _dataModel.MostPositiveWordsInHistory; } }

        public string MostNegativeWordsOverall { get { return _dataModel.MostNegativeWordsOverall; } }

        public string MostPositiveWordsOverall { get { return _dataModel.MostPositiveWordsOverall; } }

        public bool IsViewOpen
        {
            get { return _isViewOpen; }
            set
            {
                if (_isViewOpen != value)
                {
                    _isViewOpen = value;
                    OnPropertyChanged("IsViewOpen");
                    if (_isViewOpen) OnViewOpened();
                    else OnViewClosed();
                }
            }
        }

        public UserHistoryViewModel(INotificationBarViewModel notificationViewModel, UserHistoryModel model)
        {
            _installationPath = ApplicationDataStore.InstallationRoot;
            IsViewOpen = false;
            _notificationViewModel = notificationViewModel;
            _dataModel = model;
            //doing manually instead of Enum.GetNames to keep order desired as the 
            //method is not guaranteed to keep an order and re-ordering would be unneccesary cost
            TimeUnits = new List<string> { "Select Time Unit", "seconds", "minutes", "hours", "days", "months", "years" };
            SelectedTimeUnit = TimeUnits[0];

            LoadHistoryByParameters = new DelegateCommand(LoadHistory);
            ExitToMainScreen = new DelegateCommand(() => IsViewOpen = false);
            DeleteHistorySelected = new DelegateCommand(OnDeleteSelected);
            DeleteUserProfile = new DelegateCommand(OnDeleteUserProfile);
            CloseView = new DelegateCommand(BackToMainMenu);
            OpenView = new DelegateCommand(HistoryViewOpened);
            OpenFolderInExplorer = new DelegateCommand(o => OpenInExplorer(o.ToString()));
        }

        private void HistoryViewOpened()
        {
            switch (_lastNotificationType)
            {
                case NotificationType.Info:
                    _notificationViewModel.Info(_lastNotificationMessage);
                    break;
                case NotificationType.Warning:
                    _notificationViewModel.Warning(_lastNotificationMessage);
                    break;
                case NotificationType.Error:
                    _notificationViewModel.Error(_lastNotificationMessage);
                    break;
            }
            IsViewOpen = true;
        }

        /// <summary>
        /// Opens folder with user subfolder in windows explorer
        /// </summary>
        /// <param name="folder">main folder at installation root</param>
        private void OpenInExplorer(string folder)
        {
            try
            {
                string argument = Path.Combine(_installationPath, folder, _dataModel.CurrentUser);
                System.Diagnostics.Process.Start(argument);
            }
            catch (Exception)
            {
                _notificationViewModel.Error(string.Format("Cannot open {0} folder, perhaps you didn't save any {0} since installing application", folder));
            }
        }

        /// <summary>
        /// Action to delete whole user profile
        /// </summary>
        /// Notes: considering the time constraints and singularity of such message-box occurance
        /// it was decided against using Inversion of Control, ThirdParty APIs, or even
        /// an extra ~5 classes in order to make this method compliant with MVVM structure
        /// Instead the dialog box is placed right here. In other circumstances it would
        /// have been best implemented differently.
        private void OnDeleteUserProfile()
        {
            var result = MessageBox.Show("If you press OK, the \"" + _dataModel.CurrentUser + "\" user profile will be deleted.\n(your search files will be kept)",
                "Delete Profile Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);

            if (result == MessageBoxResult.OK)
            {
                try
                {
                    _dataModel.DeleteCurrentProfilePermanently();
                    UpdateMostCommon();
                    UpdateInHistory();
                    IsViewOpen = false;
                    OnUserDeleted();
                }
                catch (DatabaseManagerException e)
                {
                    _notificationViewModel.Error(e.Message);
                }
            }
        }

        private void OnDeleteSelected(object items)
        {
            var selectedItems = items as IList;
            if (selectedItems == null || selectedItems.Count == 0)
            {
                _notificationViewModel.Warning("Click on record to delete the history to that point");
                return;
            }

            _dataModel.SelectedHistory = selectedItems.Cast<HistoryItem>().ToList();
            try
            {
                _dataModel.DeleteSelectedPermanently(_dataModel.SelectedHistory.Min(_ => _.ItemDate));
                UpdateMostCommon();
                UpdateInHistory();
            }
            catch (DatabaseManagerException e)
            {
                _notificationViewModel.Error(e.Message);
            }
        }

        private void BackToMainMenu()
        {
            _lastNotificationMessage = _notificationViewModel.Message;
            _lastNotificationType = _notificationViewModel.CurrentNotificationType;
            _dataModel.ClearHistoryLoaded();
            IsViewOpen = false;
        }

        private void LoadHistory()
        {
            int integerLimit;
            if (SelectedTimeUnit == TimeUnits[0]) { _notificationViewModel.Warning("Please select the time unit from drop-down"); return; }
            if (string.IsNullOrWhiteSpace(NumberOfUnitsPerGroup)) { _notificationViewModel.Warning("Please specify time unit interval"); return; }
            if (string.IsNullOrWhiteSpace(LimitOfGroups)) _notificationViewModel.Warning("No limit specified, all history will be loaded");
            if (int.TryParse(LimitOfGroups, out integerLimit) && integerLimit < 0) _notificationViewModel.Warning("The limit must be positive or 0 for no limit");

            TimeUnit selectedTime;
            if (TimeUnit.TryParse(SelectedTimeUnit, out selectedTime))
            {
                try
                {
                    if (_dataModel.LoadHistoryByTimeUnit(selectedTime, int.Parse(NumberOfUnitsPerGroup), integerLimit))
                    {
                        //display explanaition of what is loaded
                        _notificationViewModel.Info(
                            string.Format("History displayed grouped in sets of {0} {1} with {2} limit on number of groups",
                            NumberOfUnitsPerGroup, SelectedTimeUnit, integerLimit > 0 ? LimitOfGroups : "no"));
                        UpdateInHistory();
                    }
                    else
                    {
                        _notificationViewModel.Info(
                            string.Format("There curretly exists no history recorded for {0}", _dataModel.CurrentUser));
                    }
                    UpdateMostCommon();
                }
                catch (ArgumentException e)
                {
                    _notificationViewModel.Warning(e.Message);
                }
            }
            else
                _notificationViewModel.Info("How did you get here? Please specify time unit interval");
        }

        public void Reset()
        {
            _dataModel.ClearHistoryLoaded();
            SelectedTimeUnit = TimeUnits.First();
            NumberOfUnitsPerGroup = null;
            LimitOfGroups = null;
            UpdateMostCommon();
            UpdateInHistory();
            IsViewOpen = false;
        }

        private void UpdateMostCommon()
        {
            OnPropertyChanged("MostNegativeWordsOverall");
            OnPropertyChanged("MostPositiveWordsOverall");
        }

        private void UpdateInHistory()
        {
            OnPropertyChanged("MostNegativeWordsInHistory");
            OnPropertyChanged("MostPositiveWordsInHistory");
        }

        private void ClearHistoryLoaded()
        {
            _dataModel.ClearHistoryLoaded();
        }

        public event EventHandler ViewClosed;
        protected virtual void OnViewClosed()
        {
            EventHandler handler = ViewClosed;
            if (handler != null) handler(this, null);
        }

        public event EventHandler ViewOpened;
        protected virtual void OnViewOpened()
        {
            EventHandler handler = ViewOpened;
            if (handler != null) handler(this, null);
        }

        public event EventHandler UserDeleted;
        protected virtual void OnUserDeleted()
        {
            EventHandler handler = UserDeleted;
            if (handler != null) handler(this, null);
        }
    }
}
