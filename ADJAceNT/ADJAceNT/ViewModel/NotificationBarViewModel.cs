﻿using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ADJAceNT.Helpers;

namespace ADJAceNT.ViewModel
{
    public class NotificationBarViewModel :BasePropertyChanged, INotificationBarViewModel
    {
        private readonly object _safetyLock = new object();

        private string _message;
        public string Message 
        { 
            get { return _message; }
            private set
            {
                _message = value;
                OnPropertyChanged("Message");
            }
        }

        private ImageSource _icon;
        public ImageSource Icon
        {
            get 
            { 
                lock (_safetyLock)
                {
                    return _icon;
                }
            }
            private set
            {
                lock (_safetyLock)
                {
                    _icon = value;
                }
                OnPropertyChanged("Icon");
            }
        }

        public NotificationType CurrentNotificationType
        {
            get
            {
                lock (_safetyLock)
                {
                    //Intended reference comparison as all icons 
                    //are internal and are not modified from outside
                    return _icon == _infoIcon
                        ? NotificationType.Info
                        : _icon == _warnIcon
                            ? NotificationType.Warning
                            : _icon == _errorIcon
                                ? NotificationType.Error
                                : NotificationType.None;
                }
            }
        }

        private readonly ImageSource _errorIcon;
        private readonly ImageSource _warnIcon;
        private readonly ImageSource _infoIcon;

        public NotificationBarViewModel()
        {
            //Reference http://msdn.microsoft.com/en-us/library/system.windows.interop.imaging.createbitmapsourcefromhicon%28v=vs.110%29.aspx 
            //create BitmapImageSource from system icon
            _errorIcon = Imaging.CreateBitmapSourceFromHIcon(SystemIcons.Error.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            _warnIcon = Imaging.CreateBitmapSourceFromHIcon(SystemIcons.Warning.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            _infoIcon = Imaging.CreateBitmapSourceFromHIcon(SystemIcons.Information.Handle, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }

        public void Error(string message)
        {
            Message = message;
            Icon = _errorIcon;
        }

        public void Warning(string message)
        {
            Message = message;
            Icon = _warnIcon;
        }

        public void Info(string message)
        {
            Message = message;
            Icon = _infoIcon;
        }
        
        public void Empty()
        {
            Message = string.Empty;
            Icon = null;
        }
    }

    public enum NotificationType
    {
        None = 0,
        Info,
        Warning,
        Error
    }
}
