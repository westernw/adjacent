﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ADJAceNT.Entities;
using ADJAceNT.Helpers;
using ADJAceNT.Managers;
using MinimalCometLibrary;

namespace ADJAceNT.ViewModel
{
    public class SearchViewModel : BasePropertyChanged
    {
        private bool _isConnected;
        private bool _canSearch;
        private string _searchTerm;
        private List<string> _websitesAvailable;
        private List<string> _selectedWebsites;
        private List<Advertisement> _advertisements;
        private string _statsInfo;
        private WebsiteDataReader _reader;
        private readonly SynonymManager _synonymManager;
        private readonly ImportIO _searchSource;
        private readonly INotificationBarViewModel _notificationViewModel;
        private readonly Dictionary<string, string> _connectorWebsites;
        private readonly Dictionary<string, string> _detailConnectorWebsites;
        private readonly IRankManager _rankMgr;
        private CancellationTokenSource _cancelSearchSource;
        private List<string> _allSearchTerms;
        private bool _saveSearches;

        public bool IsConnected
        {
            get { return _isConnected; }
            set 
            { 
                _isConnected = value; 
                OnPropertyChanged("IsConnected"); 
                if(!_isConnected) NotConnectedMessage();
            }
        }

        public bool CanSearch
        {
            get { return _canSearch; }
            set { _canSearch = value; OnPropertyChanged("CanSearch"); }
        }

        public string SearchTerm
        {
            get { return _searchTerm; }
            set { _searchTerm = value; OnPropertyChanged("SearchTerm"); }
        }

        public List<string> AllSearchTerms
        {
            get { return _allSearchTerms; }
            set { _allSearchTerms = value; OnPropertyChanged("AllSearchTerms"); }
        }

        public List<string> WebsitesAvailable
        {
            get { return _websitesAvailable; }
            private set { _websitesAvailable = value; OnPropertyChanged("WebsitesAvailable"); }
        }

        public List<string> SelectedWebsites
        {
            get { return _selectedWebsites; }
            private set { _selectedWebsites = value; OnPropertyChanged("SelectedWebsites"); }
        }

        public DelegateCommand Search { get; private set; }
        public DelegateCommand CancelSearch { get; private set; }
        public DelegateCommand ReconnectAction { get; private set; }
        public DelegateCommand LoadFromFiles { get; private set; }

        public List<Advertisement> Advertisements
        {
            get { return _advertisements; } 
            private set { _advertisements = value; OnPropertyChanged("Advertisements"); }
        }

        public string StatsInfo
        {
            get { return _statsInfo; } 
            set { _statsInfo = value; OnPropertyChanged("StatsInfo"); }
        }

        public bool SaveSearches
        {
            get { return _saveSearches; }
            set { _saveSearches = value; OnPropertyChanged("SaveSearches"); }
        }

        public SearchViewModel(INotificationBarViewModel notificationViewModel, IRankManager rankMgr)
        {
            _rankMgr = rankMgr;
            _notificationViewModel = notificationViewModel;
            _connectorWebsites = ConnectorManager.GetConnectorsByGroupName("Connectors");
            _detailConnectorWebsites = ConnectorManager.GetConnectorsByGroupName("DetailConnectors");

            if( _connectorWebsites.IsNullOrEmpty())
                _notificationViewModel.Error("The website information is missing please reinstall the program or contact support");
            else if (_detailConnectorWebsites.IsNullOrEmpty())
                _notificationViewModel.Error("The advertisement detail information is missing please reinstall the program or contact support");
            else
            {
                CanSearch = true;
                AllSearchTerms = new List<string>();
                _synonymManager = new SynonymManager();
                WebsitesAvailable = _connectorWebsites.Keys
#if !DEBUG
                    .Where(_ => !_.Contains("debug"))
#endif
                    .ToList();
                SelectedWebsites = new List<string> { "All Selected" };
                Search = new DelegateCommand(SearchData);
                CancelSearch = new DelegateCommand(CancelSearching);
                ReconnectAction = new DelegateCommand(Reconnect);
                LoadFromFiles = new DelegateCommand(LocalFileLoad);
                _searchSource = new ImportIO("https://query.import.io", Guid.Parse("e05b7387-426b-497a-800d-3b7259b4a908"), "/dwEkV1dXDpTywyyTtvRa8gI1l0xi8ybgtRx+KFF0c3FwAaergbEYlYuWPfVrhFMxrpxQXB96rWlJ4v7Ol/aHw==");
                _searchSource.Disconnected += (s,e) => Disconnected();
                Reconnect();
            }
        }

        private void LocalFileLoad()
        {
            if (string.IsNullOrWhiteSpace(_searchTerm))
            {
                _notificationViewModel.Error("Please input search terms to use with data that will be loaded");
                return;
            }
            if (_notificationViewModel.CurrentNotificationType != NotificationType.Info)
            {
                _notificationViewModel.Info("Welcome, "+_rankMgr.CurrentUser);
            }

            _cancelSearchSource = new CancellationTokenSource();

            var list = new List<string>();
            var words = _searchTerm.Split(new char[] {' '});
            foreach (var word in words)
            {
                list.AddRange(_synonymManager.GetMostLikelySynonymGroup(word));
            }
            AllSearchTerms = list;

            OnSearching();
            try
            {
                var s = Stopwatch.StartNew();
                var ads = AdvertisementReaderCsv.OpenFileDialogAndLoad(_rankMgr.CurrentUser, _cancelSearchSource.Token);
                if (ads.Count > 0)
                {
                    Advertisements = _rankMgr.RankAdvertisements(ads, AllSearchTerms);

                    if (SaveSearches && Advertisements.Count > 0)
                    {
                        AdvertisementWriterCsv.Write(Advertisements, _rankMgr.CurrentUser, null, "LocalMerged");
                    }
                }
                else
                {
                    Advertisements = ads;
                }
                s.Stop();
                StatsInfo = "Loaded: " + Advertisements.Count + " advertisements in " + s.Elapsed;
            }
            catch (Exception e)
            {
                _notificationViewModel.Error(e.Message);
            }
            finally
            {
                if (StatsInfo.StartsWith("Searching")) StatsInfo = "Done";
                OnSearchEnded();
            }
        }

        private void Disconnected()
        {
            IsConnected = false;
            CancelSearching();
        }

        private void Reconnect()
        {
            try
            {
                //try disconnect (but if there is no internet it will at least reset connection)
                try { _searchSource.Disconnect(); } catch (WebException) { }
                //connect anew
                _searchSource.Connect();
                IsConnected = true;
                if(_rankMgr.CurrentUser != null) _notificationViewModel.Info("Welcome "+_rankMgr.CurrentUser+", your connection is now active.");
            }
            catch (WebException)
            {
                Disconnected();
            }
        }

        /// <summary>
        /// Cancels the search if running, does nothing if not searching
        /// </summary>
        private void CancelSearching()
        {
            if (CanSearch 
                || _cancelSearchSource == null 
                || _cancelSearchSource.IsCancellationRequested) return;

            StatsInfo = "Cancelling Search ...";
            WPFRenderHelper.ForceUiUpdate();
            _cancelSearchSource.Cancel();
            _reader.ClearResults();
        }

        /// <summary>
        /// Triggers search and fires Searching event if not already searching or search disabled
        /// </summary>
        private void SearchData()
        {
            if (!CanSearch) return;
            if (_notificationViewModel.CurrentNotificationType != NotificationType.Info)
            {
                _notificationViewModel.Info("Welcome, " + _rankMgr.CurrentUser);
            }

            if (string.IsNullOrWhiteSpace(_searchTerm))
            {
                _notificationViewModel.Warning("Enter Keywords To Search");
                return;
            }

            if (SelectedWebsites.Count == 0)
            {
                _notificationViewModel.Warning("Please select at least one job website from drop-down");
                return;
            }

            OnSearching();

            _cancelSearchSource = new CancellationTokenSource();
            
            Task.Factory.StartNew(PerformSearch, _cancelSearchSource.Token);
        }

        /// <summary>
        /// Executes the actual search and fires SearchEnded event upon completion
        /// </summary>
        private void PerformSearch()
        {
            //general practice to check if already cancelled
            if (_cancelSearchSource.Token.IsCancellationRequested)
            {
                StatsInfo = "Search cancelled, keeping previous results if any";
                return;
            }
            try
            {
                _reader = new WebsiteDataReader(_searchSource);

                //backup original set of options because we will alter the collection
                var selectedSitesBackup = SelectedWebsites.ToList();

                //select all debug keys into hashset
                var debugEntries = new HashSet<string>(selectedSitesBackup.Where(_ => _.Contains("debug")));

                //use hashset to remove all those keys from the selected websites
                //we use hashset because it would allow for quick (almost O(1)) Contains lookup
#if DEBUG
                var numOfAdsRemoved = 
#endif
                selectedSitesBackup.RemoveAll(debugEntries.Contains);

#if DEBUG
                var debugAds = GetDebugAdvertisements(numOfAdsRemoved, debugEntries);
#endif

                var selectedGuids = _connectorWebsites.GetValuesByKeys(selectedSitesBackup).ToList();
                
                var s = Stopwatch.StartNew();

                StatsInfo = "Searching: Loading Advertisements ...";
                try
                {
                    _reader.LoadResults(_synonymManager.GetMostLikelySynonymGroup(_searchTerm.Trim()), selectedGuids, _cancelSearchSource.Token).Wait(_cancelSearchSource.Token);
                }
                catch (OperationCanceledException) { }

                var ads = _reader.Advertisements.Distinct().ToList();   //filter any duplicates

                var readyAds = new List<Advertisement>();
                var nullAds = new List<Advertisement>();
                foreach (var ad in ads)
                {
                    if (ad.DescriptionSummary == null) nullAds.Add(ad); //ads where we need to fill description
                    else readyAds.Add(ad);
                }

                var bg = GetDescriptionProgressWorker(nullAds.Count);
                bg.RunWorkerAsync();

                try
                {
                    _reader.PopulateDescriptions(_detailConnectorWebsites, nullAds, _cancelSearchSource.Token).Wait(_cancelSearchSource.Token);
                }
                catch (OperationCanceledException) { }


                ads = readyAds;
                ads.AddRange(_reader.Advertisements);
#if DEBUG
                ads.AddRange(debugAds);
#endif
                s.Stop();
                bg.CancelAsync();   //cancel in either case by the end/cancel of main search
                if (_cancelSearchSource.Token.IsCancellationRequested)
                {
                    StatsInfo = "Search cancelled, keeping previous results if any";
                }
                else
                {
                    if (!IsConnected)
                    {
                        NotConnectedMessage();
                        Advertisements = ads;
                    }
                    else
                    {
                        //only overwrite advertisements and search terms if search was not cancelled
                        AllSearchTerms = _reader.QuerySearchTerms;
                        Advertisements = _rankMgr.RankAdvertisements(ads, AllSearchTerms);
                        if (SaveSearches && Advertisements.Count > 0)
                        {
                            AdvertisementWriterCsv.Write(ads, _rankMgr.CurrentUser, null, "Web");
                            AdvertisementWriterCsv.Write(Advertisements, _rankMgr.CurrentUser, null, "Ranked");
                        }
                    }
                    StatsInfo = "Loaded: " + Advertisements.Count + " advertisements in " + s.Elapsed;
                }
            }
            catch (WebException)
            {
                IsConnected = false;
            }
            finally
            {
                if (StatsInfo.StartsWith("Searching")) StatsInfo = "Error occured";
                OnSearchEnded();
            }
        }

        private void NotConnectedMessage()
        {
            StatsInfo = "No results, Websites could not be reached";
            _notificationViewModel.Warning("Please check your internet connection, working offline");
        }

        private BackgroundWorker GetDescriptionProgressWorker(int totalCount)
        {
            var bg = new BackgroundWorker { WorkerReportsProgress = true, WorkerSupportsCancellation = true };

            bg.DoWork += (sender, args) =>
            {
                var latestCountOfDescriptionsLoaded = 0;
                var previous = 0;
                while (latestCountOfDescriptionsLoaded < totalCount)
                {
                    if (bg.CancellationPending) return;

                    latestCountOfDescriptionsLoaded = _reader.CurrentAdDescriptionsLoadedCount;
                    if (previous != latestCountOfDescriptionsLoaded)
                    {
                        bg.ReportProgress(latestCountOfDescriptionsLoaded);
                        previous = latestCountOfDescriptionsLoaded;
                    }

                    Thread.Sleep(5000);
                }
            };

            bg.ProgressChanged += (sender, args) => StatsInfo = string.Format(
                "Searching: Loading Advertisement Descriptions for {0}/{1} advertisments ...",
                args.ProgressPercentage, totalCount);
            return bg;
        }

#if DEBUG
        private IEnumerable<Advertisement> GetDebugAdvertisements(int numOfAdsRemoved, IEnumerable<string> debugEntries)
        {
            var debugAds = new List<Advertisement>();
            if (numOfAdsRemoved > 0)
            {
                var selectedFiles = _connectorWebsites.GetValuesByKeys(debugEntries);
                int exceptionCount = 0;
                int totalCount = 0;
                foreach (var file in selectedFiles)
                {
                    try
                    {
                        debugAds.AddRange(AdvertisementReaderCsv.Read(file));
                    }
                    catch (ArgumentException)
                    {
                        exceptionCount++;
                    }
                    totalCount++;
                }
                if (exceptionCount > 0)
                    _notificationViewModel.Warning(string.Format("Could not read {0} files out of {1}", 
                        exceptionCount, totalCount));
            }
            return debugAds;
        }
#endif

        public void Reset()
        {
            if(_cancelSearchSource != null ) _cancelSearchSource.Cancel();
            StatsInfo = String.Empty;
            SearchTerm = String.Empty;
            Advertisements = new List<Advertisement>();
            WebsitesAvailable = _connectorWebsites.Keys.ToList();
            SelectedWebsites = new List<string> { "All Selected" };
        }

        public void SearchBoxFocus()
        {
            OnResetFocus();
        }

        /*   Events   */
        public event EventHandler Searching;

        protected virtual void OnSearching()
        {
            CanSearch = false;
            StatsInfo = "Searching ...";
            EventHandler handler = Searching;
            if (handler != null) handler(this, null);
        }

        public event EventHandler SearchEnded;

        protected virtual void OnSearchEnded()
        {
            CanSearch = true;
            EventHandler handler = SearchEnded;
            if (handler != null) handler(this, null);
        }

        public event EventHandler ResetFocus;

        protected virtual void OnResetFocus()
        {
            EventHandler handler = ResetFocus;
            if (handler != null) handler(this, null);
        }
    }
}
