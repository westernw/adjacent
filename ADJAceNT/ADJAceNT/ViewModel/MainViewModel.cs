﻿using System;
using System.IO;
using System.Windows;
using ADJAceNT.Helpers;
using ADJAceNT.Managers;
using ADJAceNT.Model;
using log4net;

namespace ADJAceNT.ViewModel
{
    public class MainViewModel : BasePropertyChanged
    {
        private IRankManager _rankManager;
        private UserHistoryModel _userHistoryModel;

        private Visibility _authorisedScreeVisibility;
        private Visibility _resultsLoading;

        public Visibility AuthorisedScreenVisibility
        {
            get { return _authorisedScreeVisibility; } 
            private set { _authorisedScreeVisibility = value; OnPropertyChanged("AuthorisedScreenVisibility"); }
        }

        public Visibility ResultsLoading
        {
            get { return _resultsLoading; } 
            private set { _resultsLoading = value; OnPropertyChanged("ResultsLoading"); }
        }
        
        public String Title { get { return "Advanced Job Assistant, v." + typeof(MainViewModel).Assembly.GetName().Version; } }
        public LoginViewModel LoginViewModel { get; private set; }
        public INotificationBarViewModel NotificationBarViewModel { get; private set; }
        public SearchViewModel SearchViewModel { get; private set; }
        public AdMiscActionViewModel AdMiscActionViewModel { get; private set; }
        public DelegateCommand LogOut { get; private set; }
        public DelegateCommand OpenAccountManagement { get; private set; }
        public UserHistoryViewModel UserHistoryViewModel { get; private set; }
        
        public MainViewModel()
        {
            NotificationBarViewModel = new NotificationBarViewModel();

                new System.Threading.Thread(() =>
            {
                _rankManager = new RankManager();
                var dataSource = new DatabaseManager();
                _userHistoryModel = new UserHistoryModel(dataSource);
                LoginViewModel = new LoginViewModel(NotificationBarViewModel, dataSource);
                SearchViewModel = new SearchViewModel(NotificationBarViewModel, _rankManager);
                AdMiscActionViewModel = new AdMiscActionViewModel(NotificationBarViewModel, dataSource);
                UserHistoryViewModel = new UserHistoryViewModel(NotificationBarViewModel, _userHistoryModel);
                ResultsLoading = Visibility.Collapsed;
                AuthorisedScreenVisibility = Visibility.Collapsed;
                SearchViewModel.SaveSearches = true;
                SearchViewModel.Searching += SearchViewModelOnSearching;
                SearchViewModel.SearchEnded += SearchViewModelOnSearchEnded;
                UserHistoryViewModel.ViewClosed += (s, e) => { AuthorisedScreenVisibility = Visibility.Visible; NotificationBarViewModel.Info("Welcome, " + LoginViewModel.Username); };
                UserHistoryViewModel.ViewOpened += (s, e) => AuthorisedScreenVisibility = Visibility.Collapsed;
                UserHistoryViewModel.UserDeleted += (s, e) => LogOut.Execute(null);
                OpenAccountManagement = new DelegateCommand(() =>
                {
                    UserHistoryViewModel.IsViewOpen = true;
                    AuthorisedScreenVisibility = Visibility.Collapsed;
                });

                LogOut = new DelegateCommand(() =>
                {
                    AuthorisedScreenVisibility = Visibility.Collapsed;

                    NotificationBarViewModel.Info("Logging out, please wait ...");

                    GlobalContext.Properties["LogPath"] = Path.GetDirectoryName(GlobalContext.Properties["LogPath"].ToString());

                    if (ResultsLoading == Visibility.Visible)
                        SearchViewModel.SearchEnded += ExitToLoginScreenOnSearchEnd;
                    else 
                        LoginViewModel.Restart();

                    SearchViewModel.Reset();
                    UserHistoryViewModel.Reset();
                });

                LoginViewModel.UserLoggedIn += (sender, args) => 
                {
                    //reconfigure to user logged in
                    GlobalContext.Properties["LogPath"] = Path.Combine(GlobalContext.Properties["LogPath"].ToString(), LoginViewModel.Username);
                    log4net.Config.XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile));

                    _rankManager.CurrentUser = LoginViewModel.Username;
                    AdMiscActionViewModel.CurrentUser = LoginViewModel.Username;
                    _userHistoryModel.CurrentUser = LoginViewModel.Username;
                    AuthorisedScreenVisibility = Visibility.Visible;
                    SearchViewModel.SearchBoxFocus();
                };
                OnObjectLoaded(null);
            }).Start()
            ;
        }
        
        private void ExitToLoginScreenOnSearchEnd(object sender, EventArgs eventArgs)
        {
            LoginViewModel.Restart();
            SearchViewModel.SearchEnded -= ExitToLoginScreenOnSearchEnd;
        }

        private void SearchViewModelOnSearchEnded(object sender, EventArgs eventArgs)
        {
            ResultsLoading = Visibility.Collapsed;
        }

        private void SearchViewModelOnSearching(object sender, EventArgs eventArgs)
        {
            ResultsLoading = Visibility.Visible;
        }

        /*   Events   */
        public event EventHandler ObjectLoaded;

        protected virtual void OnObjectLoaded(EventArgs e)
        {
            EventHandler handler = ObjectLoaded;
            if (handler != null) handler(this, e);
        }
    }
}
