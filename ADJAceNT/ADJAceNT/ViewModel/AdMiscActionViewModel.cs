﻿using System.Collections.Generic;
using System.Linq;
using ADJAceNT.Entities;
using ADJAceNT.Enums;
using ADJAceNT.Exceptions;
using ADJAceNT.Helpers;
using ADJAceNT.Managers;

namespace ADJAceNT.ViewModel
{
    public class AdMiscActionViewModel:BasePropertyChanged
    {
        private readonly IDataManager _userUserDataManager;
        private readonly INotificationBarViewModel _notificationViewModel;
        private readonly HashSet<string> _exclusionWords; 
        private const string NoDescription = "No description found";
        private const string FailedDescription = "Retrieval Failed";
        private const string NotSpecified = "Not Specified";

        public DelegateCommand OpenLink { get; private set; }
        public DelegateCommand SetAdStatusGood { get; private set; }
        public DelegateCommand SetAdStatusBad { get; private set; }
        public DelegateCommand SetAdStatusNone { get; private set; }
        public string CurrentUser { get; set; }

        public AdMiscActionViewModel(INotificationBarViewModel notificationViewModel, IDataManager userDataManager = null, IExclusionWordReader exclusionReader = null)
        {
            _notificationViewModel = notificationViewModel;
            _userUserDataManager = userDataManager ?? new DatabaseManager();
            _exclusionWords = new HashSet<string>((exclusionReader ?? new ExclusionWordReader()).GetExclusionWords());

            OpenLink = new DelegateCommand(url => System.Diagnostics.Process.Start(url.ToString()));
            SetAdStatusGood = new DelegateCommand(ad => SetAdvertStatusAndNotify((Advertisement)ad, AdvertisementStatus.Interesting, true));
            SetAdStatusBad = new DelegateCommand(ad => SetAdvertStatusAndNotify((Advertisement)ad, AdvertisementStatus.Uninteresting, false));
            SetAdStatusNone = new DelegateCommand(ad => SetAdvertStatusAndNotify((Advertisement)ad, AdvertisementStatus.None, ((Advertisement)ad).Status == AdvertisementStatus.Uninteresting));
        }

        private void SetAdvertStatusAndNotify(Advertisement advertisement, AdvertisementStatus status, bool isPositive)
        {
            var wordSet = new List<string>();

            //if description loaded correctly
            if (!NoDescription.Equals(advertisement.DescriptionSummary) &&
                !FailedDescription.Equals(advertisement.DescriptionSummary))
            {
                var summary = advertisement.DescriptionSummary.ToUpperInvariant();  //make all upper for easier comparison
                wordSet.AddRange(
                    summary
                    .SplitByPunctuation()                                           //split all words
                    .Distinct()                                                     //remove duplicates
                    .Where(_ => 
                        _.Length > 2 && 
                        !string.IsNullOrWhiteSpace(_) && 
                        !_exclusionWords.Contains(_))                               //words need to be longer than 2 chars and not whitespace
                    .ToList());                                                     //persist in a list
            }
            if (!NotSpecified.Equals(advertisement.Location)) wordSet.AddRange(advertisement.Location.SplitByPunctuation());
            if (!NotSpecified.Equals(advertisement.Salary)) wordSet.AddRange(advertisement.Salary.SplitByPunctuation());

            try
            {
                _userUserDataManager.BulkAddOrUpdateWordWeightMapping(CurrentUser, wordSet, isPositive);
            }
            catch (DatabaseManagerException e)
            {
                _notificationViewModel.Warning(e.Message);
            }

            advertisement.Status = status;
        }
    }
}
