﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using ADJAceNT.Exceptions;
using ADJAceNT.Helpers;
using ADJAceNT.Managers;
using log4net;

namespace ADJAceNT.ViewModel
{
    public class LoginViewModel : BasePropertyChanged
    {
        private readonly ILog _logger;
        private readonly DelegateCommand _startWithUsernameChosen;
        private readonly INotificationBarViewModel _notificationBarViewModel;
        private readonly IDataManager _userData;

        private string _username;
        public string Username { 
            get { return _username; } 
            set
            {
                _username = value; 
                OnPropertyChanged("Username"); 
            } 
        }

        private Visibility _screenVisible;
        public Visibility ScreenVisible
        {
            get { return _screenVisible; }
            set
            {
                _screenVisible = value;
                OnPropertyChanged("ScreenVisible");
            }
        }

        public List<string> AllUsers { get; private set; }

        public DelegateCommand StartWithUsernameChosen 
        { 
            get { return _startWithUsernameChosen; }
        }

        /// <summary>
        /// Creates a new model for login view
        /// </summary>
        /// <param name="notificationBarViewModel">Must pass in implementation which will receive relevant user messages through exposed method invocations</param>
        /// <param name="logger">Will log error messages, default log4net from config used if not provided</param>
        /// <param name="userDataInputManager">Will load user data and create new user, <see cref="DatabaseManager"/> used if not provided</param>
        public LoginViewModel(INotificationBarViewModel notificationBarViewModel, IDataManager userDataInputManager = null, ILog logger = null)
        {
            _logger = logger ?? LogManager.GetLogger("GeneralLogFileAppender");
            _notificationBarViewModel = notificationBarViewModel;
            _screenVisible = Visibility.Visible;
            _startWithUsernameChosen = new DelegateCommand(ValidateAndUseUsername);
            _notificationBarViewModel.Info("Welcome, please enter new or existing username, case is ignored (i.e. \"AlBeRt\" is same as \"albert\")");
            _userData = userDataInputManager ?? new DatabaseManager();
            RefreshAllUsers();
        }

        /// <summary>
        /// Restarts the login view as if application just opened
        /// </summary>
        public void Restart()
        {
            _notificationBarViewModel.Info("Welcome, please enter new or existing username, case is ignored (i.e. \"AlBeRt\" is same as \"albert\")");
            Username = null;
            RefreshAllUsers();
            ScreenVisible = Visibility.Visible;
        }

        private void RefreshAllUsers()
        {
            try
            {
                AllUsers = _userData.GetAllUsers();
            }
            catch (DatabaseManagerException e)
            {
                _logger.Info(e.Message, e);
                AllUsers = new List<string>();
            }
        }

        /// <summary>
        /// Will select existing username if length is same and string is same ignoring case, otherwise will record as new user
        /// </summary>
        private void ValidateAndUseUsername()
        {
            //only alphanumeric username
            if (Username != null && Username.All(char.IsLetterOrDigit))
            {
                Username = AllUsers.SingleOrDefault(_ => String.Equals(_, Username, StringComparison.OrdinalIgnoreCase)) ?? Username;

                _userData.AddUser(Username);
                _notificationBarViewModel.Info("Welcome, " + Username);

                ScreenVisible = Visibility.Collapsed;
                RefreshAllUsers();
                OnUserLoggedIn(EventArgs.Empty);
            }
            else
            {
                _notificationBarViewModel.Warning("Please use only letters and/or numbers e.g. Michelle12");
                Username = null;
            }
        }

        /*   Events   */
        public event EventHandler UserLoggedIn;

        protected virtual void OnUserLoggedIn(EventArgs e)
        {
            EventHandler handler = UserLoggedIn;
            if (handler != null) handler(this, e);
        }
    }
}
