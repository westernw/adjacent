using System.ComponentModel;
using System.Windows.Media;

namespace ADJAceNT.ViewModel
{
    public interface INotificationBarViewModel
    {
        string Message { get; }
        ImageSource Icon { get; }
        NotificationType CurrentNotificationType { get; }
        void Error(string message);
        void Warning(string message);
        void Info(string message);
        void Empty();
        event PropertyChangedEventHandler PropertyChanged;
    }
}