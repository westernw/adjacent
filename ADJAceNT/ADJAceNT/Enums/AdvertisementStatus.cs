﻿namespace ADJAceNT.Enums
{
    public enum AdvertisementStatus
    {
        Interesting,
        Uninteresting,
        None
    }
}
