namespace ADJAceNT.Enums
{
    public enum TimeUnit
    {
        seconds, minutes, hours, days, months, years
    }
}