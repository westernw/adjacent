﻿using System.Collections.Concurrent;
using System.IO;
using ADJAceNT.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ADJAceNT.Benchmark.Tests.AnonymousTests
{
    class JsonConversionExperiment
    {
        private readonly BlockingCollection<Advertisement> _adsBlockingCollection;
        private readonly ConcurrentBag<Advertisement> _adsBag;
        private readonly ConcurrentQueue<Advertisement> _adsQueue;
        private readonly dynamic _json;

        public JsonConversionExperiment()
        {
            _adsBlockingCollection = new BlockingCollection<Advertisement>();
            _adsBag = new ConcurrentBag<Advertisement>();
            _adsQueue = new ConcurrentQueue<Advertisement>();
            _json = JsonConvert.DeserializeObject(File.ReadAllText(@"Files/SampleResults.txt"));
        }

        public void ConvertJToken()
        {
            foreach(JToken ad in _json.results) _adsBlockingCollection.Add(ad.ToObject<Advertisement>());
        }

        public void ConvertJObject()
        {
            foreach (JObject ad in _json.results) _adsBlockingCollection.Add(ad.ToObject<Advertisement>());
        }

        public void AddToBlocking()
        {
            foreach (JObject ad in _json.results) _adsBlockingCollection.Add(ad.ToObject<Advertisement>());
        }

        public void AddToBag()
        {
            foreach (JObject ad in _json.results) _adsBag.Add(ad.ToObject<Advertisement>());
        }

        public void AddToQueue()
        {
            foreach (JObject ad in _json.results) _adsQueue.Enqueue(ad.ToObject<Advertisement>());
        }
    }
}
