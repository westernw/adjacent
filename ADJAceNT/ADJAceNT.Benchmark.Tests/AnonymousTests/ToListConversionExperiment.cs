﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using ADJAceNT.Entities;

namespace ADJAceNT.Benchmark.Tests.AnonymousTests
{
    class ToListConversionExperiment
    {
        private readonly ConcurrentQueue<Advertisement> _queue;
 
        public ToListConversionExperiment()
        {
            _queue = new ConcurrentQueue<Advertisement>();
            for (int i = 0; i < 1000; ++i) _queue.Enqueue(new Advertisement());
        }

        /// <summary>
        /// Performs LINQ.ToList()
        /// </summary>
        public List<Advertisement> AdvertisementsToListVariant
        {
            get { return _queue.ToList(); }
        }

        /// <summary>
        /// Performs new List(ConcurrentQueue.ToArray())
        /// </summary>
        public List<Advertisement> AdvertisementsToArrayVariant
        {
            get { return new List<Advertisement>(_queue.ToArray()); }
        }

        /// <summary>
        /// Performs ConcurrentQueue.ToArray().LINQ.ToList()
        /// </summary>
        public List<Advertisement> AdvertisementsToArrayToListVariant
        {
            get { return _queue.ToArray().ToList(); }
        }
    }
}
