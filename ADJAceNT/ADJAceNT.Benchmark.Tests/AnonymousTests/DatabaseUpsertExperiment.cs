﻿using System;
using System.Data;
using System.Data.SQLite;
using ADJAceNT.Exceptions;

namespace ADJAceNT.Benchmark.Tests.AnonymousTests
{
    class DatabaseUpsertExperiment
    {
        private const string ArgumentEmptyString = "{0}";

        public SQLiteConnection Connection { get; set; }

        public DatabaseUpsertExperiment()
        {
            Connection = new SQLiteConnection("Data Source=./Files/user.db;Version=3;");
            try
            {
                Connection.Open();
            }
            catch (ArgumentException e)
            {
                throw new DatabaseManagerException("App.config is likely missing databaseName in connection strings section", e);
            }
        }

        public void AddOrPrivateWordWeightMapping_UpdateOrInsert(string username, string word, bool isPositive)
        {
            if (String.IsNullOrWhiteSpace(username)) throw new ArgumentException(string.Format(ArgumentEmptyString, "username"), "username");
            if (String.IsNullOrWhiteSpace(word)) throw new ArgumentException(string.Format(ArgumentEmptyString, "word"), "word");
            if (Connection.State == ConnectionState.Closed) throw new DatabaseManagerException("The connection must be open with Connect() method or in constructor");

            var c = Connection;
            using (var cmd = new SQLiteCommand(c))
            {
                try
                {
                    cmd.CommandText = string.Format("UPDATE Words_{0} SET Weight = Weight {1} 1 WHERE Word = @word",
                                      username, (isPositive ? "+" : "-"));
                    cmd.Parameters.Add(new SQLiteParameter("@word", word.ToUpperInvariant()));
                    cmd.ExecuteNonQuery();
                    if (Connection.Changes == 0)
                    {
                        cmd.CommandText = string.Format("INSERT into Words_{0} (Word, Weight) values (@word, {1})",
                                      username, (isPositive ? "1" : "-1"));
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (SQLiteException e)
                {
                    throw new DatabaseManagerException("Failed to record user data, more detail in log", e);
                }
            }
        }

        public void AddOrPrivateWordWeightMapping_Custom(string insertType, string username, string word, bool isPositive)
        {
            if (String.IsNullOrWhiteSpace(username)) throw new ArgumentException(string.Format(ArgumentEmptyString, "username"), "username");
            if (String.IsNullOrWhiteSpace(word)) throw new ArgumentException(string.Format(ArgumentEmptyString, "word"), "word");
            if (Connection.State == ConnectionState.Closed) throw new DatabaseManagerException("The connection must be open with Connect() method or in constructor");

            using (var cmd = new SQLiteCommand(Connection))
            {
                //username always comes from program itself sanitised on first login, 
                //the database is local user copy, hence user always has access to all data without sql injection
                cmd.CommandText = string.Format("INSERT or {0} into Words_{1} (Word, Weight) " +
                                  "values (@word, coalesce ((SELECT Weight {2} 1 from Words_{1} where word = @word ), {3}))",
                                  insertType, username, (isPositive ? "+" : "-"), (isPositive ? "1" : "-1"));
                cmd.Parameters.Add(new SQLiteParameter("@word", word.ToUpperInvariant()));

                try
                {
                    cmd.ExecuteNonQuery(); // Execute the query
                }
                catch (SQLiteException e)
                {
                    throw new DatabaseManagerException("Failed to record user data, more detail in log", e);
                }
            }
        }
    }
}
