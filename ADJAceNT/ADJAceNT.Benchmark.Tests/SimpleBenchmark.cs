﻿using System;
using System.Diagnostics;
using ADJAceNT.Benchmark.Tests.AnonymousTests;

namespace ADJAceNT.Benchmark.Tests
{
    class SimpleBenchmark
    {
        public static void Main()
        {
            BoxMessage("AddOrPrivateWordWeightMapping_UpdateOrInsert");
            Total_AddOrPrivateWordWeightMapping_UpdateOrInsert();
            BoxMessage("AddOrPrivateWordWeightMapping_Custom");
            Total_AddOrPrivateWordWeightMapping_Custom();

            BoxMessage("[new List+ClassMethod]new List<Advertisement>(ConcurrentQueue.ToArray())");
            Total_ToArray();
            BoxMessage("[LINQ.Extension]ConcurrentQueue.ToList()");
            Total_ToList();
            BoxMessage("[Both]ConcurrentQueue.ToArray().ToList()");
            Total_ToArrayToList();

            BoxMessage("Total_ConvertJToken");
            Total_ConvertJToken();
            BoxMessage("Total_ConvertJObject");
            Total_ConvertJObject();

            BoxMessage("Total_AddToQueue");
            Total_AddToQueue();
            BoxMessage("Total_AddToBlocking");
            Total_AddToBlocking();
            BoxMessage("Total_AddToBag");
            Total_AddToBag();
        }

        private static void BoxMessage(string message)
        {
            for (int i = 0; i < 80; i++) Console.Write("-");
            Console.Write("|");
            for (int i = 0; i < (79 - message.Length) / 2; i++) Console.Write(" ");
            Console.Write(message);
            for (int i = 0; i < (78 - message.Length) / 2; i++) Console.Write(" ");
            Console.Write("|");
            for (int i = 0; i < 80; i++) Console.Write("-");
        }

        private static void Total_AddOrPrivateWordWeightMapping_Custom()
        {
            var testLoader = new DatabaseUpsertExperiment();

            Benchmark.Iterations = 100;

            GC.Collect();
            Benchmark.CheckTimeReturnTotal("Custom", () => testLoader.AddOrPrivateWordWeightMapping_Custom("REPLACE", "Aleks", "word", true));
            Console.WriteLine("\n");
        }

        private static void Total_AddOrPrivateWordWeightMapping_UpdateOrInsert()
        {
            var testLoader = new DatabaseUpsertExperiment();

            Benchmark.Iterations = 100;

            GC.Collect();
            Benchmark.CheckTimeReturnTotal("UpdateOrInsert", () => testLoader.AddOrPrivateWordWeightMapping_UpdateOrInsert("Aleks", "word",true));
            Console.WriteLine("\n");
        }


        private static void Total_ConvertJToken()
        {
            var testLoader = new JsonConversionExperiment();

            Benchmark.Iterations = 20000;

            GC.Collect();
            Benchmark.CheckTimeReturnTotal("ConvertJToken", testLoader.ConvertJToken);
            Console.WriteLine("\n");
        }

        private static void Total_ConvertJObject()
        {
            var testLoader = new JsonConversionExperiment();

            Benchmark.Iterations = 20000;

            GC.Collect();
            Benchmark.CheckTimeReturnTotal("ConvertJObject", testLoader.ConvertJObject);
            Console.WriteLine("\n");
        }
        
        private static void Total_AddToQueue()
        {
            var testLoader = new JsonConversionExperiment();

            Benchmark.Iterations = 20000;

            GC.Collect();
            Benchmark.CheckTimeReturnTotal("AddToQueue", testLoader.AddToQueue);
            Console.WriteLine("\n");
        }

        private static void Total_AddToBag()
        {
            var testLoader = new JsonConversionExperiment();

            Benchmark.Iterations = 20000;

            GC.Collect();
            Benchmark.CheckTimeReturnTotal("AddToBag", testLoader.AddToBag);
            Console.WriteLine("\n");
        }

        private static void Total_AddToBlocking()
        {
            var testLoader = new JsonConversionExperiment();

            Benchmark.Iterations = 20000;

            GC.Collect();
            Benchmark.CheckTimeReturnTotal("AddToBlocking", testLoader.AddToBlocking);
            Console.WriteLine("\n");
        }

        private static void Total_ToArray()
        {
            var testLoader = new ToListConversionExperiment();

            Benchmark.Iterations = 20000;

            GC.Collect();
            Benchmark.ReturnAverageOf10Totals("AdvertisementsToArrayVariant", () =>
            {
                var t = testLoader.AdvertisementsToArrayVariant;
            });
            Console.WriteLine("\n");
        }

        private static void Total_ToList()
        {
            var testLoader = new ToListConversionExperiment();

            Benchmark.Iterations = 20000;

            GC.Collect();
            Benchmark.ReturnAverageOf10Totals("AdvertisementsToListVariant", () =>
            {
                var t = testLoader.AdvertisementsToListVariant;
            });
            Console.WriteLine("\n");
        }

        private static void Total_ToArrayToList()
        {
            var testLoader = new ToListConversionExperiment();

            Benchmark.Iterations = 20000;

            GC.Collect();
            Benchmark.ReturnAverageOf10Totals("AdvertisementsToArrayToListVariant", () =>
            {
                var t = testLoader.AdvertisementsToArrayToListVariant;
            });
            Console.WriteLine("\n");
        }

    }

    public static class Benchmark
    {
        public static int Iterations { get; set; }

        public static float CheckTimeReturnTotal(string methodName, Action method)
        {
            Stopwatch s = Stopwatch.StartNew();
            for (int i = 0; i < Iterations; i++)
            {
                method();
            }

            s.Stop();
            Console.WriteLine("Method {0}*{1} took total {2:F2} s", methodName, Iterations, s.ElapsedMilliseconds / 1000.0f);
            return s.ElapsedMilliseconds / 1000.0f;
        }

        internal static float CheckTimeReturnAverage(string methodName, Action method)
        {
            float average = 0;
            Stopwatch s = Stopwatch.StartNew();
            for (int i = 0; i < Iterations; i++)
            {
                method();
                average += s.ElapsedMilliseconds;
                s.Restart();
            }
            s.Stop();

            Console.WriteLine("Method {0}*{1} took on average {2:F2} s", methodName, Iterations, (average / Iterations) / 1000.0f);
            return (average / Iterations) / 1000.0f;
        }

        internal static float ReturnAverageOf10Totals(string methodName, Action method)
        {
            float total = 0;
            for (int i = 0; i < 10; ++i) total += CheckTimeReturnTotal(methodName, method);
            Console.WriteLine("Method {0}*{1} Totals took on average {2:F2} s", methodName, Iterations, (total / 10));
            return (total / 10);
        }
    }
}
