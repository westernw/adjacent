﻿{
  "offset": 0,
  "results": [
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1420133",
      "jobtitle": ".net developer",
      "joblink/_text": ".net developer",
      "company": "CPL ICT",
      "location": "Cavan",
      "joblink/_source": "ApplyForJob.aspx?Id=1420133"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1419530",
      "jobtitle": ".net Developer",
      "joblink/_text": ".net Developer",
      "company": "Adecco",
      "location": "Kerry",
      "joblink/_source": "ApplyForJob.aspx?Id=1419530"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1420596",
      "jobtitle": ".NET Lead Developer",
      "joblink/_text": ".NET Lead Developer",
      "company": "CPL ICT",
      "location": "Dublin City Centre",
      "joblink/_source": "ApplyForJob.aspx?Id=1420596"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1420819",
      "jobtitle": ".Net Developer - Dublin City Centre",
      "joblink/_text": ".Net Developer - Dublin City Centre",
      "company": "Lex Consultancy",
      "location": "Dublin",
      "joblink/_source": "ApplyForJob.aspx?Id=1420819"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1420456",
      "jobtitle": "C# .Net / HTML5 / Mobile App Developer - Co Cork",
      "joblink/_text": "C# .Net / HTML5 / Mobile App Developer - Co Cork",
      "company": "SouthWestern",
      "location": "Co Cork",
      "joblink/_source": "ApplyForJob.aspx?Id=1420456"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1419698",
      "jobtitle": "New Lead .net Developer, Dublin North",
      "joblink/_text": "New Lead .net Developer, Dublin North",
      "company": "Adecco",
      "location": "Dublin North",
      "joblink/_source": "ApplyForJob.aspx?Id=1419698"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1420831",
      "jobtitle": "Contract Developers Required for Exciting New Live Roles - Dublin City Centre",
      "joblink/_text": "Contract Developers Required for Exciting New Live Roles - Dublin City Centre",
      "company": "Lex Consultancy",
      "location": "Dublin",
      "joblink/_source": "ApplyForJob.aspx?Id=1420831"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1418940",
      "jobtitle": "Cpl/SAP German or French Technical Support in Galway 12 months contract initially - Contact Valentina 00353 (0) 1 614 6196",
      "joblink/_text": "Cpl/SAP German or French Technical Support in Galway 12 months contract initially - Contact Valentina 00353 (0) 1 614 6196",
      "company": "CPL Language Jobs",
      "location": "Galway",
      "joblink/_source": "ApplyForJob.aspx?Id=1418940"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1418518",
      "jobtitle": "Lead Web Developer",
      "joblink/_text": "Lead Web Developer",
      "company": "Car Trawler",
      "location": "Dundrum, Dublin 14",
      "joblink/_source": "ApplyForJob.aspx?Id=1418518"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1420156",
      "jobtitle": "Key Account Manager- Nationwide",
      "joblink/_text": "Key Account Manager- Nationwide",
      "company": "CPL Galway",
      "location": "Nationwide",
      "joblink/_source": "ApplyForJob.aspx?Id=1420156"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1416421",
      "jobtitle": "Software UI Developer - 10129",
      "joblink/_text": "Software UI Developer - 10129",
      "company": "Clever Techies",
      "location": "Dublin",
      "joblink/_source": "ApplyForJob.aspx?Id=1416421"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1383071",
      "jobtitle": "Software Technical Support / Technischer Support / Support technique for German / Deutsch or French / Français markets in Galway, Ireland. Relocation offered.",
      "joblink/_text": "Software Technical Support / Technischer Support / Support technique for German / Deutsch or French / Français markets in Galway, Ireland. Relocation offered.",
      "company": "Cpl/SAP",
      "location": "Galway",
      "joblink/_source": "ApplyForJob.aspx?Id=1383071"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1383096",
      "jobtitle": "Technical Support Cloud Computing (German / Deutsch or French / Français) in Galway - 25.000 Euro/Annum + relocation",
      "joblink/_text": "Technical Support Cloud Computing (German / Deutsch or French / Français) in Galway - 25.000 Euro/Annum + relocation",
      "company": "Cpl/SAP",
      "location": "Galway",
      "joblink/_source": "ApplyForJob.aspx?Id=1383096"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1392393",
      "jobtitle": "German Deutsch Support Agents in Cpl/ SAP Galway Ireland - Relocation Offered - Contact Valentina +353 1 614 6196",
      "joblink/_text": "German Deutsch Support Agents in Cpl/ SAP Galway Ireland - Relocation Offered - Contact Valentina +353 1 614 6196",
      "company": "Cpl/SAP",
      "location": "Galway, ireland",
      "joblink/_source": "ApplyForJob.aspx?Id=1392393"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1365681",
      "jobtitle": "Tax Manager, Dublin",
      "joblink/_text": "Tax Manager, Dublin",
      "company": "Canduco",
      "location": "Dublin City Centre",
      "joblink/_source": "ApplyForJob.aspx?Id=1365681"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1412881",
      "jobtitle": "Financial Systems - SAP Master Data & SRM Analyst",
      "joblink/_text": "Financial Systems - SAP Master Data & SRM Analyst",
      "company": "Three Ireland",
      "location": "H3G Dublin 2",
      "joblink/_source": "ApplyForJob.aspx?Id=1412881"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1420677",
      "jobtitle": "Software Developer Team Leader Asp.net C#",
      "joblink/_text": "Software Developer Team Leader Asp.net C#",
      "company": "Hertz",
      "location": "Dublin North",
      "joblink/_source": "ApplyForJob.aspx?Id=1420677"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1420618",
      "jobtitle": "General Manager Job - Industrial Catering Dublin",
      "joblink/_text": "General Manager Job - Industrial Catering Dublin",
      "company": "Excel Recruitment",
      "location": "Dublin",
      "joblink/_source": "ApplyForJob.aspx?Id=1420618"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1420079",
      "jobtitle": "Quality Analyst & QA Lead - Contract & Permanent Roles available",
      "joblink/_text": "Quality Analyst & QA Lead - Contract & Permanent Roles available",
      "company": "Fenergo",
      "location": "Ormond Quay, Dublin",
      "joblink/_source": "ApplyForJob.aspx?Id=1420079"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1405669",
      "jobtitle": "Customer Support Manager (€40k+). Permanent position with multinational company based in Dublin.",
      "joblink/_text": "Customer Support Manager (€40k+). Permanent position with multinational company based in Dublin.",
      "company": "Approach People",
      "location": "Dublin",
      "joblink/_source": "ApplyForJob.aspx?Id=1405669"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1417627",
      "jobtitle": "Data Centre Manager",
      "joblink/_text": "Data Centre Manager",
      "company": "Hertz",
      "location": "Dublin North",
      "joblink/_source": "ApplyForJob.aspx?Id=1417627"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1402131",
      "jobtitle": "Frontend Web Designer",
      "joblink/_text": "Frontend Web Designer",
      "company": "Mobacar",
      "location": "Killarney, Co. Kerry",
      "joblink/_source": "ApplyForJob.aspx?Id=1402131"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1347667",
      "jobtitle": "German ( Deutscher ) IT Support Job with Cpl/ SAP, please contact Ulrike on 0035316146191",
      "joblink/_text": "German ( Deutscher ) IT Support Job with Cpl/ SAP, please contact Ulrike on 0035316146191",
      "company": "Cpl/SAP",
      "location": "Galway City",
      "joblink/_source": "ApplyForJob.aspx?Id=1347667"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1420327",
      "jobtitle": "Technical Project Manager",
      "joblink/_text": "Technical Project Manager",
      "company": "saongroup.com",
      "location": "Dublin 2",
      "joblink/_source": "ApplyForJob.aspx?Id=1420327"
    },
    {
      "joblink": "http://www.jobs.ie/ApplyForJob.aspx?Id=1419276",
      "jobtitle": "Web Developer Support (UK)",
      "joblink/_text": "Web Developer Support (UK)",
      "company": "PayPal Ireland",
      "location": "Blanchardstown",
      "joblink/_source": "ApplyForJob.aspx?Id=1419276"
    }
  ],
  "cookies": [
    "avid=\"51d0bb670e7b45c998d6de67ff4577e7\";Path=\"/\";Domain=\"www.jobs.ie\";Port=\"80\"",
    "ASP.NET_SessionId=\"1zzqiycot2touxmxgbgkdqhe\";Path=\"/\";Domain=\"www.jobs.ie\";Port=\"80\"",
    "Irishjobs=\"2378047498.20480.0000\";Path=\"/\";Domain=\"www.jobs.ie\";Port=\"80\""
  ],
  "connectorVersionGuid": "48e28941-ca4d-4467-bbbc-6f4404ad3121",
  "connectorGuid": "8cde72cc-d53a-457b-bd02-c179cacd436b",
  "pageUrl": "http://www.jobs.ie/Jobs.aspx?Categories=Job+Type&Regions=Job+Location&Keywords=.net"
}