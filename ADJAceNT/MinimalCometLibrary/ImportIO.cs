﻿//Import.IO API taken from their official git as outlined in documentation 
//https://github.com/import-io/importio-client-libs/blob/master/csharp/4.0/ImportIO.cs
#undef TRACE
using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;
using System.Web;
// Download the Newtonsoft JSON library here http://james.newtonking.com/projects/json-net.aspx
using Newtonsoft.Json;
using System.Threading;
using System.Collections.Concurrent;

namespace MinimalCometLibrary
{
    public delegate void QueryHandler(Query query, Dictionary<string, object> data);
    
    public class Query
    {
        private readonly QueryHandler queryCallback;

        private int jobsCompleted;
        private int jobsStarted;
        private int jobsSpawned;

        public Query(QueryHandler queryCallback)
        {
            this.queryCallback = queryCallback;
        }

        public bool IsFinished { get; private set; }

        public void OnMessage(Dictionary<string, object> data)
        {
            var messageType = (string)data["type"];
#if TRACE
            Console.WriteLine((string)data["type"]);
#endif
            switch (messageType)
            {
                case "SPAWN":
                    jobsSpawned++;
                    break;
                case "INIT":
                case "START":
                    jobsStarted++;
                    break;
                case "STOP":
                    jobsCompleted++;
                    break;
            }
            //avoid ressurecting queries, if it is finished once, never let it reset
            if (IsFinished || messageType.Equals("ERROR") || messageType.Equals("UNAUTH") || messageType.Equals("CANCEL"))
            {
                IsFinished = true;
            }
            else
            {
                IsFinished = jobsStarted == jobsCompleted && jobsSpawned + 1 == jobsStarted && jobsStarted > 0;
            }
            queryCallback(this, data);
        }
    }

    public class ImportIO
    {
        private const string MessagingChannel = "/messaging";

        private readonly string apiKey;
        private readonly string url;
        private readonly CookieContainer cookieContainer = new CookieContainer();
        private readonly List<Dictionary<string, object>> _error;
 
        private readonly ConcurrentDictionary<Guid, Query> queries = new ConcurrentDictionary<Guid, Query>();
        private readonly BlockingCollection<Dictionary<string, object>> messageQueue = new BlockingCollection<Dictionary<string, object>>();

        private Thread _pollConnection, _pollQueue;
        private int msgId;
        private string clientId;
        private bool isConnected;
        private readonly string requestAppendKey; //precomputed string with given guid and apiKey for performance

        public ImportIO(string host = "https://query.import.io", Guid userGuid = default(Guid), string apiKey = null)
        {
            this.apiKey = apiKey;
            url = host + "/query/comet/";
            clientId = null;
            requestAppendKey = "?_user=" + HttpUtility.UrlEncode(userGuid.ToString()) + "&_apikey=" + HttpUtility.UrlEncode(this.apiKey);
            _error = new List<Dictionary<string, object>>();
        }
        public void Login(string username, string password, string host = "https://api.import.io")
        {
            var loginParams = "username=" + HttpUtility.UrlEncode(username) + "&password=" + HttpUtility.UrlEncode(password);
            var loginUrl = host + "/auth/login";
            var loginRequest = (HttpWebRequest)WebRequest.Create(loginUrl);

            loginRequest.Method = "POST";
            loginRequest.ContentType = "application/x-www-form-urlencoded";
            loginRequest.ContentLength = loginParams.Length;
            loginRequest.CookieContainer = cookieContainer;

            using (var dataStream = loginRequest.GetRequestStream())
            {
                dataStream.Write(Encoding.UTF8.GetBytes(loginParams), 0, loginParams.Length);

                var loginResponse = (HttpWebResponse)loginRequest.GetResponse();

                if (loginResponse.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception("Could not log in, code:" + loginResponse.StatusCode);
                }
            }
        }

        public void Connect()
        {
            if (isConnected)
            {
                return;
            }

            clientId = null;
            Handshake();

            var subscribeData = new Dictionary<string, object> { { "subscription", MessagingChannel } };
            Request("/meta/subscribe", true, subscribeData);

            SoftRestartPollThreads();
            queries.Clear();
        }

        private void SoftRestartPollThreads()
        {
            isConnected = false;
            WaitOnThreadExit(ref _pollConnection);
            WaitOnThreadExit(ref _pollQueue);

            isConnected = true;

            //modified to be in background so that they don't hang "zombie" on unexpected application close
            _pollConnection = new Thread(Poll) { IsBackground = true };
            _pollConnection.Start();
            _pollQueue = new Thread(PollQueue) { IsBackground = true };
            _pollQueue.Start();
        }

        private void WaitOnThreadExit(ref Thread t)
        {
            if (t == null) return;
            try
            {
                //don't do abort, interrupt, just wait for loop to exit up to 5 seconds
                t.Join(5000);
                //if didn't stop/abort then abort explicitly
                if (!t.ThreadState.HasFlag(ThreadState.Stopped & ThreadState.Aborted))
                {
                    t.Abort();
                    while (!t.ThreadState.HasFlag(ThreadState.Stopped & ThreadState.Aborted))
                        Thread.Sleep(0);
                    t = null;
                }
            }
            catch (ThreadStateException){}
            catch (ThreadAbortException){}
        }

        public List<string> AuthenticateConnector(string connectorGuid, string connectorAuthDomain, string domainUsername, string domainPassword)
        {
            var requestUrl = string.Format("https://api.import.io/store/connector/{0}/_query", connectorGuid);
            requestUrl = AppendApiKey(requestUrl);

            var data = new Dictionary<string, object>
            {
                { "loginOnly", true },
                { "additionalInput", new Dictionary<string, object>
                    {
                        { connectorGuid, new Dictionary<string, object>
                            {
                                { "domainCredentials", new Dictionary<string, object>
                                    {
                                        { connectorAuthDomain, new Dictionary<string, object>
                                            {
                                                { "username", domainUsername },
                                                { "password", domainPassword }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            var dataJson = JsonConvert.SerializeObject(data);
            var request = BuildWebRequest(requestUrl, dataJson);

            using (var response = (HttpWebResponse)request.GetResponse())
            using (var responseStream = new StreamReader(response.GetResponseStream()))
            {
                var responseJson = responseStream.ReadToEnd();
                var responseList = JsonConvert.DeserializeObject<Dictionary<string, object>>(responseJson);
                return JsonConvert.DeserializeObject<List<string>>(responseList["cookies"].ToString());
            }
            
        }

        public bool DoQuery(Dictionary<string, object> query, QueryHandler queryHandler)
        {
            return DoQuery(query, new Query(queryHandler));
        }

        public bool DoQuery(Dictionary<string, object> query, Query specialQuery)
        {
            if (!isConnected) return false;
            var requestId = Guid.NewGuid();
            if (queries.TryAdd(requestId, specialQuery))
            {
                query.Add("requestId", requestId);
                try
                {
                    return Request("/service/query", new Dictionary<string, object> { { "data", query } }) != _error;
                }
                catch (WebException)
                {
                    isConnected = false;
                    OnDisconnected();
                }
            }
            return false;
        }

        public void Disconnect()
        {
            Request("/meta/disconnect", true);
            isConnected = false;
            WaitOnThreadExit(ref _pollConnection);
            WaitOnThreadExit(ref _pollQueue);
        }

        private void Handshake()
        {
            var handshakeData = new Dictionary<string, object>
            {
                {"version", "1.0"},
                {"minimumVersion", "0.9"},
                {"supportedConnectionTypes", new List<string> {"long-polling"}},
                {"advice", new Dictionary<string, int> {{"timeout", 60000}, {"interval", 0}}}
            };
            var responseList = Request("/meta/handshake", true, handshakeData, "handshake");
            clientId = (string)responseList.Find(_ => _.ContainsKey("clientId"))["clientId"];
        }

        private List<Dictionary<string, object>> Request(
        string channel,
        bool doThrow,
        Dictionary<string, object> data = null,
        string path = "")
        {
            var dataPacket = new Dictionary<string, object>
            {
                { "channel", channel },
                { "connectionType", "long-polling" },
                { "id", (Interlocked.Increment(ref msgId)).ToString() }
            };

            if (clientId != null)
            {
                dataPacket.Add("clientId", clientId);
            }

            if (data != null)
            {
                foreach (var entry in data)
                {
                    dataPacket.Add(entry.Key, entry.Value);
                }
            }

            var requestUrl = url + path;

            requestUrl = AppendApiKey(requestUrl);

            var dataJson = JsonConvert.SerializeObject(new List<object> { dataPacket });

            var request = BuildWebRequest(requestUrl, dataJson);

            //TODO: consider if timeout is required here and test it out (request.Timeout = ~30000)
            //TODO: read carefully http://bytes.com/topic/c-sharp/answers/549972-why-webhttprequest-getresponse-stuck discussion
            try
            {
                using (var response = (HttpWebResponse)request.GetResponse())
                using (var responseStream = new StreamReader(response.GetResponseStream()))
                {
                    var responseJson = responseStream.ReadToEnd();
                    var responseList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseJson);
                    foreach (var responseDict in responseList)
                    {
                        if (responseDict.ContainsKey("successful") && (bool)responseDict["successful"] != true)
                        {
                            if (doThrow)
                            {
                                return _error;
                            }
                        }
                        if (!responseDict["channel"].Equals(MessagingChannel))
                        {
                            continue;
                        }
                        if (responseDict.ContainsKey("data"))
                        {
                            messageQueue.Add(
                                ((Newtonsoft.Json.Linq.JObject)responseDict["data"])
                                    .ToObject<Dictionary<string, object>>());
                        }
                    }
                    return responseList;
                }
            }
            catch(WebException)
            {
                return _error;
            }
        }

        private List<Dictionary<string,object>> Request(string channel, Dictionary<string, object> data)
        {
            return Request(channel, false, data);
        }

        private HttpWebRequest BuildWebRequest(string requestUrl, string dataJson)
        {
            var request = (HttpWebRequest)WebRequest.Create(requestUrl);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Method = "POST";
            request.ContentType = "application/json;charset=UTF-8";
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip");
            request.ContentLength = dataJson.Length;
            request.CookieContainer = cookieContainer;
            request.Timeout = 60000;
            using (var dataStream = request.GetRequestStream())
            {
                dataStream.Write(Encoding.UTF8.GetBytes(dataJson), 0, dataJson.Length);
            }
            return request;
        }

        private string AppendApiKey(string requestUrl)
        {
            if (apiKey != null)
            {
                requestUrl = String.Concat(requestUrl, requestAppendKey);
            }
            return requestUrl;
        }

        private void Poll()
        {
            while (isConnected)
            {
                try
                {
                    Request("/meta/connect", false, null, "connect");
                }
                catch (Exception)
                {
                    isConnected = false;
                    OnDisconnected();
                }
            }
        }

        private void PollQueue()
        {
            while (isConnected)
            {
                ProcessMessage(messageQueue.Take());
            }
        }

        private void ProcessMessage(Dictionary<string, object> data)
        {
            var requestId = Guid.Parse((string)data["requestId"]);
            Query query;
            if (queries.TryGetValue(requestId, out query))
            {
                query.OnMessage(data);
                if (query.IsFinished)
                {
                    queries.TryRemove(requestId, out query);
                }
            }
        }

        public EventHandler Disconnected;

        protected virtual void OnDisconnected()
        {
            EventHandler handler = Disconnected;
            if (handler != null) handler(this, null);
        }
    }
}
