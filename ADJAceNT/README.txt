DISCLAIMER:
In addition to disclaimer provided by GNU v3.0 license in Section 15.
"Disclaimer of Warranty". It is stated that author provides NO guarantee 
that the import.io service API key and keys present in app.config of 
.\ADJAceNT will remain active. The account used is not intended to be 
terminated purposely, however any entity who wishes to use the code 
present in this solution should take precautions and consider creating 
own import.io account, connectors, extractors and hence API keys.

All projects and content of this solution is released
under GNU v3.0 license unless otherwise stated in this document.
The license is present in COPYRIGHT file in root directory or
is available at http://www.gnu.org/licenses/gpl-3.0.txt if 
COPYRIGHT file is damanged or missing.

File Hashes for COPYRIGHT.txt:
MD5: 3C34AFDC3ADF82D2448F12715A255122
SHA-1: 7713A1753CE88F2C7E6B054ECC8E4C786DF76300
SHA-256: 0B383D5A63DA644F628D99C33976EA6487ED89AAA59F0B3257992DEAC1171E6B
SHA-512: 4937848B94F5B50EA16C51F9E98FDCD3953ACA63D63CA3BB05D8A62C107E382B71C496838D130AE504A52032398630B957ACAEA6C48032081A6366D27CBA5EA9

Files not under GNU v3.0 license:
.\ADJAceNT\Dictionaries
- WordNet_3.1 obtained from http://wordnet.princeton.edu/wordnet/download/current-version/
	specifically http://wordnetcode.princeton.edu/wn3.1.dict.tar.gz
	License applicable:
	WordNet 3.1 license: (Download)
	
	WordNet Release 3.1 This software and database is being provided to you, the LICENSEE, by Princeton University under the following license. 
	By obtaining, using and/or copying this software and database, you agree that you have read, understood, and will comply with these terms 
	and conditions.: Permission to use, copy, modify and distribute this software and database and its documentation for any purpose and without 
	fee or royalty is hereby granted, provided that you agree to comply with the following copyright notice and statements, including the disclaimer,
	and that the same appear on ALL copies of the software, database and documentation, including modifications that you make for internal use or 
	for distribution. WordNet 3.1 Copyright 2006 by Princeton University. All rights reserved. 
	THIS SOFTWARE AND DATABASE IS PROVIDED "AS IS" AND PRINCETON UNIVERSITY MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED. BY WAY OF 
	EXAMPLE, BUT NOT LIMITATION, PRINCETON UNIVERSITY MAKES NO REPRESENTATIONS OR WARRANTIES OF MERCHANT- ABILITY OR FITNESS FOR ANY PARTICULAR 
	PURPOSE OR THAT THE USE OF THE LICENSED SOFTWARE, DATABASE OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS
	OR OTHER RIGHTS. The name of Princeton University or Princeton may not be used in advertising or publicity pertaining to distribution of the 
	software and/or database. Title to copyright in this software, database and any associated documentation shall at all times remain with Princeton
	University and LICENSEE agrees to preserve same.

.\ADJAceNT\ThirdParty
- All files within this directory contain license disclaimers at the start of the file as C# comments
	Any modifications made to the files may remain under original license
	The licenses present are Microsoft Public License (Ms-PL) and CPOL, please review the files for more detail

.\MinimalCometLibrary
- import.io service library for C# under the license:
	Note that all original code falls under the below license, however the class was extensively changed
	and all modifications now fall under GNU v3.0 license. The modifications can be obtained by performing
	a diff operation using one of your favourite source code file comparers or using github history tab
	at this project repository.
	Original class available at:https://github.com/import-io/importio-client-libs/blob/master/csharp/4.0/ImportIO.cs
	Commit Date: 26 Sep 2014 
	Commit Value: f9d499670093da20d62a863c114c894e3dfc16c8
	Apache License
	Version 2.0, January 2004
	http://www.apache.org/licenses/

.\WordNet
	The license from author of API is included in Docs\README.txt subfolder