﻿using System.Collections.Generic;
using ADJAceNT.Entities;
using ADJAceNT.Enums;
using ADJAceNT.Managers;
using ADJAceNT.ViewModel;
using Moq;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.ViewModel
{
    [TestFixture]
    class AdMiscActionViewModelTests
    {
        private AdMiscActionViewModel _vm;
        private Mock<IDataManager> _mockManager;

        [SetUp]
        public void Setup()
        {
            _mockManager = new Mock<IDataManager>();
            _mockManager.Setup(_ => _.BulkAddOrUpdateWordWeightMapping(It.IsAny<string>(),It.IsAny<List<string>>(), It.IsAny<bool>()));
            _vm = new AdMiscActionViewModel(new Mock<INotificationBarViewModel>().Object, _mockManager.Object);
            _vm.CurrentUser = "User";
        }

        [Test]
        public void SetAdvertStatusAndNotify_PositiveMissingAllParts_CallsDataManagerWithEmptyList()
        {
            var ad = new Advertisement
            {
                DescriptionSummary = "No description found",
                Salary = "Not Specified",
                Location = "Not Specified"
            };
            _vm.SetAdStatusGood.Execute(ad);

            _mockManager.Verify(_ => _.BulkAddOrUpdateWordWeightMapping(It.IsAny<string>(), It.Is<List<string>>(__ => __.Count == 0), true), Times.Once());
        }

        [Test]
        public void SetAdvertStatusAndNotify_PositiveGotAllParts_CallsDataManagerWithNWords()
        {
            var ad = new Advertisement
            {
                DescriptionSummary = "DescriptionWord",
                Salary = "SalaryWord",
                Location = "LocationWord"
            };
            _vm.SetAdStatusGood.Execute(ad);

            _mockManager.Verify(_ => _.BulkAddOrUpdateWordWeightMapping(It.IsAny<string>(), It.Is<List<string>>(__ => __.Count == 3), true), Times.Once());
        }

        [Test]
        public void SetAdvertStatusAndNotify_NegativeMissingAllParts_CallsDataManagerWithEmptyList()
        {
            var ad = new Advertisement
            {
                DescriptionSummary = "No description found",
                Salary = "Not Specified",
                Location = "Not Specified"
            };
            _vm.SetAdStatusBad.Execute(ad);

            _mockManager.Verify(_ => _.BulkAddOrUpdateWordWeightMapping(It.IsAny<string>(), It.Is<List<string>>(__ => __.Count == 0), false), Times.Once());
        }

        [Test]
        public void SetAdvertStatusAndNotify_UndoAfterInterestingMissingAllParts_CallsDataManagerWithEmptyList()
        {
            var ad = new Advertisement
            {
                DescriptionSummary = "No description found",
                Salary = "Not Specified",
                Location = "Not Specified",
                Status = AdvertisementStatus.Interesting
            };
            _vm.SetAdStatusNone.Execute(ad);

            _mockManager.Verify(_ => _.BulkAddOrUpdateWordWeightMapping(It.IsAny<string>(), It.Is<List<string>>(__ => __.Count == 0), false), Times.Once());
        }

        [Test]
        public void SetAdvertStatusAndNotify_UndoAfterUninterestingMissingAllParts_CallsDataManagerWithEmptyList()
        {
            var ad = new Advertisement
            {
                DescriptionSummary = "No description found",
                Salary = "Not Specified",
                Location = "Not Specified",
                Status = AdvertisementStatus.Uninteresting
            };
            _vm.SetAdStatusNone.Execute(ad);

            _mockManager.Verify(_ => _.BulkAddOrUpdateWordWeightMapping(It.IsAny<string>(), It.Is<List<string>>(__ => __.Count == 0), true), Times.Once());
        }
    }
}
