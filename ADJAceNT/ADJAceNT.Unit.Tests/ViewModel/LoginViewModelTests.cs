﻿using System.Collections.Generic;
using System.Windows;
using ADJAceNT.Managers;
using ADJAceNT.ViewModel;
using log4net;
using Moq;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.ViewModel
{
    [TestFixture]
    class LoginViewModelTests
    {
        private Mock<INotificationBarViewModel> _mockNotifier;
        private Mock<IDataManager> _mockDataManager;
        private LoginViewModel _vm;

        [SetUp]
        public void SetUp()
        {
            _mockNotifier = new Mock<INotificationBarViewModel>();
            _mockDataManager = new Mock<IDataManager>();
            _mockDataManager.Setup(_ => _.GetAllUsers()).Returns(new List<string>{"random user"});
            _vm = new LoginViewModel(_mockNotifier.Object, _mockDataManager.Object, new Mock<ILog>().Object);
        }

        [Test]
        public void Reset_PropertiesChangedAndInvoked_SetsPropertiesToOriginalValues()
        {
            var control = new LoginViewModel(new Mock<INotificationBarViewModel>().Object, _mockDataManager.Object, new Mock<ILog>().Object);
            _vm.Username = "differentOne";
            _vm.AllUsers.Add("random user");
            _vm.ScreenVisible = Visibility.Hidden;
            _vm.Restart();

            Assert.AreEqual(control.AllUsers.Count, _vm.AllUsers.Count);
            Assert.AreEqual(control.ScreenVisible, _vm.ScreenVisible);
            Assert.AreEqual(control.Username, _vm.Username);
            _mockNotifier.Verify(_ => _.Info(It.IsAny<string>()), Times.Exactly(2));//at startup and after restart
        }

        [Test]
        public void Reset_PropertiesNotChangedAndInvoked_SetsPropertiesToOriginalValues()
        {
            var control = new LoginViewModel(new Mock<INotificationBarViewModel>().Object, _mockDataManager.Object, new Mock<ILog>().Object);
            _vm.Restart();

            Assert.AreEqual(control.AllUsers.Count, _vm.AllUsers.Count);
            Assert.AreEqual(control.ScreenVisible, _vm.ScreenVisible);
            Assert.AreEqual(control.Username, _vm.Username);
            _mockNotifier.Verify(_ => _.Info(It.IsAny<string>()), Times.Exactly(2));//at startup and after restart
        }

        [Test]
        public void ValidateAndUseUsername_InvalidUsername_IssuesWarningNotificationAndSetsUsernameNull()
        {
            var eventRaised = false;
            _vm.UserLoggedIn += (sender, args) => eventRaised = true;
            _vm.Username = "[invalid]";
            _vm.StartWithUsernameChosen.Execute(null);

            Assert.False(eventRaised);
            Assert.AreEqual(null, _vm.Username);
            _mockNotifier.Verify(_ => _.Warning(It.IsAny<string>()), Times.Exactly(1));
        }

        [Test]
        public void ValidateAndUseUsername_NullUsername_IssuesWarningNotificationAndSetsUsernameNull()
        {
            var eventRaised = false;
            _vm.UserLoggedIn += (sender, args) => eventRaised = true;
            _vm.Username = null;
            _vm.StartWithUsernameChosen.Execute(null);

            Assert.False(eventRaised);
            Assert.AreEqual(null, _vm.Username);
            _mockNotifier.Verify(_ => _.Warning(It.IsAny<string>()), Times.Exactly(1));
        }

        [Test]
        public void ValidateAndUseUsername_ValidUsername_InvokesAddUserOnDataSourcewithUsernamePassedIn()
        {
            var eventRaised = false;
            _vm.UserLoggedIn += (sender, args) => eventRaised = true;
            _vm.Username = "valid";
            _vm.StartWithUsernameChosen.Execute(null);

            Assert.True(eventRaised);
            Assert.AreEqual("valid", _vm.Username);
            _mockDataManager.Verify(_ => _.AddUser(_vm.Username), Times.Exactly(1));
        }
    }
}
