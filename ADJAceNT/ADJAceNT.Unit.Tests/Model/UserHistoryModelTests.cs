﻿using System;
using System.Collections.Generic;
using ADJAceNT.Entities;
using ADJAceNT.Enums;
using ADJAceNT.Managers;
using ADJAceNT.Model;
using Moq;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Model
{
    [TestFixture]
    class UserHistoryModelTests
    {
        private UserHistoryModel _model;
        private readonly Mock<IDataManager> _mockManager;

        public UserHistoryModelTests()
        {
            _mockManager = new Mock<IDataManager>();
            _mockManager
                .Setup(_ => _.GetUserHistoryByTimeUnit(It.IsAny<string>(), TimeUnit.seconds, 1, 1))
                .Returns(new Dictionary<DateTime, string> {{DateTime.Now, "SomeWordsLoaded"}});

            _mockManager
                .Setup(_ => _.GetUserHistoryByTimeUnit(It.IsAny<string>(), TimeUnit.seconds, 1, 2))
                .Returns(new Dictionary<DateTime, string> {{DateTime.Now, "SomeWordsLoaded"},{DateTime.Now.AddSeconds(1), "OtherLoaded"}});
        }

        [SetUp]
        public void SetUp()
        {
            _model = new UserHistoryModel(_mockManager.Object) {CurrentUser = "User"};
        }

        [Test]
        public void SelectedHistory_ReturnsIndependentList()
        {
            _model.SelectedHistory = new List<HistoryItem> { new Mock<HistoryItem>().Object };
            var independentList = _model.SelectedHistory;
            independentList.Add(new Mock<HistoryItem>().Object);

            Assert.AreEqual(1, _model.SelectedHistory.Count);
            Assert.AreEqual(2, independentList.Count);
        }

        [Test]
        public void SelectAll_HasItemsLoaded_HasAllLoadedItemsAsSelected()
        {
            _model.LoadHistoryByTimeUnit(TimeUnit.seconds, 1, 1);

            _model.SelectAll();

            var result = _model.SelectedHistory;

            Assert.AreEqual(1, result.Count);
        }

        public void UnSelectAll_HasItemsLoaded_MakesSelectedItemsCollectionEmpty()
        {
            _model.SelectedHistory = new List<HistoryItem>{new Mock<HistoryItem>().Object};

            _model.UnSelectAll();
            var result = _model.SelectedHistory;

            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void LoadHistoryByTimeUnit_GetsTwoItems_OrdersThemNewestDateFirst()
        {
            _model.LoadHistoryByTimeUnit(TimeUnit.seconds, 1, 2);

            var result = _model.History;

            Assert.AreEqual(2, result.Count);
            Assert.True(result[0].ItemDate > result[1].ItemDate);
        }

        [Test]
        public void ClearHistoryLoaded_HasItemsInCollections_MakesBothcollectionsEmpty()
        {
            _model.LoadHistoryByTimeUnit(TimeUnit.seconds, 1, 2);
            _model.SelectAll();

            _model.ClearHistoryLoaded();

            Assert.AreEqual(0, _model.SelectedHistory.Count);
            Assert.AreEqual(0, _model.History.Count);
        }

        [Test]
        public void DeleteSelectedPermanently_HasSelectedItems_CallsToDeleteInDataManager()
        {
            _model.DeleteSelectedPermanently(new DateTime());

            _mockManager.Verify(_ => _.RevertUserDataByDate(It.IsAny<string>(), new DateTime()), Times.Once());
        }
    }
}
