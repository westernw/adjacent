﻿using System;
using System.Globalization;
using System.Windows;
using ADJAceNT.Converters;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Converters
{
    class InvertableBooleanToVisibilityConverterTests
    {
        private readonly InvertableBooleanToVisibilityConverter _converter;
        private static object[] VisibleCases;
        private static object[] CollapsedCases;

        static InvertableBooleanToVisibilityConverterTests()
        {
            const bool directVisibility = false;
            const bool invertedVisibility = true;

            const bool isVisible = true;
            const bool isCollapsed = false;

            VisibleCases = new object[]
            {
                new object[]{isVisible, directVisibility},
                new object[]{isCollapsed, invertedVisibility}
            };

            CollapsedCases = new object[]
            {
                new object[] {isVisible, invertedVisibility},
                new object[] {isCollapsed, directVisibility}
            };
        }

        public InvertableBooleanToVisibilityConverterTests()
        {
            _converter = new InvertableBooleanToVisibilityConverter();
        }

        //vi || cd    == Collapsed
        //vd || ci    == Visible 
        [Test, TestCaseSource("VisibleCases")]
        public void Convert_VariousInput_ExpectsVisible(object value, object parameter)
        {
            var result = _converter.Convert(value, default(Type), parameter, default(CultureInfo));

            Assert.AreEqual(Visibility.Visible, result);
        }

        [Test, TestCaseSource("CollapsedCases")]
        public void Convert_VariousInput_ExpectsCollapsed(object value, object parameter)
        {
            var result = _converter.Convert(value, default(Type), parameter, default(CultureInfo));

            Assert.AreEqual(Visibility.Collapsed, result);
        }
    }
}
