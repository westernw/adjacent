﻿using System;
using System.Globalization;
using System.Windows;
using ADJAceNT.Converters;
using ADJAceNT.Enums;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Converters
{
    [TestFixture]
    class AdvertisementStatusButtonVisibilityStatusConverterTests
    {
        private readonly AdvertisementStatusButtonVisibilityStatusConverter _converter;
        private static readonly object[] VisibleCases;
        private static readonly object[] CollapsedCases;

        static AdvertisementStatusButtonVisibilityStatusConverterTests()
        {
            const AdvertisementStatus goodAd = AdvertisementStatus.Interesting;
            const AdvertisementStatus badAd = AdvertisementStatus.Uninteresting;
            const AdvertisementStatus noneAd = AdvertisementStatus.None;

            VisibleCases = new object[]
            {
                new object[]{noneAd, "Good"},
                new object[]{noneAd, "Bad"},
                new object[]{goodAd, "Undo"},
                new object[]{badAd, "Undo"}
            };

            CollapsedCases = new object[]
            {
                new object[] {goodAd, "Good"},
                new object[] {badAd, "Good"},
                new object[] {goodAd, "Bad"},
                new object[] {badAd, "Bad"},
                new object[] {noneAd, "Undo"}
            };
        }

        public AdvertisementStatusButtonVisibilityStatusConverterTests()
        {
            _converter = new AdvertisementStatusButtonVisibilityStatusConverter();
        }

        //gg || bg || gb || bb || nu    == Collapsed
        //ng || nb || gu || bu 	        == Visible 
        [Test, TestCaseSource("VisibleCases")]
        public void Convert_VariousInput_ExpectsVisible(object value, object parameter)
        {
            var result = _converter.Convert(value, default(Type), parameter, default(CultureInfo));

            Assert.AreEqual(Visibility.Visible, result);
        }

        [Test, TestCaseSource("CollapsedCases")]
        public void Convert_VariousInput_ExpectsCollapsed(object value, object parameter)
        {
            var result = _converter.Convert(value, default(Type), parameter, default(CultureInfo));

            Assert.AreEqual(Visibility.Collapsed, result);
        }
    }
}
