﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;
using ADJAceNT.Converters;
using ADJAceNT.Entities;
using Moq;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Converters
{
    [TestFixture]
    class KeyWordConverterTests
    {
        private readonly KeyWordHighlightingConverter _converter;
        private readonly Advertisement _nullDescription;
        private readonly Advertisement _properDescriptionNoMatchingSearchWord;
        private readonly Advertisement _properDescriptionWithMatchingSearchWord;

        public KeyWordConverterTests()
        {
            _converter = new KeyWordHighlightingConverter();
            _nullDescription = new Advertisement();
            _properDescriptionNoMatchingSearchWord = new Advertisement
            {
                DescriptionSummary = "This job is for enthusiastic and ... Sales Manager"
            };
            _properDescriptionWithMatchingSearchWord = new Advertisement
            {
                DescriptionSummary = "This job is for enthusiastic and ... Sales Manager"
            };
        }

        [Test]
        public void Convert_null_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _converter.Convert(null, new Mock<Type>().Object, null, CultureInfo.CurrentCulture));
        }

        [Test, RequiresSTA]
        public void Convert_AdWithNullDescription_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _converter.Convert(new object[] { _nullDescription, new List<string>() }, new Mock<Type>().Object, null, CultureInfo.CurrentCulture));
        }

        [Ignore("TextBlock.Text does not work until loaded"), Test, RequiresSTA]
        public void Convert_DescriptionNoMatchingSearchWord_SameTextboxOutputAsDescription()
        {
            var txtBox = _converter.Convert(new object[] { _properDescriptionNoMatchingSearchWord, new List<string>() }, new Mock<Type>().Object, null, CultureInfo.CurrentCulture) as TextBlock;
            var test = txtBox.Text;
            Assert.AreEqual(txtBox.Text, _properDescriptionNoMatchingSearchWord.DescriptionSummary);
        }

        [Ignore("TextBlock.Text does not work until loaded"), Test, RequiresSTA]
        public void Convert_DescriptionWithMatchingSearchWord_DifferentTextboxOutputToDescription()
        {
            var txtBox = _converter.Convert(new object[] { _properDescriptionWithMatchingSearchWord, new List<string> { "Sales", "Manager" } }, new Mock<Type>().Object, null, CultureInfo.CurrentCulture) as TextBlock;
            Assert.AreNotEqual(txtBox.Text, _properDescriptionWithMatchingSearchWord.DescriptionSummary);
        }


    }
}
