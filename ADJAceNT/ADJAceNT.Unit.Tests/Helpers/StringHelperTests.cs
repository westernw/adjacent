﻿using System;
using ADJAceNT.Helpers;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Helpers
{
    [TestFixture]
    class StringHelperTests
    {
        private string _stringWithAllDelimeters;
        
        [SetUp]
        public void SetUp()
        {
            _stringWithAllDelimeters =
                "this string;has,;,13:entries\"which\'are.returned-note(empty*entries*are*removed)))";
        }

        [Test]
        public void SplitByPunctuation_NullString_ThrowsExceptionAsAnyOtherStringMethodDoes()
        {
            Assert.Throws<NullReferenceException>(() => (null as string).SplitByPunctuation());
        }

        [Test]
        public void SplitByPunctuation_ValidString_SplitsOnAllDocumentedCharacters()
        {
            var results = _stringWithAllDelimeters.SplitByPunctuation();
            Assert.AreEqual(13, results.Length);
            Assert.AreEqual("this", results[0]);
            Assert.AreEqual("string", results[1]);
            Assert.AreEqual("has", results[2]);
            Assert.AreEqual("13", results[3]);
            Assert.AreEqual("entries", results[4]);
            Assert.AreEqual("which", results[5]);
            Assert.AreEqual("are", results[6]);
            Assert.AreEqual("returned", results[7]);
            Assert.AreEqual("note", results[8]);
            Assert.AreEqual("empty", results[9]);
            Assert.AreEqual("entries", results[10]);
            Assert.AreEqual("are", results[11]);
            Assert.AreEqual("removed", results[12]);
        }
    }
}
