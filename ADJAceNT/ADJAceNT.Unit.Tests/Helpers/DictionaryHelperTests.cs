﻿using System.Collections.Generic;
using ADJAceNT.Helpers;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Helpers
{
    [TestFixture]
    class DictionaryHelperTests
    {
        private Dictionary<string, string> _nullDictionary;
        private Dictionary<string, string> _emptyDictionary;
        private Dictionary<string, string> _sampleDictionary;

        [SetUp]
        public void SetUp()
        {
            _nullDictionary = null;
            _emptyDictionary = new Dictionary<string, string>();
            _sampleDictionary = new Dictionary<string, string>{{"some","entries"}, {"more","words"}};
        }

        [Test]
        public void IsNullOrEmpty_NullDictionary_ReturnsTrue()
        {
            Assert.True(_nullDictionary.IsNullOrEmpty());
        }

        [Test]
        public void IsNullOrEmpty_EmptyDictionary_ReturnsTrue()
        {
            Assert.True(_emptyDictionary.IsNullOrEmpty());
        }

        [Test]
        public void IsNullOrEmpty_NonEmptyDictionary_ReturnsFalse()
        {
            Assert.False(_sampleDictionary.IsNullOrEmpty());
        }

        [Test]
        public void AddOrUpdateIfLonger_NewKeyWord_TotalCountOneValuePairAdded()
        {
            _emptyDictionary.AddOrUpdateIfLonger("some", "word");

            Assert.AreEqual(1, _emptyDictionary.Count);
            Assert.AreEqual("word", _emptyDictionary["some"]);
        }

        [Test]
        public void AddOrUpdateIfLonger_LongerWord_TotalCountSameValueReplaced()
        {
            var initialCount = _sampleDictionary.Count;
            _sampleDictionary.AddOrUpdateIfLonger("some", "wordLonger");

            Assert.AreEqual(initialCount, _sampleDictionary.Count);
            Assert.AreEqual("wordLonger", _sampleDictionary["some"]);
        }

        [Test]
        public void AddOrUpdateIfLonger_ShorterWord_TotalCountSameValueUnchanged()
        {
            var initialCount = _sampleDictionary.Count;
            _sampleDictionary.AddOrUpdateIfLonger("some", "short");

            Assert.AreEqual(initialCount, _sampleDictionary.Count);
            Assert.AreNotEqual("short", _sampleDictionary["some"]);
        }
    }
}
