﻿using ADJAceNT.Managers;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Managers
{
    class SynonymManagerTests
    {
        private readonly SynonymManager _manager;

        public SynonymManagerTests()
        {
            const string path = "C:\\Projects\\FYP\\ADJAceNT\\ADJAceNT\\bin\\Debug\\Dictionaries\\WordNet_3.1";
            _manager = new SynonymManager(path);
        }

        [Test]
        public void GetMostLikelySynonymGroup_ExistingWord_ReturnsSynonymArray()
        {
            var synonyms = _manager.GetMostLikelySynonymGroup("bakery");
            Assert.Contains("bakeshop", synonyms);
            Assert.Contains("bakery", synonyms);
            Assert.Contains("bakehouse", synonyms);
        }

        [Test]
        public void GetMostLikelySynonymGroup_FakeWord_ReturnsOriginalWordOnly()
        {
            var synonyms = _manager.GetMostLikelySynonymGroup("apzi45");
            Assert.AreEqual("apzi45", synonyms[0]);
            Assert.AreEqual(1, synonyms.Length);
        }

        [Test]
        public void GetMostLikelySynonymGroup_ExistingWordWithoutSynonyms_ReturnsOriginalWordOnly()
        {
            var synonyms = _manager.GetMostLikelySynonymGroup("parabola");
            Assert.AreEqual("parabola", synonyms[0]);
            Assert.AreEqual(1, synonyms.Length);
        }
    }
}
