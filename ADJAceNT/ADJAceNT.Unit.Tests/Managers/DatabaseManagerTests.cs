﻿using System;
using ADJAceNT.Managers;
using log4net;
using Moq;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Managers
{
    class DatabaseManagerFake : DatabaseManager
    {
        public DatabaseManagerFake(ILog log):base(log, false)
        {
        }
    }
    
    /// <summary>
    /// Database tested manually,but can be implemented by including sample database
    /// into ADJAceNT.Unit.Tests root folder and then tests can be run here and more added 
    /// </summary>
    [TestFixture]
    class DatabaseManagerTests
    {
        private DatabaseManager _db;
        private Mock<ILog> _mockLog;

        [SetUp]
        public void Setup()
        {
            //new clean db manager for each test
            _mockLog = new Mock<ILog>();
            _mockLog.Setup(x => x.Error(It.IsAny<String>()));
            _db = new DatabaseManagerFake(_mockLog.Object);
        }

        [Test]
        [Ignore]
        public void AddUser_ValidUsername_DoesNotThrowException()
        {
            _db.AddUser("ValidUser");
            _mockLog.Verify(x => x.Error(It.IsAny<String>()), Times.Never());
        }
    }
}
