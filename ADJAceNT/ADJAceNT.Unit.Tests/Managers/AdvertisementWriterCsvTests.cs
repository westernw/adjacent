﻿using System;
using ADJAceNT.Entities;
using ADJAceNT.Managers;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Managers
{
    [TestFixture]
    class AdvertisementWriterCsvTests
    {
        [Test]
        public void AdToCsv_ValidAd_QuoteEscapedString()
        {
            var original = new Advertisement
            {
                Company = "Company Name",
                DescriptionSummary = "Description",
                Salary = "Salary\"10\"",
                JobLink = new Uri("http://www.dit.ie"),
                JobTitle = "Lecturer",
                Location = "Dublin"
            };
            var result = AdvertisementWriterCsv.AdToCsvRow(original);

            Assert.AreEqual("\"Company Name\",\"Description\",\"Lecturer\",\"Salary\"\"10\"\"\",\"Dublin\",\"http://www.dit.ie/\"", result);
        }

        [Test]
        public void AdToCsv_NullAdvertisement_ThrowsNullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() => AdvertisementWriterCsv.AdToCsvRow(null));
        }
    }
}
