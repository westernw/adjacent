﻿using System;
using ADJAceNT.Managers;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Managers
{
    [TestFixture]
    class AdvertisementReaderCsvTests
    {
        [Test]
        public void CsvToAd_ValidQuoteEscapedCsvRow_Advertisement()
        {
            var result = AdvertisementReaderCsv.CsvRowToAd("\"Company Name\",\"Description\",\"Lecturer\",\"Salary\"10\"\",\"Dublin\",\"http://www.dit.ie/\"");

            Assert.AreEqual("Company Name", result.Company);
            Assert.AreEqual("Description", result.DescriptionSummary);
            Assert.AreEqual("Lecturer", result.JobTitle);
            Assert.AreEqual("Dublin", result.Location);
            Assert.AreEqual(new Uri("http://www.dit.ie"), result.JobLink);
        }

        [TestCase("Some\",\"Invalid\",\"Row")]
        [TestCase("Invalid,Row,but,Correct,Number,Columns")]
        public void CsvToAd_InvalidCsvRow_ThrowsArgumentException(string invalidRow)
        {
            Assert.Throws<ArgumentException>(() => AdvertisementReaderCsv.CsvRowToAd(invalidRow));
        }
    }
}
