﻿using System.Collections.Generic;
using ADJAceNT.Entities;
using ADJAceNT.Managers;
using Moq;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Managers
{
    [TestFixture]
    class RankManagerTests
    {
        private readonly RankManager _mgr;
        private List<Advertisement> _adverts;
        private Advertisement _ad1;
        private Advertisement _ad2;
        private Advertisement _ad3;
        private Advertisement _ad4;

        [SetUp]
        public void SetUpAdverts()
        {
            _ad1 = new Advertisement { DescriptionSummary = "some important words" , Salary = "40k"};
            _ad2 = new Advertisement { DescriptionSummary = "less important" , Salary = "40k" };
            _ad3 = new Advertisement { DescriptionSummary = "all not at all words" , Salary = "40k" };
            _ad4 = new Advertisement { DescriptionSummary = "completely irrelevant" , Salary = "40k" };
            _adverts = new List<Advertisement> { _ad1, _ad2, _ad3, _ad4 };
        }

        public RankManagerTests()
        {
            var dataMock = new Mock<IDataManager>();
            dataMock.Setup(_ => _.GetUserWordWeightMappings("user"))
                     .Returns(new Dictionary<string, int>
                     {
                         {"IMPORTANT", 2},
                         {"WORDS", 1}
                     });

            _mgr = new RankManager("user", dataMock.Object);
        }

        [TestCase("wOrds")]
        [TestCase("Words")]
        [TestCase("words")]
        public void RankAdvertisements_PresetSearchWord1_ExpectsSortedReturn(string searchWord)
        {
            var result = _mgr.RankAdvertisements(_adverts, new List<string> {searchWord});

            Assert.AreEqual(_adverts.Count, result.Count);
            Assert.AreEqual(_ad1, result[0]);
            Assert.AreEqual(_ad2, result[1]);
            Assert.AreEqual(_ad3, result[2]);
            Assert.AreEqual(_ad4, result[3]);
            Assert.AreEqual(4, result[0].Rank);
            Assert.AreEqual(2, result[1].Rank);
            Assert.AreEqual(2, result[2].Rank);
            Assert.AreEqual(0, result[3].Rank);
        }

        [Test]
        public void RankAdvertisements_PresetSearchWord2_ExpectsSortedReturn()
        {
            var result = _mgr.RankAdvertisements(_adverts, new List<string> { "all" });

            Assert.AreEqual(_adverts.Count, result.Count);
            Assert.AreEqual(_ad1, result[0]);
            Assert.AreEqual(_ad3, result[1]);
            Assert.AreEqual(_ad2, result[2]);
            Assert.AreEqual(_ad4, result[3]);
            Assert.AreEqual(3, result[0].Rank);
            Assert.AreEqual(3, result[1].Rank);
            Assert.AreEqual(2, result[2].Rank);
            Assert.AreEqual(0, result[3].Rank);
        }
    }
}
