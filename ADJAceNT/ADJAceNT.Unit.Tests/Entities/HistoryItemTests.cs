﻿using System;
using ADJAceNT.Entities;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Entities
{
    [TestFixture]
    class HistoryItemTests
    {
        [Test]
        public void Equals_Null_False()
        {
            Assert.False(new HistoryItem(DateTime.Now, string.Empty).Equals(null));
        }

        [Test]
        public void Equals_object_False()
        {
            Assert.False(new HistoryItem(DateTime.Now, string.Empty).Equals(new object()));
        }

        [Test]
        public void Equals_SameAdvertObject_True()
        {
            var ad1 = new HistoryItem(DateTime.Now, "various words");
            var ad2 = ad1;
            Assert.True(ad1.Equals(ad2));
        }
    }
}
