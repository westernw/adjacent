﻿using System;
using ADJAceNT.Entities;
using NUnit.Framework;

namespace ADJAceNT.Unit.Tests.Entities
{
    [TestFixture]
    class AdvertisementTests
    {
        [Test]
        public void Equals_Null_False()
        {
            Assert.False(new Advertisement().Equals(null));
        }

        [Test]
        public void Equals_object_False()
        {
            Assert.False(new Advertisement().Equals(new object()));
        }

        [Test]
        public void Equals_DifferentJobUri_False()
        {
            var ad1 = new Advertisement{JobLink = new Uri("http://www.dit.ie")};
            var ad2 = new Advertisement {JobLink = new Uri("http://www.dit.ie/index.html")};
            Assert.False(ad1.Equals(ad2));
        }

        [Test]
        public void Equals_SameJobUri_True()
        {
            var ad1 = new Advertisement { JobLink = new Uri("http://www.dit.ie") };
            var ad2 = new Advertisement { JobLink = new Uri("http://www.dit.ie/") };
            Assert.True(ad1.Equals(ad2));
        }

        [Test]
        public void Equals_SameAdvertObject_True()
        {
            var ad1 = new Advertisement();
            var ad2 = ad1;
            Assert.True(ad1.Equals(ad2));
        }
    }
}
