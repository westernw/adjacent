# README #

### What is this repository for? ###

* Quick summary: The code present forms part of Thesis work of Aleksandrs Kurjanovs, Dublin Institute of Technology, Ireland 2014/2015
* Version: 1.0.0.0

### How do I get set up? ###

** To be updated **

* Summary of set up
* Configuration
* Dependencies: The project requires the following external libraries in order to run as intended.

You can install the application and get the required DLLs there as alternative (https://www.dropbox.com/s/ilupq8peq4ypkle/ADJAceNTSetup.exe?dl=0)

    + log4net (at time of writing 1.2.13), .NET logging framework, "bin\log4net-1.2.13\bin\net\4.0\release\log4net.dll"
> http://ftp.heanet.ie/mirrors/www.apache.org/dist//logging/log4net/source/log4net-1.2.13-src.zip

    + Newtonsoft.Json, JSON parsing library, "Bin\Net45\Newtonsoft.Json.dll"
> https://github.com/JamesNK/Newtonsoft.Json/releases

    + Sqlite (at time of writing) v.1.0.94.0, "System.Data.SQLite.dll"
> http://system.data.sqlite.org/index.html/doc/trunk/www/downloads.wiki

    + Testing NUint and Moq
> http://nunit.org/?p=download
>
> https://code.google.com/p/moq/downloads/detail?name=Moq.4.0.10827.Final.zip

    + WordNet API requires LAIR libraries described in Doc
> https://ptl.sys.virginia.edu/msg8u/NLP/Libraries/Public

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

** The project is released under [General Public License v3.0](http://opensource.org/licenses/GPL-3.0) **

* NO contributions will be examined until end of March 2015.
* Writing tests: Testing project follows same layout as the main project and is stored parallel to the the main project in solution

### Who do I talk to? ###

* Repo owner or admin